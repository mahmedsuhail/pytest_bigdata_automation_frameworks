#!/bin/bash

# resolve encoding error
export PYTHONIOENCODING=utf8

# set python3 as default
sudo alternatives --set python /usr/bin/python3.6

# set PYSPARK environment variables
export SPARK_HOME=/usr/lib/spark
export PYTHONPATH=$SPARK_HOME/python/lib/py4j-0.10.7-src.zip:$PYTHONPATH
export PYTHONPATH=$SPARK_HOME/python:$SPARK_HOME/python/build:$PYTHONPATH

# pre-req libraries
sudo pip install boto3
sudo pip install pandas
sudo pip install findspark
sudo pip install PyMySQL
sudo pip install pytest
sudo pip install pytest-spark
sudo pip install allure-pytest
sudo pip install pytest-xdist

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
export NVM_DIR="$HOME/.nvm"
. ~/.nvm/nvm.sh
nvm install node
npm install allure-commandline
