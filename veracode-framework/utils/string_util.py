"""
----------------------------------------------------------------------------------------------------------
Description:

usage: This script will be used to manipulate stings as per project need.

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-587

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/15/2020        Zeeshan Mirza        VDL-587              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""
import allure

@allure.step("Format table {table_name}")
def format_table_name( table_name ):
    table_name = table_name.replace('-', '_')
    table_name = table_name.replace(':', '_')
    table_name = table_name.replace('.', '_')
    return table_name