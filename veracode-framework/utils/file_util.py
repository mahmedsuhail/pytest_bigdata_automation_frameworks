import os


def get_config_file(architecture, file_name):
    dir_path = os.path.dirname(__file__)
    if architecture.lower() == 'glue':
        folder = 'glue_architecture_based_config'
    elif architecture.lower() == 'schema':
        folder = 'sch_evo_architecture_based_config'
    rel_path = f'../configs/{folder}/{file_name}'
    abs_path = os.path.join(dir_path, rel_path)
    with open(abs_path) as file:
        return file.read()


if __name__ == '__main__':
    # for testing
    data = get_config_file('schema', 'catalog_config.json')
    print(data)

