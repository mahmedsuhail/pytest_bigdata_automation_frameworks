"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Not null column Validation

Author  : Zeeshan Mirza
Release : 1
Sprint  : 4
Story   : VDL-618

Modification Log:

-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
02/12/2020        Usama Abbas        VDL-618             Initial draft.
02/14/2020        Usama Abbas        VDL-618             Made following changes
                                                         1. File path correction made
                                                         2. Error code correction made
                                                         3. Program exit with exit code

-----------------------------------------------------------------------------------------------------------
"""


import argparse
import json


def get_severity_dict(path):
    file = open(path, 'r')
    data = file.read()
    return json.loads(data)


def get_test_cases_stats(test_cases):
    failed = 0
    broken = 0
    total = len(test_cases)
    for _case in test_cases:
        if _case['status'].lower() == 'failed' and _case['severity'].lower() == 'critical':
            failed += 1
        elif _case['status'].lower() == 'broken':
            broken += 1
    return {'total': total, 'failed': failed, 'broken': broken}


def get_test_cases_status(path):
    test_cases = get_severity_dict(path)
    response = get_test_cases_stats(test_cases)
    if response['failed'] or response['broken']:
        response['status_code'] = 1
    else:
        response['status_code'] = 0
    return response


def main(args):
    hop = args.hop #'staging'  # args.hop
    etl_ts = args.etl_ts #'202001031052'  # args.etl_ts
    path = f'test-framework/allure-reports/{hop}/{etl_ts}/widgets/severity.json'
    return get_test_cases_status(path)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    args = parser.parse_args()

    result_code = main(args)
    exit(result_code['status_code'])
