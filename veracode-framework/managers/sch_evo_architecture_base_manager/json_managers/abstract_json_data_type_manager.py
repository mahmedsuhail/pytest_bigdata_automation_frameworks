"""
----------------------------------------------------------------------------------------------------------
Description:

usage: This is an Abstract Class deals with all config files of the project to process and returns the required out put.

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-587

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/01/2019        Zeeshan Mirza        VDL-587              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

from abc import ABC, abstractmethod


class AbstractJsonDataTypeManager(ABC):
    def __init__(self):
        print()

    @abstractmethod
    def get_all_sources(self):
        pass
