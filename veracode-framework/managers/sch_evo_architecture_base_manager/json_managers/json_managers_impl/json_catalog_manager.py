"""
----------------------------------------------------------------------------------------------------------
Description:

usage: This is an Abstract Class deals with all config files of the project to process and returns the required out put.

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-587

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/01/2019        Zeeshan Mirza        VDL-587              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import json

from managers.common_managers.s3_manager import s3_landing_data as s3_manager
from managers.sch_evo_architecture_base_manager.json_managers.abstract_json_catalog_manager import AbstractJsonManager


class JsonCatalogManager(AbstractJsonManager):

    def __init__(self, source, database, config_file_name):
        self.source = source
        self.database = database
        self.config_file_name = config_file_name
        self.s3_manager = s3_manager()
        s3_file = self.s3_manager.get_config_file(self.config_file_name)
        self.config = json.loads(s3_file)

    def get_database_by_source(self):
        return self.config[self.source]['sources']

    def get_table_names_by_database(self):
        return self.config[self.source][self.database]['tables']

    def get_target_db_by_source_database(self, stage_name):
        return self.config[self.source][self.database]['target_db'][stage_name]

    def __get_table_columns__(self, table, col_type):
        return self.config[self.source][self.database][table][col_type]

    def get_partition_keys_by_table_name(self, table):
        return self.__get_table_columns__(table, 'partition_keys')

    def get_not_null_columns_list_by_table_name(self, table):
        return self.__get_table_columns__(table, 'not_nullable_columns')

    def get_table_columns_by_table_name(self, table):
        return self.__get_table_columns__(table, 'columns').keys()

    def get_column_data_type_by_table_and_column_name(self, table_name,
                                                      column_name):
        return self.__get_table_columns__(table_name, 'columns')[column_name]['datatype']

    def get_column_length_by_table_and_column_name(self, table_name, column_name):
        return self.__get_table_columns__(table_name, 'columns')[column_name]['field_length']

    def get_columns_type_and_length_by_table(self, table_name):
        return self.__get_table_columns__(table_name, 'columns')

    def get_not_null_columns_for_all_tables(self):
        tables = self.get_table_names_by_database()
        not_null_columns = {table: self.get_not_null_columns_list_by_table_name(table) for table in tables}
        print("Not Null columns...." % not_null_columns)
        return not_null_columns


if __name__ == '__main__':
    obj = JsonCatalogManager("oracle", "intake", "catalog_config.json")
    x = obj.get_not_null_columns_for_all_tables()
    print(x)
