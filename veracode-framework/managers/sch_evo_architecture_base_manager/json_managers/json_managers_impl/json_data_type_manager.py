"""
----------------------------------------------------------------------------------------------------------
Description:

usage: This is an Abstract Class deals with all config files of the project to process and returns the required out put.

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-587

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/014/2020        Zeeshan Mirza        VDL-587              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import json
from utils.file_util import get_config_file
from managers.sch_evo_architecture_base_manager.json_managers.abstract_json_data_type_manager import AbstractJsonDataTypeManager


class JsonDataTypeManager(AbstractJsonDataTypeManager):

    def __init__(self):
        file_name = 'data_type_config.json'
        config_file = get_config_file('schema', file_name)
        self.config = json.loads(config_file)

    def get_all_sources(self):
        """
        Method to get all the sources
        :return: Sources
        """
        return self.config.keys()

    def get_all_source_mappings(self, source_type):
        """
        Method to get all the source data type mappings
        :param source_type: Type of the source
        :return: List of Mappings
        """
        return self.config[source_type].keys()

    def get_source_column_mapping(self, source_type, destination_type, column_type):
        """
        Method to get the column level data type mappings of a source
        :param source_type: Type of the source
        :param destination_type: Type of the destination
        :param column_type: Type of the column
        :return: Column Data Type Mappings
        """
        mappings = self.config[source_type][destination_type]
        for mapping in mappings:
            source_columns = [column.lower() for column in mapping['column_type']]
            if column_type.lower() in source_columns:
                return mapping['mapping']
        return []

    def get_source_column_max_length(self, source_type, column_type):
        """
        Method to get the max length of a source column
        :param source_type: Type of the source
        :param column_type: Type of the column
        :return: 
        """
        columns_max_length = self.config[source_type]['columns_max_length']
        for column in columns_max_length:
            column_types = [column.lower() for column in column['column_type']]
            if column_type.lower() in column_types:
                return column['max_length']
        return -1


if __name__ == '__main__':
    obj = JsonDataTypeManager()
    x = obj.get_all_sources()
    print(x)

    y = obj.get_all_source_mappings('redshift')
    print(y)

    z = obj.get_source_column_mapping('redshift', 'spark', 'bigint')
    print(z)

    a = obj.get_source_column_max_length('redshift', 'float')
    print(a)

