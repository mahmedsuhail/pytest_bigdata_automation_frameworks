"""
----------------------------------------------------------------------------------------------------------
Description:

usage: This is an Abstract Class deals with all config files of the project to process and returns the required out put.

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-587

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/014/2020        Zeeshan Mirza        VDL-587              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import json
from utils.file_util import get_config_file
from managers.sch_evo_architecture_base_manager.json_managers.abstract_json_hop_manager import AbstractJsonHopManager


class JsonHopManager(AbstractJsonHopManager):

    def __init__(self):
        file_name = 'buckets_config.json'
        config_file = get_config_file('schema', file_name)
        self.config = json.loads(config_file)

    def get_all_hop_names(self):
        """
        Method to get the names of all hops
        :return: Names of hops
        """
        return self.config['hops']

    def get_hop_source_path(self, hop):
        """
        Method to get the source path of a hop
        :param hop: Hop Name
        :return: Source Path
        """
        return self.config[hop]['source']

    def get_hop_target_path(self, hop):
        """
        Method to get the target path of a hop
        :param hop: Hop Name
        :return: Target Path
        """
        return self.config[hop]['target']

    def get_athena_results_bucket(self):
        return self.config['athena_results']['bucket']

    def get_athena_results_key(self):
        return self.config['athena_results']['key']

    def get_athena_results_path(self):
        bucket = self.get_athena_results_bucket()
        key = self.get_athena_results_key()
        return f's3://{bucket}/{key}'


if __name__ == '__main__':
    obj = JsonHopManager()
    x = obj.get_all_hop_names()
    print(x)
