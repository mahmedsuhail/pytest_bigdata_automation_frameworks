"""
----------------------------------------------------------------------------------------------------------
Description:

usage: This is an Abstract Class deals with all config files of the project to process and returns the required out put.

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-587

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/01/2019        Zeeshan Mirza        VDL-587              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

from abc import ABC, abstractmethod

class AbstractJsonManager(ABC):
    def __init__(self):
        print()


    def set_source(self, source):
        self.source = source

    @abstractmethod
    def get_database_by_source(self):
        pass

    @abstractmethod
    def get_table_names_by_database(self):
        pass

    @abstractmethod
    def get_target_db_by_source_database(self, stage_name):
        pass

    @abstractmethod
    def get_partition_keys_by_table_name(self, table_name):
        pass

    @abstractmethod
    def get_not_null_columns_list_by_table_name(self, table_name):
        pass

    @abstractmethod
    def get_table_columns_by_table_name(self, table_name):
        pass

    @abstractmethod
    def get_column_data_type_by_table_and_column_name(self, table_name, column_name):
        pass

    @abstractmethod
    def get_column_length_by_table_and_column_name(self, table_name, column_name):
        pass

    @abstractmethod
    def get_columns_type_and_length_by_table(self, table_name):
        pass

    @abstractmethod
    def get_not_null_columns_for_all_tables(self):
        pass
