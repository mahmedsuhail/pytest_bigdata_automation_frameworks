"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Deals with tables row count validation for each hop

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 3
Story   : VDL

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/01/2019        Zeeshan Mirza        VDL-              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

from helpers.athena_helper import athena_helper

class RowCountValidationManager:
    def __init__(self):
        self.athena_helper = athena_helper()
        print()


    def landing_to_historical_row_count_validation(self, resultant_table="", expected_table="" ):
        # create both tables for this hop
        # self.athena_helper.create_database("test_database")
        sql="SELECT (select count(*) from csv_test_db.%s)=(select count(*) " \
            "from parquet_test_db.%s) AS RowCountResult;" %(resultant_table, expected_table)
        print("SQL Query ", sql)
        self.athena_helper.run_query(sql)
        print()


    def __create_table(self, table_name):
        print()


    def __create_test_database(self, database_name):
        print()

