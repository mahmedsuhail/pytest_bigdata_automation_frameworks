"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Deals with column level validation for external tables(using Athena)

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 3
Story   : VDL

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/01/2019        Zeeshan Mirza        VDL-              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

from helpers.athena_helper import athena_helper
from managers.sch_evo_architecture_base_manager.data_integrity_manager.column_manager_base import ColumnManagerBase


class AthenaColumnValidationManager(ColumnManagerBase):
    """
    Deals with column level validation for external tables(using Athena)
    """
    def __init__(self):
        self.athena_helper = athena_helper()

    def column_nullable_validation(self, resultant_db, resultant_table, column_name ):
        """
        Method to validate that there are no null values in a column
        :param resultant_db: The resultant database
        :param resultant_table: The resultant table
        :param column_name: The name of the column
        :return: Result, Test Passes or not
        """
        sql=f'SELECT COUNT("{column_name}") AS count FROM "{resultant_db}"."{resultant_table}" WHERE "{column_name}" IS NULL;'
        print("SQL Query ", sql)
        resultant_frame = self.athena_helper.run_query(sql)

        test_passed = False
        null_count = None
        if not resultant_frame.empty:
            null_count = int(resultant_frame['count'])
            if null_count == 0:
                test_passed = True

        result_str = f"Test Failed : [ Expected Count( 0 ) != Actual Count ({null_count}) ] "
        return test_passed, result_str

    def column_length_validation(self, resultant_db, resultant_table, column, column_max_len):
        """
        Method to validate the data type of a column
        :param resultant_db: The resultant database
        :param resultant_table: The resultant table
        :param column_name: The name of the column
        :param column_max_len: The maximum length of the column
        :return: Result, Test Passes or not
        """
        sql=f'SELECT MAX(LENGTH(CAST("{column}" AS varchar))) AS length FROM "{resultant_db}"."{resultant_table}" ;'
        print("SQL Query ", sql)
        resultant_frame = self.athena_helper.run_query(sql)
        print("Is Frame Empty :: ", str(resultant_frame.empty))
        test_passed = False

        if resultant_frame.empty:
            actual_length = 0
        else:
            actual_length = int(resultant_frame['length'])
        if actual_length < int(column_max_len):
            test_passed = True

        result_str = f"Test Failed : [ Expected Length {column_max_len} != Actual length {actual_length}"
        return test_passed, result_str

    def column_data_type_validation(self, resultant_db, resultant_table, column):
        """
        Method to validate the data type of a column
        :param resultant_db: The resultant database
        :param resultant_table: The resultant table
        :param column_name: The name of the column
        :return: Result, Test Passes or not
        """
        sql = f'SELECT "{column}" FROM "{resultant_db}"."{resultant_table}"'
        print(f"SQL Query: {sql}")
        query_exec_id = self.athena_helper.run_athena_query(sql)['QueryExecutionId']
        response = self.athena_helper.get_query_status_and_output_location(
            query_exec_id)
        # response type {'status': state, 'reason': reason,'output_location': output_location}
        # state = SUCCEEDED / FAILED / CANCELLED
        return response


if __name__ == '__main__':
    obj = AthenaColumnValidationManager()
    db = 'staging_test_db'
    table = 'staging_order_2020_01_02_15_03_32_143384'
    result = obj.column_data_type_validation(db, table, 'id')
    print(result)
