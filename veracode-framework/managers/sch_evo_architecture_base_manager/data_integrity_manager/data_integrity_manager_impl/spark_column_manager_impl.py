"""
----------------------------------------------------------------------------------------------------------
Description: Pre-req to proceed test cases will be handled

usage: Column manager used for column level validation using Spark engine

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-603

Modification Log:

-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/01/2020        Zeeshan Mirza        VDL-607             Initial draft.

-----------------------------------------------------------------------------------------------------------
"""
import allure
from helpers.spark_helper import SparkHelper
from managers.sch_evo_architecture_base_manager.data_integrity_manager.column_manager_base import ColumnManagerBase


class SparkColumnValidationManager( ColumnManagerBase ):

    def __init__(self, spark_session):
        self.spark_session = spark_session
        self.spark_helper = SparkHelper(spark_session)

    def create_hive_database(self, database_name):
        """
        Method to create the hive database
        :param database_name: Name of the database
        :return: None
        """
        query = f"CREATE DATABASE IF NOT EXISTS {database_name}"
        self.spark_helper.execute_hive_query(query)

    def __get_net_change(self, path):
        """
        Method to get the net change data
        :param path: Path of the files
        :return: Data frame with the net change
        """
        hudi_df = self.spark_helper.load_hudi_data_frame(path)
        remove_cols = ['_hoodie_commit_time', '_hoodie_commit_seqno',
                       '_hoodie_record_key', '_hoodie_partition_path',
                       '_hoodie_file_name']
        # using this approach in case order of these columns change
        select_cols = hudi_df.columns
        for col in remove_cols:
            select_cols.remove( col )
        # other option
        # select_cols = hudi_df.columns[5:]
        commit_time_col = '_hoodie_commit_time'
        max_value = self.spark_helper.get_column_max_value( hudi_df, commit_time_col )
        return hudi_df.select( select_cols ).where( f'{commit_time_col} = {max_value}' )

    def create_cache_temp_table(self, table_name, path):
        """
        Method to cache the temp table
        :param table_name: The name of the table
        :param path: Path of the files
        :return: None
        """
        if not self.spark_helper.is_temp_table_exist(table_name):
            net_change_df = self.__get_net_change(path)
            self.spark_helper.create_temp_table(net_change_df, table_name)
        if not self.spark_helper.is_table_cached(table_name):
            self.spark_helper.cache_table(table_name)

    def create_hive_table(self, database_name, table_name, src_path, target_path):
        """
        Method to create a hive table
        :param database_name: Name of the database
        :param table_name: Name of the table
        :param src_path: Source Path
        :param target_path: Target Path
        :return: None
        """
        net_change_df = self.spark_helper.load_data_frame(src_path, "parquet")
        self.spark_helper.write_dataframe_create_table(net_change_df, target_path, database_name, table_name)

    @allure.step("Verify Column Data Type")
    def column_data_type_validation(self, resultant_db, resultant_table, column_name, expected_data_types):
        """
        Method to validate the data type of a column
        :param resultant_db: The resultant database
        :param resultant_table: The resultant table
        :param column_name: The name of the column
        :param expected_data_types: Expected datatypes of the column
        :return: Result, Test Passes or not
        """
        with allure.step(
                f"Expected Data Types for column_name {column_name} are {expected_data_types}"):
            pass

        data_type = self.spark_helper.get_column_data_type(resultant_db, resultant_table, column_name)

        with allure.step(
                f"Actual Data Type for column_name {column_name} is {data_type}"):
            pass

        test_passed = False
        expected_data_types = [_type.lower() for _type in expected_data_types]
        if data_type.lower() in expected_data_types:
            test_passed = True

        result_str = f"Test Failed : [ Expected Data Type( {expected_data_types} ) == Actual Data Type ({data_type}) ] "

        return test_passed, result_str

    @allure.step("Verify Not Null Column")
    def column_nullable_validation(self, resultant_db, resultant_table, column_name):
        """
        Method to validate that the column has no null values
        :param resultant_db: The resultant database
        :param resultant_table: The resultant table
        :param column_name: The name of the column
        :return: Result, Test Passed or not
        """
        with allure.step( "Expected Count of NULL values for column %s is : 0" % (
                resultant_table + "." + column_name) ):
            pass

            null_count = self.spark_helper.get_column_null_count(resultant_db, resultant_table, column_name)

        with allure.step( f"Count Of NULL Values For Column {resultant_table}.{column_name} is : {null_count}" ):
            pass

        test_passed = False
        if null_count == 0:
            test_passed = True

        result_str = f"Test Failed : [ Expected Count( 0 ) == Actual Count ({null_count}) ] "

        return test_passed, result_str

    @allure.step("Verify Column Length")
    def column_length_validation(self, resultant_db, resultant_table, column_name, column_max_len):
        """
        Method to validate that the column has no null values
        :param resultant_db: The resultant database
        :param resultant_table: The resultant table
        :param column_name: The name of the column
        :param column_max_len: The maximum length of the column
        :return: Result, Test Passed or not
        """
        with allure.step(
                f"Expected maximum length of column {column_name} is  {column_max_len-1}"):
            pass

        column_length = self.spark_helper.get_column_max_length(resultant_db, resultant_table, column_name)
        with allure.step(
                f"Actual maximum length of column {column_name} is  {column_length}"):
            pass

        test_passed = False
        if column_length < column_max_len:
            test_passed = True

        result_str = f"Test Failed : [ Expected column max length ({column_max_len-1}) > " \
                     f"Actual column max length ({column_length}) ] "

        return test_passed, result_str
