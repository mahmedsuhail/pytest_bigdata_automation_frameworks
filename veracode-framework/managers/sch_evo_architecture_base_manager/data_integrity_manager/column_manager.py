"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Deals with table columns not null validation for each hop

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 3
Story   : VDL

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/01/2019        Zeeshan Mirza        VDL-              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import boto3

from helpers.athena_helper import athena_helper

class ColumnValidationManager:
    def __init__(self):
        self.athena_helper = athena_helper()
        print()


    def column_nullable_validation(self, resultant_table, resultant_db, column_name ):
        # create both tables for this hop
        # self.athena_helper.create_database("test_database")
        sql="SELECT %s from %s.%s where %s is Null limit 1;" %(column_name, resultant_db, resultant_table, column_name)
        print("SQL Query ", sql)
        resultant_frame = self.athena_helper.run_query(sql)
        print(resultant_frame)
        if resultant_frame.empty:
            result = True
        else:
            result = False
        return result
        # print()

    def column_length_validation(self, resultant_table, resultant_db, column_max_len ):
        sql="SELECT MAX(LENGTH(dept_no)) as length FROM parquet_test_db.extra group by dept_no order by length desc limit 1"
        print("SQL Query ", sql)
        self.athena_helper.run_query(sql)

    def column_data_type_validation(self, resultant_table, resultant_db, column_data_type ):
        sql = ""
        print("SQL Query ", sql)
        self.athena_helper.run_query(sql)


    def __create_table(self, table_name):
        print()


    def __create_test_database(self, database_name):
        print()

if __name__ == '__main__':
    obj = ColumnValidationManager()
    result = obj.column_nullable_validation("extra", "csv_test_db", "dept_no")