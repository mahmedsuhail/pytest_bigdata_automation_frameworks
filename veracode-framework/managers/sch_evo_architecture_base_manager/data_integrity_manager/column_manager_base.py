"""
----------------------------------------------------------------------------------------------------------
Description: Pre-req to proceed test cases will be handled

usage: This is an Abstract Class will be used for column level validation

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-603

Modification Log:

-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/01/2020        Zeeshan Mirza        VDL-607             Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

from abc import ABC, abstractmethod

class ColumnManagerBase(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def column_data_type_validation(self, resultant_db, resultant_table, column):
        ''''''

    @abstractmethod
    def column_nullable_validation(self, resultant_table, resultant_db, column_name):
        ''''''

    @abstractmethod
    def column_length_validation(self, resultant_db, resultant_table, resultant_table_column, column_max_len):
        ''''''
