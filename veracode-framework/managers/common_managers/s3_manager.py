"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Deals with any s3 operations

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-587

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/01/2019        Zeeshan Mirza        VDL-587              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

from helpers.s3_helper import S3

class s3_landing_data():

    def __init__(self):
        self.s3_helper = S3()
        self.bucket_name = "nbs-test-veracode"
        self.etl_timestamp = "2020-01-02_15:03:32.143384"
        self.etl_path = ("vosp/raw/intake/%s/" % (self.etl_timestamp))
        self.target_etl_path = ("vosp/staging/intake/%s/" % (self.etl_timestamp))
        self.catalog_config_path = "config/%s"


    def get_list_of_tables_by_etl_timestamp(self, etll):
        # datalake - qa - 101 - landing - data
        # tables = s3_helper.get_bucket_folders( bucket="nbs-test-veracode")
        print(etll)
        tables = self.s3_helper.list_folders(bucket="nbs-test-veracode", prefix=self.etl_path)
        return tables
        # print(tables)

    def get_config_file(self, file_name):
        key = (self.catalog_config_path % file_name)
        return self.s3_helper.get_object(bucket=self.bucket_name, key=key)

    def main(self):
        return self.get_list_of_tables_by_etl_timestamp()


if __name__ == '__main__':
    s3 = s3_landing_data()
    print(s3.main())
    # s3.get_list_of_tables_by_etl_timestamp()

