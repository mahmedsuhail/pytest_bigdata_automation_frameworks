"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Deals with any Athena operations

Author  : Usama Abbas
Release : 1
#Sprint  : 2
Story   :

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
11/01/2020        Usama Abbas        VDL-XYZ              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

from helpers.athena_helper import athena_helper
from utils import string_util


class AthenaManager():
    """
    Deals with any Athena operations
    """

    def __init__(self):
        """
        constructor
        """
        self.athena_helper = athena_helper()

    def create_database(self, database_name):
        """
        create external database/schema
        :param database_name: example staging_test_db
        :return: response of create database/schema athena query
        """
        return self.athena_helper.delete_then_create_database(database_name)

    def create_table(self, database, table_name, columns, location, file_type):
        """
        create external table in glue using athena query
        :param database: example staging_test_db
        :param table_name: example analysis_unit_scan_window
        :param columns: list of columns along with their data types of a table
        :param location: S3 path of path for given table
        :param file_type: file type of data for which table needs to be created  example: csv / parquet
        :return: response of create table athena query
        """
        table_name = string_util.format_table_name(table_name)
        return self.athena_helper.create_table(database, table_name,columns, location,
                                        file_type)

    def create_table_as(self, database, table_name, source_table_name, columns, file_type):
        table_name = string_util.format_table_name(table_name)
        source_table_name = string_util.format_table_name(source_table_name)

        source_sql = f"WHERE \"_hoodie_commit_time\" = (SELECT max(\"_hoodie_commit_time\") FROM \"{database}\".\"{source_table_name}\") ; "
        external_location = f"s3://nbs-test-veracode/Hudi_tables/{table_name}"

        return self.athena_helper.create_table_as(database, table_name, source_table_name,
            columns, external_location, file_type, source_sql)


if __name__ == '__main__':
    am = AthenaManager()
    #res = am.athena_helper.get_query_status_and_output_location(exec_id)
    #print(res)
