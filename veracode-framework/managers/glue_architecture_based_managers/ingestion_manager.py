"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Ingestion Validation - Unstructured

Author  : Adil Qayyum
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Adil Qayyum        VDL-247              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import allure

from helpers import s3_helper
from helpers.allure_helper import *
from helpers.sns_helper import *
from helpers.unstructured_log_helper import *

s3_obj = s3_helper.S3()
athena_obj = athena_helper()

@allure.step("Getting log count from bucket")
def get_log_count(bucket, value='Null', exclusion='N'):
    logs_objects = s3_obj.get_s3_objects(bucket, value=value, exclusion=exclusion)
    logging.info(logs_objects[1])
    return (logs_objects[1])


def verify_ac_log_count():
    source_count = get_log_count(bucket_client_ac, value=exclusion_value_ac, exclusion='Y')
    s3sync_count = get_log_count(bucket_s3sync_ac+env)
    print("\nAC logs at Source bucket: " + repr(source_count))
    print("AC logs at S3 Sync bucket: " + repr(s3sync_count))
    if source_count == s3sync_count:
        print('AC logs synced correctly!' + "\n")
        return True
    else:
        message = 'AC logs not synced correctly!' + "\n"
        print(message)
        publish_notification(message)
        return False


def verify_scan_log_count():
    source_count = s3_obj.get_all_s3_keys(bucket_client_scan, exclusion_value_scan)
    s3sync_count = get_log_count(bucket_s3sync_scan+env)
    print("\nScan logs at Source bucket: " + repr(source_count[1]))
    print("Scan logs at S3 Sync bucket: " + repr(s3sync_count))
    if source_count == s3sync_count:
        print('Scan logs synced correctly!' + "\n")
        return True
    else:
        message = 'Scan logs not synced correctly!' + "\n"
        print(message)
        publish_notification(message)
        return False


def verify_flaw_log_count():
    logs_at_landing = s3_obj.get_s3_object(bucket_landing+env, 'jobId=')
    logs_at_raw = s3_obj.get_s3_object(bucket_raw+env, 'jobId=', value='$folder$', exclusion='Y')
    print("Flaw logs at Landing: "+repr(logs_at_landing[1]))
    print("Flaw logs at Raw: "+repr(logs_at_raw[1]))

    if logs_at_landing[1] == logs_at_raw[1]:
        return True
    else:
        message = 'Flaw log count not matched between Landing & Raw'
        publish_notification(message)
        return False


def verify_log_landing(s3sync_bucket, landing_bucket, log_type):
    logs_object_s3sync = s3_obj.get_s3_objects(s3sync_bucket+env)
    logs_object_landing = s3_obj.get_s3_object(landing_bucket+env, log_type, value='$folder$', exclusion='Y')
    logs_object_raw = s3_obj.get_s3_object(bucket_raw+env, log_type, value='$folder$', exclusion='Y')

    print("Logs at s3sync: " + repr(logs_object_s3sync[1]))
    print("Logs at landing: " + repr(logs_object_landing[1]))
    print("Logs at raw: " + repr(logs_object_raw[1]) + "\n")


def verify_log_against_key(client_bucket, s3sync_bucket, landing_bucket, key, exclusion_value):
    logs_object_source = s3_obj.get_s3_object(client_bucket, key, value=exclusion_value, exclusion='Y')
    logs_object_s3sync = s3_obj.get_s3_object(s3sync_bucket+env, key)
    logs_object_target = s3_obj.get_s3_object(landing_bucket+env, key, value='$folder$', exclusion='Y')

    print("Logs at source: " + repr(logs_object_source[1]))
    print("Logs at s3sync: " + repr(logs_object_s3sync[1]))
    print("Logs at landing: " + repr(logs_object_target[1]) + "\n")

    if logs_object_s3sync[1] == logs_object_target[1]:
        return True
    else:
        message = 'Data validation failed for unstructured logs for '+key+' and bucket '+landing_bucket+env
        publish_notification(message)
        return False


def verify_ac_log_values():
    result_source = get_ac_source_params(bucket_landing+env, ac_file_name, 'ac-logs', ac_columns_select, ac_columns_where)
    result_target = athena_obj.query_builder(ac_columns_select, ac_columns_where, ac_database_raw+env, ac_table, result_source)
    try:
        output = athena_obj.compare_results(result_target[0], result_target[1], result_target[2], ac_columns_select, ac_table, result_source)
        return output
    except:
        print('No results to compare!')


def verify_scan_log_values():
    result_source = get_scan_source_params(bucket_landing+env, scan_file_name, 'scan-logs', scan_columns_select, scan_columns_where)
    result_target = athena_obj.query_builder(scan_columns_select, scan_columns_where, scan_database_raw+env, scan_table, result_source)
    try:
        output = athena_obj.compare_results(result_target[0], result_target[1], result_target[2], scan_columns_select, scan_table, result_source)
        return output
    except:
        print('No results to compare!')


def verify_flaw_log_values():
    result_source = get_flaw_source_params(bucket_landing+env, flaw_file_name, flaw_columns_select, flaw_columns_where)
    result_target = athena_obj.query_builder(flaw_columns_select, flaw_columns_where, flaw_database_raw+env, flaw_table, result_source)
    try:
        output = athena_obj.compare_results(result_target[0], result_target[1], result_target[2], flaw_columns_select, flaw_table, result_source)
        return output
    except:
        print('No results to compare!')