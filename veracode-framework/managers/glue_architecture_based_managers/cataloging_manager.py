"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Cataloging Validation

Author  : Adil Qayyum
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Adil Qayyum        VDL-247              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

from helpers import glue_helper
from helpers.configurations_helper import *
from helpers.sns_helper import *
from managers.glue_architecture_based_managers import config_manager

glue_obj = glue_helper.Glue()


def verify_ac_logs_catalog(bucket_postfix):
    table_name = glue_obj.get_glue_table_names('ac_logs_'+bucket_postfix+'_'+env)
    print(table_name)
    ac_logs_columns = glue_obj.get_glue_table_column_names('ac_logs_'+bucket_postfix+'_'+env, table_name[0])
    print(ac_logs_columns)

    if bucket_postfix == 'raw':
        ac_logs_catalog = config_manager.get_catalog_from_config('ac_logs_catalog')
        print(ac_logs_catalog)
    else:
        ac_logs_catalog = config_manager.get_catalog_from_config('dim_ac_logs_catalog')
        print(ac_logs_catalog)

    if ac_logs_columns == ac_logs_catalog:
        print('AC logs cataloged correctly!'+ "\n")
        return True
    else:
        message = 'AC logs not cataloged correctly!'+ "\n"
        print(message)
        publish_notification(message)
        return False


def verify_scan_logs_catalog(bucket_postfix):
    table_name = glue_obj.get_glue_table_names('scan_logs_'+bucket_postfix+'_'+env)
    print(table_name)
    scan_logs_columns = glue_obj.get_glue_table_column_names('scan_logs_'+bucket_postfix+'_'+env, table_name[0])
    print(scan_logs_columns)

    if bucket_postfix == 'raw':
        scan_logs_catalog = config_manager.get_catalog_from_config('scan_logs_catalog')
        print(scan_logs_catalog)

    else:
        scan_logs_catalog = config_manager.get_catalog_from_config('dim_scan_logs_catalog')
        print(scan_logs_catalog)

    if scan_logs_columns == scan_logs_catalog:
        print('Scan logs cataloged correctly for '+bucket_postfix+ "\n")
        return True
    else:
        message = 'Scan logs not cataloged correctly for '+bucket_postfix+ "\n"
        print(message)
        publish_notification(message)
        return False


def verify_flaw_logs_catalog(bucket_postfix):
    table_name = glue_obj.get_glue_table_names('flaw_logs_'+bucket_postfix+'_'+env)
    print(table_name)
    flaw_logs_columns = glue_obj.get_glue_table_column_names('flaw_logs_'+bucket_postfix+'_'+env, table_name[0])
    print(flaw_logs_columns)

    if bucket_postfix == 'raw':
        flaw_logs_catalog = config_manager.get_catalog_from_config('flaw_logs_catalog')
        print(flaw_logs_catalog)
    else:
        flaw_logs_catalog = config_manager.get_catalog_from_config('dim_flaw_logs_catalog')
        print(flaw_logs_catalog)

    if flaw_logs_columns == flaw_logs_catalog:
        print('Flaw logs cataloged correctly for '+bucket_postfix+ "\n")
        return True
    else:
        message = 'Flaw logs not cataloged correctly for '+bucket_postfix+ "\n"
        print(message)
        publish_notification(message)
        return False


def verify_curation_table_catalog(table_name):
    curation_table_columns = glue_obj.get_glue_table_column_names(env+'_curation_db', table_name)
    print(curation_table_columns)

    curation_table_catalog = config_manager.get_catalog_from_config(table_name + '_catalog')
    print(curation_table_catalog)

    if curation_table_columns == curation_table_catalog:
        print(table_name+' table cataloged correctly!'+ "\n")
        return True
    else:
        message = table_name+' table not cataloged correctly!'+ "\n"
        publish_notification(message)
        print(message)
        return False


def verify_raw_table_catalog(database ,table_name):
    raw_table_columns = glue_obj.get_glue_table_column_names(database, table_name)
    print(raw_table_columns)

    raw_table_catalog = config_manager.get_catalog_from_config(table_name + '_catalog')
    print(raw_table_catalog)

    if raw_table_columns == raw_table_catalog:
        print(table_name+' table cataloged correctly!'+ "\n")
        return True
    else:
        message = table_name+' table not cataloged correctly!'+ "\n"
        publish_notification(message)
        print(message)
        return False