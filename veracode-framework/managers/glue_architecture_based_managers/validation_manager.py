"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Structured Validation Manager

Author  : Usman Zahid
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Usman Zahid        VDL-247              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import csv
import pandas as pd



class validation_manager:

    # this functions helps to match the two dataframes
    def compare_dataframes(self, source_result, target_result):
        result = source_result.equals(target_result)
        return result

    # To get mismatched records from csv file comparison
    def get_unmatched_results_from_csv(self,source_results,target_results):
        # saving the results in CSV file for fining the unmatched resutls
        source_results.to_csv(r'source_dataframe.csv', index=None, header=True)
        target_results.to_csv(r'target_dataframe.csv', index=None, header=True)

        # start comparison on both results to extract missing records
        with open('target_dataframe.csv', 'rt', encoding='utf-8') as target:
            master_indices = dict((r[0], i) for i, r in enumerate(csv.reader(target)))

            with open('source_dataframe.csv', 'rt', encoding='utf-8') as source:
                with open('varient_results.csv', 'w') as results:
                    reader = csv.reader(source)
                    writer = csv.writer(results)

                    writer.writerow(next(reader, []) + ['RESULTS'])

                    for row in reader:
                        index = master_indices.get(row[0])
                        if index is not None:
                            True
                        else:
                            message = 'NOT FOUND in target results'
                            writer.writerow(row + [message])

        results.close()
        unmatched_results=pd.read_csv('varient_results.csv')
        return unmatched_results

    # to get mismatched results from two different dataframes
    def get_mismatched_records(self,source_results,target_results):
        unmatched_records = pd.concat([source_results, target_results]).drop_duplicates(keep=False)
        return unmatched_records

    
