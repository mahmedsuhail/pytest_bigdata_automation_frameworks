"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Curation Data Extraction & Validation

Author  : Adil Qayyum
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Adil Qayyum        VDL-247              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

from helpers.athena_helper import *
from helpers.sns_helper import *

curated_db = get_structured_curated_database()

athena_obj = athena_helper()


def curation_verification(database, raw_query, curated_query):
    result_identity = get_structured_raw_data(database)
    result_raw = athena_obj.exec_query_athena(raw_query, result_identity['database'])
    result_curated = athena_obj.exec_query_athena(curated_query, curated_db)

    print(result_raw)
    print(result_curated)

    if result_raw.equals(result_curated):
        return True
    else:
        publish_notification("Curation Data not matched for "+raw_query)
        return False


def ac_curation_verification():
    athena_obj.exec_query_athena(ac_create_view, ac_database_curated+env)
    result_raw = athena_obj.exec_query_athena(ac_raw, ac_database_raw+env)
    result_curated = athena_obj.exec_query_athena(ac_curated, ac_database_curated+env)
    athena_obj.exec_query_athena(ac_drop_view, ac_database_curated+env)

    print(result_raw)
    print(result_curated)

    if result_raw.equals(result_curated):
        return True
    else:
        publish_notification("Curation Data not matched for "+ac_raw)
        return False


def scan_curation_verification():
    athena_obj.exec_query_athena(scan_create_view, scan_database_curated + env)
    result_raw = athena_obj.exec_query_athena(scan_raw, scan_database_raw+env)
    result_curated = athena_obj.exec_query_athena(scan_curated, scan_database_curated+env)
    athena_obj.exec_query_athena(scan_drop_view, scan_database_curated + env)

    print(result_raw)
    print(result_curated)

    if result_raw.equals(result_curated):
        return True
    else:
        publish_notification("Curation Data not matched for "+scan_raw)
        return False


def flaw_curation_verification():
    athena_obj.exec_query_athena(flaw_create_view, flaw_database_curated + env)
    result_raw = athena_obj.exec_query_athena(flaw_raw, flaw_database_raw+env)
    result_curated = athena_obj.exec_query_athena(flaw_curated, flaw_database_curated+env)
    athena_obj.exec_query_athena(flaw_drop_view, flaw_database_curated + env)

    print(result_raw)
    print(result_curated)

    if result_raw.equals(result_curated):
        return True
    else:
        publish_notification("Curation Data not matched for "+flaw_raw)
        return False