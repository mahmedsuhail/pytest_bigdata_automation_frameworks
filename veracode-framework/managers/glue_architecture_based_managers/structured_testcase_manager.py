"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Testcase container for structured data

Author  : Usman Zahid
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/15/2019        Usman Zahid        VDL-247              Initial draft.
03/25/2019        Usman Zahid        VDL-287              Feedback incorporated
-----------------------------------------------------------------------------------------------------------
"""


from helpers.athena_helper import athena_helper
from helpers.configurations_helper import *
from helpers.db_helper import db_helper
from helpers.other_helper_utilities import get_json_contents
from managers.glue_architecture_based_managers.validation_manager import validation_manager


class get_sql_queries:
    def create_sql_queries_for_data_validation(self, query_json_key, is_join=False, post_query_condition ='', target_db_postfix='', source_db_postfix='' ,hop='source vs raw'):
        json_cls_obj=get_json_contents()
        sql_query_list=json_cls_obj.read_json(query_json_key)
        # print(sql_query_list)
        if hop == 'source vs raw':
            if is_join is False:
                for i in range(0, len(sql_query_list), 5):
                    get_query_filter = self.get_column_end_date(sql_query_list[i],sql_query_list[i+1])
                    column_name=get_query_filter[0]
                    source_query = "select distinct  " + sql_query_list[i +2] +"  from  " +sql_query_list[i] + source_db_postfix +"."+ sql_query_list[i+1] + " " + "where" + " " + column_name + ">=" + "'" + \
                                   get_query_filter[
                        1] + "'" + " " + "and" + " " + column_name + "<=" + "'" + get_query_filter[2] + "'" + ' ' \
                                   + str(post_query_condition) + ';'

                    if column_name=='id' or column_name=='Id' or column_name=='ID':
                        target_query = "select distinct  " + sql_query_list[i +3] +"  from  " + sql_query_list[i] + target_db_postfix + "." + sql_query_list[i + 1] + " " + "where" + " " + column_name + " >= " \
                                       + get_query_filter[1] + " " + "and" + " " + column_name + \
                                       " <= " + get_query_filter[2] + ' ' + \
                                       str(post_query_condition) + ';'
                    else:
                        target_query = "select distinct  " + sql_query_list[i +3] +"  from  " + sql_query_list[i] + target_db_postfix + "." + sql_query_list[i + 1] + " " + "where" + " " + column_name + ">=  cast(" + "'" + \
                                       get_query_filter[1] + "'" + " as Timestamp)  " + "and" + " " + column_name + \
                                       "<= cast(" + "'" + get_query_filter[2] + "'" + " as Timestamp)" + ' ' + \
                                       str(post_query_condition) + ';'
                    #
                    print(source_query)
                    print(target_query)
            elif is_join is True:
                for i in range(0, len(sql_query_list), 6):
                    get_query_filter = self.get_column_end_date(sql_query_list[i],sql_query_list[i+1])
                    if get_query_filter[0]=='agora_findingservice.status.modified_date|agora_findingservice.status.finding_' \
                                            'id|agora_findingservice.finding.id' or get_query_filter[0]\
                            =='agora_findingservice.status.modified_date|agora_findingservice.status.latest' \
                              '_scan|agora_findingservice.scan.id':
                        column_name='modified_date'
                    else:
                        column_name = get_query_filter[0]
                    source_query = "select distinct  " + sql_query_list[i +2] +"  from  " + sql_query_list[i + 5] + " " + "where" + " " + "t2." + column_name + ">=" + "'" + \
                                   get_query_filter[
                        1] + "'" + " " + "and" + " " + "t2." + column_name + "<=" + "'" + get_query_filter[2] + "'" + ' ' \
                                   + str(post_query_condition) + ';'
                    target_query = "select distinct  " + sql_query_list[i +3] +"  from  " + sql_query_list[i] + target_db_postfix + "." + sql_query_list[i + 1] + " " + "where" + " " + column_name + ">=  cast(" + "'" + \
                                   get_query_filter[1] + "'" + " as Timestamp)  " + "and" + " " + column_name + "<= cast" \
                                                                                                                "(" + "'" \
                                   + get_query_filter[2] + "'" + " as Timestamp)" + ' ' + str(post_query_condition) +  ';'
                    print(source_query)
                    print(target_query)
            else:
                print("There is some problem while creating queries or join arrgument is not passed while calling "
                      "the get_sql_queries_from_config_file() function ")


            return source_query,target_query

        elif hop=='source vs landing':
            if is_join is False:
                for i in range(0, len(sql_query_list), 5):
                    get_query_filter = self.get_column_end_date(sql_query_list[i],sql_query_list[i+1])
                    column_name=get_query_filter[0]
                    source_query = "select distinct  " + sql_query_list[i +2] +"  from  " +sql_query_list[i] + source_db_postfix +"."+ sql_query_list[i+1] + " " + "where" + " " + column_name + ">=" + "'" + \
                                   get_query_filter[
                        1] + "'" + " " + "and" + " " + column_name + "<=" + "'" + get_query_filter[2] + "'" + ' ' \
                                   + str(post_query_condition) + ';'

                    target_query = "select distinct  " + sql_query_list[i +3] +"  from  " + sql_query_list[i] + target_db_postfix + "." + sql_query_list[i + 1] + " " + "where" + " REPLACE(" + column_name + ",'\"','')  >= '" \
                                   + get_query_filter[1] + "' " + "and" + " " + " REPLACE(" + column_name + ",'\"','') <= '" + get_query_filter[2] + "'" + \
                                   str(post_query_condition) + ";"
                    print(source_query)
                    print(target_query)
            elif is_join is True:
                for i in range(0, len(sql_query_list), 6):
                    get_query_filter = self.get_column_end_date(sql_query_list[i],sql_query_list[i+1])
                    if get_query_filter[0]=='agora_findingservice.status.modified_date|agora_findingservice.status.finding_' \
                                            'id|agora_findingservice.finding.id' or get_query_filter[0]\
                            =='agora_findingservice.status.modified_date|agora_findingservice.status.latest' \
                              '_scan|agora_findingservice.scan.id':
                        column_name='modified_date'
                    else:
                        column_name = get_query_filter[0]
                    source_query = "select distinct  " + sql_query_list[i +2] +"  from  " + sql_query_list[i + 5] + " " + "where" + " " + "t2." + column_name + ">=" + "'" + \
                                   get_query_filter[
                        1] + "'" + " " + "and" + " " + "t2." + column_name + "<=" + "'" + get_query_filter[2] + "'" + ' ' \
                                   + str(post_query_condition) + ';'
                    target_query = "select distinct  " + sql_query_list[i +3] +"  from  " + sql_query_list[i] + target_db_postfix + "." + sql_query_list[i + 1] + " " + "where" + "  REPLACE(" + column_name + ",'\"','') >=  " + "'" + \
                                   get_query_filter[1] + "' "  "and" + " REPLACE(" + column_name + ",'\"','') <= " \
                                                                                                                + "'" \
                                   + get_query_filter[2] + "' " + ' ' + str(post_query_condition) +  ';'
                    print(source_query)
                    print(target_query)
            else:
                print("There is some problem while creating queries or join arrgument is not passed while calling "
                      "the get_sql_queries_from_config_file() function ")


            return source_query,target_query

        else:
            for i in range(0, len(sql_query_list), 5):
                get_query_filter = self.get_column_end_date(sql_query_list[i],sql_query_list[i+1])
                column_name = get_query_filter[0]
                source_query = "select distinct  " + sql_query_list[i + 2] + "  from  " + sql_query_list[i] + source_db_postfix + "." + \
                               sql_query_list[i + 1] + " " + "where" + " REPLACE(" + column_name + ",'\"','') >=  " + "'" + \
                                   get_query_filter[1] + "'  " + "and" + " REPLACE(" + column_name + ",'\"','') <=  " + "'" + get_query_filter[2] + "'" + ' ' + \
                                   str(post_query_condition) + ';'

                if column_name == 'id' or column_name == 'Id' or column_name == 'ID':
                    target_query = "select distinct  " + sql_query_list[i + 3] + "  from  " + sql_query_list[
                        i] + target_db_postfix + "." + sql_query_list[i + 1] + " " + "where" + " " + column_name + " >= " \
                                   + get_query_filter[1] + " " + "and" + " " + column_name + \
                                   " <= " + get_query_filter[2] + ' ' + \
                                   str(post_query_condition) + ';'
                else:
                    target_query = "select distinct  " + sql_query_list[i + 2] + "  from  " + sql_query_list[
                        i] + target_db_postfix + "." + sql_query_list[
                                       i + 1] + " " + "where" + " " + column_name + ">=  cast(" + "'" + \
                                   get_query_filter[1] + "'" + " as Timestamp)  " + "and" + " " + column_name + \
                                   "<= cast(" + "'" + get_query_filter[2] + "'" + " as Timestamp)" + ' ' + \
                                   str(post_query_condition) + ';'
                #
                print(source_query)
                print(target_query)
                return source_query, target_query

    def create_sql_queries_for_count_validation(self, query_json_key, is_join=False, post_query_condition ='', target_db_postfix='', source_db_postfix='', hop='source vs raw'):
        json_cls_obj=get_json_contents()
        sql_query_list=json_cls_obj.read_json(query_json_key)
        # print(sql_query_list)
        if hop == 'source vs raw':
            if is_join is False:
                for i in range(0, len(sql_query_list), 5):
                    get_query_filter = self.get_column_end_date(sql_query_list[i],sql_query_list[i+1])
                    column_name=get_query_filter[0]
                    source_query = "select distinct count(*) from  " + sql_query_list[i] + source_db_postfix +"."+ sql_query_list[i+1] + " " + "where" + " " + column_name + ">=" + "'" + \
                                   get_query_filter[
                        1] + "'" + " " + "and" + " " + column_name + "<=" + "'" + get_query_filter[2] + "'" + ' ' \
                                   + post_query_condition + ';'

                    if column_name=='id' or column_name=='Id' or column_name=='ID':
                        target_query = "select distinct count(*) from  " + sql_query_list[i] + str(target_db_postfix) + "." + sql_query_list[i + 1] + " " + "where" + " " + column_name + " >= " \
                                       + get_query_filter[1] + " " + "and" + " " + column_name + \
                                       " <= " + get_query_filter[2] + ' ' + \
                                       post_query_condition + ';'
                    else:
                        target_query = "select distinct count(*) from  " + sql_query_list[i] + str(target_db_postfix) + "." + sql_query_list[i + 1] + " " + "where" + " " + column_name + ">=  cast(" + "'" + \
                                       get_query_filter[1] + "'" + " as Timestamp)  " + "and" + " " + column_name + \
                                       "<= cast(" + "'" + get_query_filter[2] + "'" + " as Timestamp)" + ' ' + \
                                       post_query_condition + ';'
                    #
                    print(source_query)
                    print(target_query)
            elif is_join is True:
                for i in range(0, len(sql_query_list), 6):
                    get_query_filter = self.get_column_end_date(sql_query_list[i],sql_query_list[i+1])
                    if get_query_filter[0]=='agora_findingservice.status.modified_date|agora_findingservice.status.finding_' \
                                            'id|agora_findingservice.finding.id' or get_query_filter[0]\
                            =='agora_findingservice.status.modified_date|agora_findingservice.status.latest' \
                              '_scan|agora_findingservice.scan.id':
                        column_name='modified_date'
                    else:
                        column_name = get_query_filter[0]
                    source_query = "select distinct count(*) from  " + sql_query_list[i+5] + " " + "where" + " " + "t2." + column_name + ">=" + "'" + \
                                   get_query_filter[
                        1] + "'" + " " + "and" + " " + "t2." + column_name + "<=" + "'" + get_query_filter[2] + "'" + ' ' \
                                   + post_query_condition + "  " + ';'
                    target_query = "select distinct count(*) from  " + sql_query_list[i] + str(target_db_postfix) + "." + sql_query_list[i + 1] + " " + "where" + " " + column_name + ">=  cast(" + "'" + \
                                   get_query_filter[1] + "'" + " as Timestamp)  " + "and" + " " + column_name + "<= cast" \
                                                                                                                "(" + "'" \
                                   + get_query_filter[2] + "'" + " as Timestamp)" + ' ' + post_query_condition + "  " + ';'
                    print(source_query)
                    print(target_query)

            else:
                print("There is some problem while creating queries or join arrgument is not passed while calling "
                      "the get_sql_queries_from_config_file() function ")


            return source_query,target_query

        elif hop == 'source vs landing':
            if is_join is False:
                for i in range(0, len(sql_query_list), 5):
                    get_query_filter = self.get_column_end_date(sql_query_list[i],sql_query_list[i+1])
                    column_name = get_query_filter[0]
                    source_query = "select distinct count(*) from  " + sql_query_list[i] + source_db_postfix + "." + \
                                   sql_query_list[i + 1] + " " + "where" + " " + column_name + ">=" + "'" + \
                                   get_query_filter[
                                       1] + "'" + " " + "and" + " " + column_name + "<=" + "'" + get_query_filter[
                                       2] + "'" + ' ' \
                                   + post_query_condition + ';'
                    target_query = "select distinct count(*) from  " + sql_query_list[i] + str(
                        target_db_postfix) + "." + sql_query_list[
                                       i + 1] + " " + "where" + " REPLACE(" + column_name + ",'\"','')  >= '" \
                                   + get_query_filter[1] + "' " + "and" + " REPLACE(" + column_name + ",'\"','')  <= '" + get_query_filter[2] + "'" + \
                                   post_query_condition + ';'

                    #
                    print(source_query)
                    print(target_query)
            elif is_join is True:
                for i in range(0, len(sql_query_list), 6):
                    get_query_filter = self.get_column_end_date(sql_query_list[i],sql_query_list[i+1])
                    if get_query_filter[
                        0] == 'agora_findingservice.status.modified_date|agora_findingservice.status.finding_' \
                              'id|agora_findingservice.finding.id' or get_query_filter[0] \
                            == 'agora_findingservice.status.modified_date|agora_findingservice.status.latest' \
                               '_scan|agora_findingservice.scan.id':
                        column_name = 'modified_date'
                    else:
                        column_name = get_query_filter[0]
                    source_query = "select distinct count(*) from  " + sql_query_list[
                        i + 5] + " " + "where" + " " + "t2." + column_name + ">=" + "'" + \
                                   get_query_filter[
                                       1] + "'" + " " + "and" + " " + "t2." + column_name + "<=" + "'" + \
                                   get_query_filter[2] + "'" + ' ' \
                                   + post_query_condition + "  " + ';'
                    target_query = "select distinct count(*) from  " + sql_query_list[i] + str(
                        target_db_postfix) + "." + sql_query_list[
                                       i + 1] + " " + "where" + " REPLACE(" + column_name + ",'\"','') >=  " + "'" + \
                                   get_query_filter[
                                       1] + "' "  + "and" + " REPLACE(" + column_name + ",'\"','')  <= " \
                                                                                                   + "'" \
                                   + get_query_filter[
                                       2] + "' "  + ' ' + post_query_condition + "  " + ';'
                    print(source_query)
                    print(target_query)

            else:
                print("There is some problem while creating queries or join arrgument is not passed while calling "
                      "the get_sql_queries_from_config_file() function ")

            return source_query, target_query

        else:
            for i in range(0, len(sql_query_list), 5):
                get_query_filter = self.get_column_end_date(sql_query_list[i],sql_query_list[i+1])
                column_name = get_query_filter[0]
                source_query = "select distinct count(*) "+ "  from  " + sql_query_list[i] + source_db_postfix + "." + \
                               sql_query_list[i + 1] + " " + "where" + " " + column_name + ">=  " + "'" + \
                                   get_query_filter[1] + "' " + "and" + " " + column_name + \
                                    "<=  " + "'" + get_query_filter[2] + "' " +  ' ' + \
                                   str(post_query_condition) + ';'

                if column_name == 'id' or column_name == 'Id' or column_name == 'ID':
                    target_query = "select distinct count(*) " + "  from  " + sql_query_list[
                        i] + target_db_postfix + "." + sql_query_list[i + 1] + " " + "where" + " " + column_name + " >= " \
                                   + get_query_filter[1] + " " + "and" + " " + column_name + \
                                   " <= " + get_query_filter[2] + ' ' + \
                                   str(post_query_condition) + ';'
                else:
                    target_query = "select distinct count(*) " + "  from  " + sql_query_list[
                        i] + target_db_postfix + "." + sql_query_list[
                                       i + 1] + " " + "where" + " " + column_name + ">=  cast(" + "'" + \
                                   get_query_filter[1] + "'" + " as Timestamp)  " + "and" + " " + column_name + \
                                   "<= cast(" + "'" + get_query_filter[2] + "'" + " as Timestamp)" + ' ' + \
                                   str(post_query_condition) + ';'
                #
                print(source_query)
                print(target_query)
                return source_query, target_query


    def get_column_end_date(self,database,table):
        try:
            class_object = db_helper()
            json_cls_object=get_json_contents()
            get_auditing_db_from_config=json_cls_object.read_json("config")
            auditingDB_postfix="_"+env
            query="SELECT source_column, column_start,column_end FROM auditing" + auditingDB_postfix + ".table_marker WHERE source_type='Structured' AND source_table= '"+ database + "." +table+"' ORDER BY id DESC LIMIT 1;"
            get_end_date=class_object.execute_query(query, "auditing")
            result_in_list = get_end_date.values.tolist()[0]
            return result_in_list
        except Exception as e:
            print("There is some error, while getting the info from auditing module \n", e)

class SourceToRawValidation:
    def testcase_container_for_SourceToRaw_count_validation(self, query_json_key, is_join=False):
        json_cls_obj = get_json_contents()
        helper_cls_obj = get_sql_queries()
        get_query_params = json_cls_obj.read_json("config")
        # print("target_db_postfix ",get_query_params[3])
        db_postfix="_" + env
        source_query, target_query  = helper_cls_obj.create_sql_queries_for_count_validation(query_json_key, is_join, "",db_postfix)
        # print(source_query)
        # print(target_query)
        db_class_object = db_helper()
        athena_class_object = athena_helper()
        get_query_results_from_source = db_class_object.execute_query(source_query, 'source')
        print("Source Result \n",get_query_results_from_source)
        get_query_results_from_target = athena_class_object.execute_query(target_query)
        print("Target Result \n",get_query_results_from_target)
        validation_class_object = validation_manager()
        get_comparison_result = validation_class_object.compare_dataframes(get_query_results_from_target,
                                                                           get_query_results_from_source)
        return get_comparison_result

    def testcase_container_for_SourceToRaw_data_validation(self,query_json_key, is_join=False):
        helper_cls_obj = get_sql_queries()
        json_cls_obj = get_json_contents()
        get_query_post_condition = json_cls_obj.read_json(query_json_key)
        get_query_params = json_cls_obj.read_json("config")
        db_postfix = "_" + env
        source_query, target_query  = helper_cls_obj.create_sql_queries_for_data_validation(query_json_key, is_join,
                                                                                            get_query_post_condition[4],db_postfix)
        # print(source_query)
        # print(target_query)
        db_class_object = db_helper()
        athena_class_object = athena_helper()
        get_query_results_from_source = db_class_object.execute_query(source_query, 'source')
        print("Source Result \n",get_query_results_from_source)
        get_query_results_from_target = athena_class_object.execute_query(target_query)
        print("Target Result \n",get_query_results_from_target)
        validation_class_object = validation_manager()
        # if get_query_results_from_source!=None and get_query_results_from_target!=None:
        get_comparison_result = validation_class_object.compare_dataframes(get_query_results_from_target,
                                                                           get_query_results_from_source)
        # save_results_in_csv=validation_class_object.get_unmatched_results_from_csv(get_query_results_from_target,
        #                                                                    get_query_results_from_source)
        get_variance=validation_class_object.get_mismatched_records(get_query_results_from_target,
                                                                           get_query_results_from_source)
        # print(get_variance)
        return get_comparison_result, get_variance



class SourceToLandingValidation:
    def testcase_container_for_SourceToLanding_count_validation(self, query_json_key, get_athena_table_name, is_join=False, post_query_condition =''):
        helper_cls_obj = get_sql_queries()
        json_cls_obj = get_json_contents()
        config_cls_object=get_json_contents()
        db_class_object = db_helper()
        athena_class_object = athena_helper()
        get_query_params = json_cls_obj.read_json(query_json_key)
        get_athena_ddl=json_cls_obj.read_json(get_athena_table_name)
        create_db_query = "create database if not exists " + get_query_params[0] + \
                          json_cls_obj.read_json("landing_config")[0]
        drop_table_query="drop table " + get_query_params[0] + json_cls_obj.read_json("landing_config")[0]+"." + get_query_params[1]
        drop_db_query="drop database " + get_query_params[0] + json_cls_obj.read_json("landing_config")[0]
        create_db = athena_class_object.execute_query(create_db_query, True)
        create_athena_table=athena_class_object.execute_query(get_athena_ddl[0], True)
        source_query, target_query = helper_cls_obj.create_sql_queries_for_count_validation(query_json_key,
                                                                                            is_join,
                                                                                            post_query_condition,
                                                                                            config_cls_object.read_json("landing_config")[0], '', 'source vs landing')
        get_query_results_from_source = db_class_object.execute_query(source_query, 'source')
        print("Source Result \n",get_query_results_from_source)
        get_query_results_from_target = athena_class_object.execute_query(target_query)
        print("Target Result \n",get_query_results_from_target)
        validation_class_object = validation_manager()
        get_comparison_result = validation_class_object.compare_dataframes(get_query_results_from_target,
                                                                           get_query_results_from_source)
        drop_table_from_athena = athena_class_object.execute_query(drop_table_query, True)
        drop_db_from_athena=athena_class_object.execute_query(drop_db_query, True)
        return get_comparison_result


    def testcase_container_for_SourceToLanding_data_validation(self, query_json_key, get_athena_table_name, is_join=False):
        helper_cls_obj = get_sql_queries()
        json_cls_obj = get_json_contents()
        db_class_object = db_helper()
        athena_class_object = athena_helper()
        get_query_params = json_cls_obj.read_json(query_json_key)
        get_athena_ddl = json_cls_obj.read_json(get_athena_table_name)
        create_db_query="create database if not exists " + get_query_params[0] + json_cls_obj.read_json("landing_config")[0]
        drop_table_query = "drop table " + get_query_params[0] + json_cls_obj.read_json("landing_config")[0] + "." + \
                           get_query_params[1]
        drop_db_query = "drop database " + get_query_params[0] + json_cls_obj.read_json("landing_config")[0]
        create_db= athena_class_object.execute_query(create_db_query, True)
        create_athena_table = athena_class_object.execute_query(get_athena_ddl[0], True)
        source_query, target_query = helper_cls_obj.create_sql_queries_for_data_validation(query_json_key,is_join,
                                                                                           get_query_params[4], json_cls_obj.read_json("landing_config")[0], '', 'source vs landing')
        athena_class_object = athena_helper()
        get_query_results_from_source = db_class_object.execute_query(source_query, 'source')
        print("Source Result \n", get_query_results_from_source)
        get_query_results_from_target = athena_class_object.execute_query(target_query)
        print("Target Result \n", get_query_results_from_target)
        validation_class_object = validation_manager()
        # if get_query_results_from_source!=None and get_query_results_from_target!=None:
        get_comparison_result = validation_class_object.compare_dataframes(get_query_results_from_target,
                                                                           get_query_results_from_source)

        get_variance=validation_class_object.get_mismatched_records(get_query_results_from_target,
                                                                           get_query_results_from_source)
        drop_table_from_athena = athena_class_object.execute_query(drop_table_query, True)
        drop_db_from_athena = athena_class_object.execute_query(drop_db_query, True)
        # print(get_variance)
        return get_comparison_result, get_variance


class LandingToRawValidation:

    def testcase_container_for_LandingToRaw_count_validation(self, query_json_key, get_athena_table_ddl, is_join=False):
        helper_cls_obj = get_sql_queries()
        json_cls_obj = get_json_contents()
        config_cls_object = get_json_contents()
        db_class_object = db_helper()
        athena_class_object = athena_helper()
        get_query_params = json_cls_obj.read_json(query_json_key)
        get_athena_ddl= json_cls_obj.read_json(get_athena_table_ddl)
        db_postfix = "_" + env
        create_db_query = "create database if not exists " + get_query_params[0] + \
                          json_cls_obj.read_json("landing_config")[0]
        drop_table_query= "drop table " + get_query_params[0] + json_cls_obj.read_json("landing_config")[0]+'.'+get_query_params[1]
        drop_db_query = "drop database " + get_query_params[0] + json_cls_obj.read_json("landing_config")[0]
        create_db = athena_class_object.execute_query(create_db_query, True)
        create_athena_table = athena_class_object.execute_query(get_athena_ddl[0], True)

        source_query, target_query  = helper_cls_obj.create_sql_queries_for_count_validation(query_json_key,
                                                                                            is_join,
                                                                                            '',
                                                                                             db_postfix,config_cls_object.read_json("landing_config")[0], 'landing vs raw' )
        athena_class_object = athena_helper()
        get_query_results_from_source = athena_class_object.execute_query(source_query)
        print("Source Result \n", get_query_results_from_source)
        get_query_results_from_target = athena_class_object.execute_query(target_query)
        print("Target Result \n", get_query_results_from_target)
        validation_class_object = validation_manager()
        get_comparison_result = validation_class_object.compare_dataframes(get_query_results_from_target,
                                                                           get_query_results_from_source)
        drop_table=athena_class_object.execute_query(drop_table_query, True)
        drop_db_from_athena = athena_class_object.execute_query(drop_db_query, True)
        return get_comparison_result

    def testcase_container_for_LandingToRaw_data_validation(self, query_json_key, get_athena_table_ddl, is_join=False):
        helper_cls_obj = get_sql_queries()
        json_cls_obj = get_json_contents()
        config_cls_object = get_json_contents()
        db_postfix = "_" + env
        athena_class_object = athena_helper()
        get_query_params = json_cls_obj.read_json(query_json_key)
        get_athena_ddl = json_cls_obj.read_json(get_athena_table_ddl)
        create_db_query = "create database if not exists " + get_query_params[0] + \
                          json_cls_obj.read_json("landing_config")[0]
        drop_table_query = "drop table " + get_query_params[0] + json_cls_obj.read_json("landing_config")[0] + '.' + \
                           get_query_params[1]
        drop_db_query = "drop database " + get_query_params[0] + json_cls_obj.read_json("landing_config")[0]
        create_db = athena_class_object.execute_query(create_db_query, True)
        create_athena_table = athena_class_object.execute_query(get_athena_ddl[0], True)
        source_query, target_query = helper_cls_obj.create_sql_queries_for_data_validation(query_json_key,
                                                                                            is_join,
                get_query_params[4], db_postfix, config_cls_object.read_json("landing_config")[0], 'landing vs raw')
        get_query_results_from_source = athena_class_object.execute_query(source_query)
        print("Source Result \n",get_query_results_from_source)
        get_query_results_from_target = athena_class_object.execute_query(target_query)
        print("Target Result \n",get_query_results_from_target)
        validation_class_object = validation_manager()
        # if get_query_results_from_source!=None and get_query_results_from_target!=None:
        get_comparison_result = validation_class_object.compare_dataframes(get_query_results_from_target,
                                                                           get_query_results_from_source)
        get_variance=validation_class_object.get_mismatched_records(get_query_results_from_target,
                                                                           get_query_results_from_source)
        drop_table = athena_class_object.execute_query(drop_table_query, True)
        drop_db_from_athena = athena_class_object.execute_query(drop_db_query, True)
        # print(get_variance)
        return get_comparison_result, get_variance



class CustomValidation:
    def custom_testcase_container_for_source_to_raw_or_curation(self, query_json_key):
        json_cls_obj = get_json_contents()
        db_class_object = db_helper()
        athena_class_object = athena_helper()
        custom_queries = json_cls_obj.read_json(query_json_key)

        get_query_results_from_source = db_class_object.execute_query(custom_queries[0], 'source')
        print("Source Result \n", get_query_results_from_source)
        get_query_results_from_target = athena_class_object.execute_query(custom_queries[1])
        print("Target Result \n", get_query_results_from_target)
        validation_class_object = validation_manager()
        # if get_query_results_from_source!=None and get_query_results_from_target!=None:
        get_comparison_result = validation_class_object.compare_dataframes(get_query_results_from_target,
                                                                           get_query_results_from_source)
        # save_results_in_csv=validation_class_object.get_unmatched_results_from_csv(get_query_results_from_target,
        #                                                                    get_query_results_from_source)
        get_variance = validation_class_object.get_mismatched_records(get_query_results_from_target,
                                                                      get_query_results_from_source)
        # print(get_variance)
        return get_comparison_result, get_variance

    def custom_testcase_container_for_source_to_landing(self, query_json_key):
        json_cls_obj = get_json_contents()
        db_class_object = db_helper()
        athena_class_object = athena_helper()
        custom_queries = json_cls_obj.read_json(query_json_key)

        get_query_results_from_source = db_class_object.execute_query(custom_queries[0], 'source')
        print("Source Result \n", get_query_results_from_source)
        get_query_results_from_target = athena_class_object.execute_query(custom_queries[1])
        print("Target Result \n", get_query_results_from_target)
        validation_class_object = validation_manager()
        # if get_query_results_from_source!=None and get_query_results_from_target!=None:
        get_comparison_result = validation_class_object.compare_dataframes(get_query_results_from_target,
                                                                           get_query_results_from_source)
        # save_results_in_csv=validation_class_object.get_unmatched_results_from_csv(get_query_results_from_target,
        #                                                                    get_query_results_from_source)
        get_variance = validation_class_object.get_mismatched_records(get_query_results_from_target,
                                                                      get_query_results_from_source)
        # print(get_variance)
        return get_comparison_result, get_variance


    def custom_testcase_container_for_athena_to_athena(self, query_json_key):
        json_cls_obj = get_json_contents()
        athena_class_object = athena_helper()
        custom_queries = json_cls_obj.read_json(query_json_key)

        get_query_results_from_source = athena_class_object.execute_query(custom_queries[0])
        print("Source Result \n", get_query_results_from_source)
        get_query_results_from_target = athena_class_object.execute_query(custom_queries[1])
        print("Target Result \n", get_query_results_from_target)
        validation_class_object = validation_manager()
        # if get_query_results_from_source!=None and get_query_results_from_target!=None:
        get_comparison_result = validation_class_object.compare_dataframes(get_query_results_from_target,
                                                                           get_query_results_from_source)
        # save_results_in_csv=validation_class_object.get_unmatched_results_from_csv(get_query_results_from_target,
        #                                                                    get_query_results_from_source)
        get_variance = validation_class_object.get_mismatched_records(get_query_results_from_target,
                                                                      get_query_results_from_source)
        # print(get_variance)
        return get_comparison_result, get_variance