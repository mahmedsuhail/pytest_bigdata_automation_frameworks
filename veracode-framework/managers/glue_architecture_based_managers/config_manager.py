"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Configuration Manager

Author  : Adil Qayyum
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Adil Qayyum        VDL-247              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import boto3
import ast

s3 = boto3.resource('s3')
content_object = s3.Object('veracode-qa-testing-artifacts', 'automation_config/Config.json')
file_content = content_object.get()['Body'].read().decode('utf-8')

content_object = s3.Object('veracode-qa-testing-artifacts', 'automation_config/catalog_config.json')
catalog_file_content = content_object.get()['Body'].read().decode('utf-8')


def get_catalog_from_config(log_type, content=catalog_file_content):
    final_content = ast.literal_eval(content)
    return final_content[log_type]


def get_bucket_name(bucket, content=file_content):
    final_content = ast.literal_eval(content)
    return final_content['buckets'][bucket]


def get_exclusions(log_type, content=file_content):
    final_content = ast.literal_eval(content)
    return final_content['exclusions'][log_type]


def get_databases(db, content=file_content):
    final_content = ast.literal_eval(content)
    return final_content['databases'][db]


def get_keys(key, content=file_content):
    final_content = ast.literal_eval(content)
    return final_content['keys'][key]


def get_select_columns(log_type, content=file_content):
    final_content = ast.literal_eval(content)
    return final_content[log_type]['select']


def get_where_columns(log_type, content=file_content):
    final_content = ast.literal_eval(content)
    return final_content[log_type]['where']


def get_unstructured_query(log_type, query_type, content=file_content):
    final_content = ast.literal_eval(content)
    return final_content[log_type][query_type]


def get_structured_raw_data(data_source, content=file_content):
    final_content = ast.literal_eval(content)
    return final_content['structured_raw_queries'][data_source]


def get_structured_curated_data(data_source, content=file_content):
    final_content = ast.literal_eval(content)
    return final_content['structured_curated_queries'][data_source]


def get_structured_curated_database(content=file_content):
    final_content = ast.literal_eval(content)
    return final_content['structured_curated_queries']['database']