"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Not null column Validation

Author  : Zeeshan Mirza
Release : 1
Sprint  : 4
Story   : VDL-612

Modification Log:

-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/08/2020        Zeeshan Mirza        VDL-612             Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import sqlparse
import json

from ddlparse.ddlparse import DdlParse

from managers.sch_evo_architecture_base_manager.json_managers.json_managers_impl.json_data_type_manager import JsonDataTypeManager

jsonDataTypeManager = JsonDataTypeManager()


def main():

    file_path = "./configs/sch_evo_architecture_based_config/intake_schema_ddl.sql"

    create_statement = "CREATE TABLE IF NOT EXISTS"

    ddl_info_dict = dict()
    create_statement_list = []
    database = dict()
    list_of_tables = []

    source_dict = dict()
    database_list = []
    database["sources"] = database_list
    source_dict["oracle"] = database

    with open (file_path) as ddl_file:
        sql = ddl_file.read()
        ddls = sqlparse.split(sql)

        target_db_dict = dict()
        for ddl in ddls:
            result_ddl = ddl.split(';')
            for statment in result_ddl:
                tmp_table_match = (statment.strip()).startswith(create_statement)
                if tmp_table_match:
                    create_statement_list.append(statment)
                    table = DdlParse().parse( ddl=statment, source_database=DdlParse.DATABASE.redshift )
                    if table.schema not in database:
                        database[table.schema] = dict()
                        database[table.schema]["tables"] = [table.name]
                        database_list.append(table.schema)
                    else:
                        (database[table.schema]["tables"]).append(table.name)

                    if "target_db" not in database[table.schema]:
                        database[table.schema]["target_db"] = get_target_db_dict()

                    database[table.schema][table.name] = populate_table_column_dict(table)
                    database[table.schema][table.name]["partition_keys"] = get_table_partition_key_list( table )
                    database[table.schema][table.name]["not_nullable_columns"] = get_table_not_null_columns_list(table)
    write_json_config(source_dict)


def write_json_config(source_dict):
    # now write output to a file
    path = './configs/sch_evo_architecture_based_config/catalog_config.json'
    data_file = open(path, "w")
    # magic happens here to make it pretty-printed
    data_file.write(json.dumps(source_dict, indent=4))
    data_file.close()


def get_table_partition_key_list(table):
    #TODO: partition keys are yet to be decide from client end
    return []


def get_table_not_null_columns_list(table):
    col_list = []
    for col in table.columns.values():
        if col.not_null:
            col_list.append(col.name)
    return col_list


def get_target_db_dict():
    target_db_dict = dict()
    target_db_dict['staging'] = "staging_test_db"
    target_db_dict['snapshot'] = "snapshot_test_db"
    return target_db_dict


def populate_table_column_dict(table):
    columns_dict = dict()
    columns_dict["columns"] = dict()
    for col in table.columns.values():
        columns_dict["columns"][col.name] = dict()
        columns_dict["columns"][col.name]["datatype"] = col.data_type
        columns_dict["columns"][col.name]["field_length"] = col.length
        columns_dict["columns"][col.name]["precision"] = col.precision
        columns_dict["columns"][col.name]["scale"] = col.scale

        if col.precision and col.scale:
            columns_dict["columns"][col.name]["field_length"] = col.precision+col.scale+1
        elif col.length is None:
            length = jsonDataTypeManager.get_source_column_max_length("redshift", col.data_type)
            columns_dict["columns"][col.name]["field_length"] = length
    return columns_dict


if __name__ == "__main__":
    main()
