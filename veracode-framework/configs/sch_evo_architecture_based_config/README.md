## Test Framework Config

### Config files
There are three levels of config that apply to our testing framework:
* Catalog config: Contains information of our data sources
    - This config file in automatically generated by a script "create_catalog_config.py". This script takes the DDL path as input and outputs this config file
* Hop config: Contains information of our hop level paths
* Data Type config: Contains column data type mappings

### Catalog Config Structure Details
```json
{
    "source_name":{
        "sources": ["list of databases"],
        "database_name": { // dictionary objects of all the tables in the database
            "tables": ["list of tables"],
            "target_db": {
                "staging": "name of the database for cataloging for staging layer",
                "snapshot":"name of the database for cataloging for snapshot layer"
            },
            "table_name":{
                "columns":{ // dictionary objects of all the columns
                     "column_name": {
                        "datatype": "data type of column",
                        "field_length": "length of column field",
                        "precision": "in case of decimal value, number of digits before decimal point",
                        "scale": "in case of decimal value, number of digits right to the decimal point"
                    }
                },
                "partition_keys":["list of partition keys"],
                "not_nullable_columns":["list of columns that can't be null"]
            }
        } 
    }
}
```

### Buckets Config Structure Details
```json
{
    "hops":["list of all hops"],
    "hop_name":{
        "source": "s3 source path",
        "target": "s3 target path"
    },
    "athena_results":{
		"bucket":"bucket_name",
		"key":"path where athena query results are stored"
	}
}
```


### Data Type Config Structure Details
```json
{
    "source_engine":{ // source engine
        "columns_max_length":[ // list of dictionaries of column types
            {
                "column_type": ["list of column data types"],
                "max_length": "max length of the column"
            }
        ],
        "destination_engine": [ // mapping of source egine column types to mentioned engine
            {
                "column_type": ["list of column data types"],
                "mapping": ["list of column mappings for given column"]
            }
        ]
    }
}
```
