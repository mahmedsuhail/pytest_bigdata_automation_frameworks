-- Drop table

-- DROP TABLE analysis_unit_dyn_summary;

--DROP TABLE intake.analysis_unit_dyn_summary;
CREATE TABLE IF NOT EXISTS intake.analysis_unit_dyn_summary
(
	analysis_unit_dyn_summary_id BIGINT NOT NULL  ENCODE RAW
	,analysis_unit_id BIGINT NOT NULL  ENCODE zstd
	,links_visited BIGINT NOT NULL  ENCODE zstd
	,vulnerable_links BIGINT NOT NULL  ENCODE zstd
	,logged_in SMALLINT   ENCODE lzo
	,valid_ssl SMALLINT   ENCODE lzo
	,scan_date_time TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,scanned_ip VARCHAR(8000)   ENCODE zstd
	,scanned_port BIGINT NOT NULL  ENCODE zstd
	,web_server_detected VARCHAR(256)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,scan_end_time TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,uncrawled_links BIGINT NOT NULL DEFAULT 0 ENCODE zstd
	,PRIMARY KEY (analysis_unit_dyn_summary_id)
)
DISTSTYLE KEY
 DISTKEY (analysis_unit_dyn_summary_id)
 SORTKEY (
	analysis_unit_dyn_summary_id
	)
;
ALTER TABLE intake.analysis_unit_dyn_summary owner to vcoderoot;

-- Drop table

-- DROP TABLE analysis_unit_pub_issue;

--DROP TABLE intake.analysis_unit_pub_issue;
CREATE TABLE IF NOT EXISTS intake.analysis_unit_pub_issue
(
	analysis_unit_pub_issue_id NUMERIC(18,0) NOT NULL  ENCODE RAW
	,analysis_unit_id NUMERIC(18,0) NOT NULL  ENCODE RAW
	,pub_issue_type NUMERIC(18,0) NOT NULL  ENCODE RAW
	,details VARCHAR(1024)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(256) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,PRIMARY KEY (analysis_unit_pub_issue_id)
)
DISTSTYLE KEY
 DISTKEY (analysis_unit_pub_issue_id)
 SORTKEY (
	analysis_unit_pub_issue_id
	, analysis_unit_id
	, pub_issue_type
	)
;
ALTER TABLE intake.analysis_unit_pub_issue owner to vcoderoot;

-- Drop table

-- DROP TABLE analysis_unit_scan_window;

--DROP TABLE intake.analysis_unit_scan_window;
CREATE TABLE IF NOT EXISTS intake.analysis_unit_scan_window
(
	analysis_unit_scan_window_id BIGINT NOT NULL  ENCODE lzo
	,analysis_unit_id BIGINT NOT NULL  ENCODE lzo
	,start_time TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,end_time TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,run_immediately SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,duration BIGINT  DEFAULT 0 ENCODE RAW
	,PRIMARY KEY (analysis_unit_scan_window_id)
)
DISTSTYLE ALL
 SORTKEY (
	duration
	)
;
ALTER TABLE intake.analysis_unit_scan_window owner to vcoderoot;

-- Drop table

-- DROP TABLE app_src_file;

--DROP TABLE intake.app_src_file;
CREATE TABLE IF NOT EXISTS intake.app_src_file
(
	app_src_file_id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,app_id NUMERIC(20,0)   ENCODE RAW
	,filename_hashcode NUMERIC(38,0)   ENCODE zstd
	,filename VARCHAR(4000)   ENCODE zstd
	,local_path VARCHAR(4000)   ENCODE zstd
	,ref_guid VARCHAR(32)   ENCODE zstd
	,platform_generated SMALLINT   ENCODE zstd
	,analysis_type NUMERIC(20,0)   ENCODE zstd
	,sanitized SMALLINT   ENCODE zstd
	,dyn_type NUMERIC(20,0)   ENCODE zstd
	,crawl_status NUMERIC(20,0)   ENCODE zstd
	,orig_exec_unit_ver_id NUMERIC(20,0)   ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,PRIMARY KEY (app_src_file_id)
)
DISTSTYLE KEY
 DISTKEY (app_src_file_id)
 SORTKEY (
	app_src_file_id
	, app_id
	)
;
ALTER TABLE intake.app_src_file owner to etl_user;

-- Drop table

-- DROP TABLE code;

--DROP TABLE intake.code;
CREATE TABLE IF NOT EXISTS intake.code
(
	code_id BIGINT NOT NULL  ENCODE RAW
	,code_val VARCHAR(512) NOT NULL  ENCODE lzo
	,code_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,code_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num BIGINT NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,PRIMARY KEY (code_id)
)
DISTSTYLE ALL
 SORTKEY (
	code_id
	)
;
ALTER TABLE intake.code owner to vcoderoot;

-- Drop table

-- DROP TABLE config_property;

--DROP TABLE intake.config_property;
CREATE TABLE IF NOT EXISTS intake.config_property
(
	config_property_id BIGINT NOT NULL  ENCODE RAW
	,config_property_name VARCHAR(8000)   ENCODE lzo
	,config_property_value VARCHAR(4096)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,PRIMARY KEY (config_property_id)
)
DISTSTYLE ALL
 SORTKEY (
	config_property_id
	)
;
ALTER TABLE intake.config_property owner to vcoderoot;

-- Drop table

-- DROP TABLE customer_rec;

--DROP TABLE intake.customer_rec;
CREATE TABLE IF NOT EXISTS intake.customer_rec
(
	account_id BIGINT   ENCODE lzo
	,account_name VARCHAR(512)   ENCODE lzo
	,nas_dir VARCHAR(512)   ENCODE lzo
	,is_internal SMALLINT   ENCODE lzo
	,login_enabled SMALLINT   ENCODE lzo
	,industry_vertical_id BIGINT   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(512)   ENCODE lzo
	,record_ver BIGINT   ENCODE lzo
	,deleted SMALLINT   ENCODE lzo
	,allow_os SMALLINT   ENCODE lzo
	,account_type BIGINT   ENCODE lzo
	,manager_id BIGINT   ENCODE lzo
	,sales_lead VARCHAR(512)   ENCODE lzo
	,has_branding_image SMALLINT   ENCODE lzo
	,account_status INTEGER   ENCODE lzo
	,creator_login_account_id BIGINT   ENCODE lzo
	,creator_account_id BIGINT   ENCODE lzo
	,enable_learn_managers SMALLINT   ENCODE lzo
	,learn_group_id BIGINT   ENCODE lzo
	,learn_course_selection_id BIGINT   ENCODE lzo
	,external_account_id VARCHAR(512)   ENCODE lzo
	,default_course_visibility BIGINT   ENCODE lzo
	,sales_lead_email VARCHAR(512)   ENCODE lzo
	,sa_name VARCHAR(512)   ENCODE lzo
	,sa_email VARCHAR(512)   ENCODE lzo
	,csm_name VARCHAR(512)   ENCODE lzo
	,csm_email VARCHAR(512)   ENCODE lzo
	,cots_agree BIGINT   ENCODE lzo
	,cots_agree_id BIGINT   ENCODE lzo
	,cots_agree_date TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,last_contracted_id BIGINT   ENCODE lzo
	,last_contracted_date TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,deleted_files SMALLINT   ENCODE lzo
	,is_test SMALLINT   ENCODE lzo
	,vendor_type BIGINT   ENCODE lzo
	,auto_pub_disabled SMALLINT   ENCODE lzo
	,machine_group_id BIGINT   ENCODE lzo
	,dynamic_scan_type BIGINT   ENCODE lzo
	,sanitized SMALLINT   ENCODE lzo
	,saml_enabled SMALLINT   ENCODE lzo
	,keep_binary_days BIGINT   ENCODE lzo
	,dyn_scan_req_aprvl SMALLINT   ENCODE lzo
	,enable_custom_templates SMALLINT   ENCODE lzo
	,allow_adv_dyn_scan_opt SMALLINT   ENCODE lzo
	,self_assign_curriculum SMALLINT   ENCODE lzo
	,suppress_preflight_login_error SMALLINT   ENCODE lzo
	,birst_group_name VARCHAR(600)   ENCODE lzo
	,readout_state BIGINT   ENCODE lzo
	,dyn_auto_publish SMALLINT   ENCODE lzo
	,discovery_scan SMALLINT   ENCODE lzo
	,dynamicmp_scan SMALLINT   ENCODE lzo
	,manual_scan SMALLINT   ENCODE lzo
	,vsa_enabled SMALLINT   ENCODE lzo
	,vsa_scan_req_aprvl SMALLINT   ENCODE lzo
	,waf_rules_generation_enabled SMALLINT   ENCODE lzo
	,dynamic_machine_group_id BIGINT   ENCODE lzo
	,sandbox_enabled SMALLINT   ENCODE lzo
	,concurrent_sandbox_scan_limit BIGINT   ENCODE lzo
	,remediation_scan SMALLINT   ENCODE lzo
	,expand_ears SMALLINT   ENCODE lzo
	,vendor_rescan SMALLINT   ENCODE lzo
	,dyn_prescan_err_enable_submit SMALLINT   ENCODE lzo
	,custom_severity SMALLINT   ENCODE lzo
	,pov_in_progress SMALLINT   ENCODE lzo
	,disco_auto_publish SMALLINT   ENCODE lzo
	,vendor_directory_enabled SMALLINT   ENCODE lzo
	,sca_enabled SMALLINT   ENCODE lzo
	,fingerprint_enabled SMALLINT   ENCODE lzo
	,auto_scan_default SMALLINT   ENCODE lzo
	,hide_template_errors SMALLINT   ENCODE lzo
	,vendor_display_name VARCHAR(512)   ENCODE lzo
	,vendor_contact_name VARCHAR(512)   ENCODE lzo
	,vendor_contact_email VARCHAR(512)   ENCODE lzo
	,asset_inventory_enabled SMALLINT   ENCODE lzo
	,delay_rollout_months BIGINT   ENCODE lzo
	,vendor_contact_job_title VARCHAR(512)   ENCODE lzo
	,is_vpn_enabled SMALLINT   ENCODE lzo
	,dyn_login_err_enable_submit SMALLINT   ENCODE lzo
	,associate_name VARCHAR(512)   ENCODE lzo
	,associate_email VARCHAR(512)   ENCODE lzo
	,api_credential_ttl BIGINT   ENCODE lzo
	,static_auto_pub_setting BIGINT   ENCODE lzo
	,auto_pub_size_threshold_mb BIGINT   ENCODE lzo
	,is_scan_funded_by_vendor SMALLINT   ENCODE lzo
	,custom_cleanser_setting BIGINT   ENCODE lzo
	,looker_group_space_id NUMERIC(20,0)   ENCODE lzo
)
DISTSTYLE EVEN
;
ALTER TABLE intake.customer_rec owner to etl_user;

-- Drop table

-- DROP TABLE display_text;

--DROP TABLE intake.display_text;
CREATE TABLE IF NOT EXISTS intake.display_text
(
	display_text_id NUMERIC(18,0) NOT NULL  ENCODE RAW
	,display_text VARCHAR(4000)   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,PRIMARY KEY (display_text_id)
)
DISTSTYLE KEY
 DISTKEY (display_text_id)
 SORTKEY (
	display_text_id
	)
;
ALTER TABLE intake.display_text owner to etl_user;

-- Drop table

-- DROP TABLE display_text_old;

--DROP TABLE intake.display_text_old;
CREATE TABLE IF NOT EXISTS intake.display_text_old
(
	display_text_id NUMERIC(18,0) NOT NULL  ENCODE RAW
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,display_text VARCHAR(4000)   ENCODE lzo
)
DISTSTYLE KEY
 DISTKEY (display_text_id)
 SORTKEY (
	display_text_id
	)
;
ALTER TABLE intake.display_text_old owner to etl_user;

-- Drop table

-- DROP TABLE dynamic_analysis_scan;

--DROP TABLE intake.dynamic_analysis_scan;
CREATE TABLE IF NOT EXISTS intake.dynamic_analysis_scan
(
	dynamic_analysis_scan_id BIGINT NOT NULL  ENCODE RAW
	,onewas_name VARCHAR(256) NOT NULL  ENCODE lzo
	,analysis_occurrence_id VARCHAR(256) NOT NULL  ENCODE lzo
	,dynamicmp_job_id BIGINT   ENCODE lzo
	,app_id BIGINT   ENCODE RAW
	,login_account_id BIGINT   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(256) NOT NULL  ENCODE lzo
	,record_ver BIGINT NOT NULL DEFAULT 1 ENCODE lzo
	,dynamic_analysis_status SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,PRIMARY KEY (dynamic_analysis_scan_id)
)
DISTSTYLE KEY
 DISTKEY (dynamic_analysis_scan_id)
 SORTKEY (
	dynamic_analysis_scan_id
	, app_id
	)
;
ALTER TABLE intake.dynamic_analysis_scan owner to vcoderoot;

-- Drop table

-- DROP TABLE dynamic_analysis_scan_job;

--DROP TABLE intake.dynamic_analysis_scan_job;
CREATE TABLE IF NOT EXISTS intake.dynamic_analysis_scan_job
(
	dynamic_analysis_scan_job_id BIGINT NOT NULL  ENCODE RAW
	,exec_unit_ver_id BIGINT NOT NULL  ENCODE RAW
	,run_ver_id NUMERIC(18,0) NOT NULL  ENCODE lzo
	,dynamic_analysis_scan_id BIGINT NOT NULL  ENCODE RAW
	,scan_occurrence_id VARCHAR(256) NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(256) NOT NULL  ENCODE lzo
	,record_ver BIGINT NOT NULL DEFAULT 1 ENCODE lzo
	,alternator_scan SMALLINT   ENCODE lzo
	,PRIMARY KEY (dynamic_analysis_scan_job_id)
)
DISTSTYLE KEY
 DISTKEY (dynamic_analysis_scan_job_id)
 SORTKEY (
	dynamic_analysis_scan_job_id
	, dynamic_analysis_scan_id
	, exec_unit_ver_id
	)
;
ALTER TABLE intake.dynamic_analysis_scan_job owner to vcoderoot;

-- Drop table

-- DROP TABLE engine_phase_stats;

--DROP TABLE intake.engine_phase_stats;
CREATE TABLE IF NOT EXISTS intake.engine_phase_stats
(
	engine_phase_stats_id NUMERIC(20,0) NOT NULL  ENCODE lzo
	,phase NUMERIC(20,0)   ENCODE lzo
	,completed SMALLINT   ENCODE lzo
	,run_time NUMERIC(20,0)   ENCODE lzo
	,high_mem_bytes NUMERIC(20,0)   ENCODE lzo
	,high_vm_bytes NUMERIC(20,0)   ENCODE lzo
	,exec_unit_ver_id NUMERIC(20,0)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(256)   ENCODE lzo
	,record_ver NUMERIC(38,18)   ENCODE lzo
	,PRIMARY KEY (engine_phase_stats_id)
)
DISTSTYLE AUTO
;
ALTER TABLE intake.engine_phase_stats owner to etl_user;

-- Drop table

-- DROP TABLE enum_accountsharereltype;

--DROP TABLE intake.enum_accountsharereltype;
CREATE TABLE IF NOT EXISTS intake.enum_accountsharereltype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_accountsharereltype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_accountstatus;

--DROP TABLE intake.enum_accountstatus;
CREATE TABLE IF NOT EXISTS intake.enum_accountstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_accountstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_accounttype;

--DROP TABLE intake.enum_accounttype;
CREATE TABLE IF NOT EXISTS intake.enum_accounttype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_accounttype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_aclpermission;

--DROP TABLE intake.enum_aclpermission;
CREATE TABLE IF NOT EXISTS intake.enum_aclpermission
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_aclpermission owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_aclreason;

--DROP TABLE intake.enum_aclreason;
CREATE TABLE IF NOT EXISTS intake.enum_aclreason
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_aclreason owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_aclresourcetype;

--DROP TABLE intake.enum_aclresourcetype;
CREATE TABLE IF NOT EXISTS intake.enum_aclresourcetype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_aclresourcetype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_aclsubjecttype;

--DROP TABLE intake.enum_aclsubjecttype;
CREATE TABLE IF NOT EXISTS intake.enum_aclsubjecttype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_aclsubjecttype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_adminstate;

--DROP TABLE intake.enum_adminstate;
CREATE TABLE IF NOT EXISTS intake.enum_adminstate
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_adminstate owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_analysistype;

--DROP TABLE intake.enum_analysistype;
CREATE TABLE IF NOT EXISTS intake.enum_analysistype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE KEY
 DISTKEY (enum_id)
 SORTKEY (
	enum_id
	)
;
ALTER TABLE intake.enum_analysistype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_appdyntype;

--DROP TABLE intake.enum_appdyntype;
CREATE TABLE IF NOT EXISTS intake.enum_appdyntype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_appdyntype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_appendixentrytype;

--DROP TABLE intake.enum_appendixentrytype;
CREATE TABLE IF NOT EXISTS intake.enum_appendixentrytype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_appendixentrytype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_appremediationstatus;

--DROP TABLE intake.enum_appremediationstatus;
CREATE TABLE IF NOT EXISTS intake.enum_appremediationstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_appremediationstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_appserverjobstatus;

--DROP TABLE intake.enum_appserverjobstatus;
CREATE TABLE IF NOT EXISTS intake.enum_appserverjobstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_appserverjobstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_appserverjobtype;

--DROP TABLE intake.enum_appserverjobtype;
CREATE TABLE IF NOT EXISTS intake.enum_appserverjobtype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_appserverjobtype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_appsizedefinition;

--DROP TABLE intake.enum_appsizedefinition;
CREATE TABLE IF NOT EXISTS intake.enum_appsizedefinition
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_appsizedefinition owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_assessmenttype;

--DROP TABLE intake.enum_assessmenttype;
CREATE TABLE IF NOT EXISTS intake.enum_assessmenttype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_assessmenttype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_asyncreporttype;

--DROP TABLE intake.enum_asyncreporttype;
CREATE TABLE IF NOT EXISTS intake.enum_asyncreporttype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_asyncreporttype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_auditaction;

--DROP TABLE intake.enum_auditaction;
CREATE TABLE IF NOT EXISTS intake.enum_auditaction
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_auditaction owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_audittype;

--DROP TABLE intake.enum_audittype;
CREATE TABLE IF NOT EXISTS intake.enum_audittype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_audittype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_aufixedflawstate;

--DROP TABLE intake.enum_aufixedflawstate;
CREATE TABLE IF NOT EXISTS intake.enum_aufixedflawstate
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_aufixedflawstate owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_autopubstatus;

--DROP TABLE intake.enum_autopubstatus;
CREATE TABLE IF NOT EXISTS intake.enum_autopubstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_autopubstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_brandinglogotype;

--DROP TABLE intake.enum_brandinglogotype;
CREATE TABLE IF NOT EXISTS intake.enum_brandinglogotype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_brandinglogotype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_businesscriticality;

--DROP TABLE intake.enum_businesscriticality;
CREATE TABLE IF NOT EXISTS intake.enum_businesscriticality
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_businesscriticality owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_codelanguage;

--DROP TABLE intake.enum_codelanguage;
CREATE TABLE IF NOT EXISTS intake.enum_codelanguage
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_codelanguage owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_communicationtype;

--DROP TABLE intake.enum_communicationtype;
CREATE TABLE IF NOT EXISTS intake.enum_communicationtype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_communicationtype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_cookieoverridetype;

--DROP TABLE intake.enum_cookieoverridetype;
CREATE TABLE IF NOT EXISTS intake.enum_cookieoverridetype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_cookieoverridetype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_coursestatus;

--DROP TABLE intake.enum_coursestatus;
CREATE TABLE IF NOT EXISTS intake.enum_coursestatus
(
	enum_id SMALLINT NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num SMALLINT NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_coursestatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_coursevisibility;

--DROP TABLE intake.enum_coursevisibility;
CREATE TABLE IF NOT EXISTS intake.enum_coursevisibility
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_coursevisibility owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_crawlfiletype;

--DROP TABLE intake.enum_crawlfiletype;
CREATE TABLE IF NOT EXISTS intake.enum_crawlfiletype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_crawlfiletype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_crawlstatus;

--DROP TABLE intake.enum_crawlstatus;
CREATE TABLE IF NOT EXISTS intake.enum_crawlstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_crawlstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_customcleansersetting;

--DROP TABLE intake.enum_customcleansersetting;
CREATE TABLE IF NOT EXISTS intake.enum_customcleansersetting
(
	enum_id SMALLINT NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num SMALLINT NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_customcleansersetting owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_customreportformat;

--DROP TABLE intake.enum_customreportformat;
CREATE TABLE IF NOT EXISTS intake.enum_customreportformat
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_customreportformat owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_customreporttype;

--DROP TABLE intake.enum_customreporttype;
CREATE TABLE IF NOT EXISTS intake.enum_customreporttype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_customreporttype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_definedtechnologytype;

--DROP TABLE intake.enum_definedtechnologytype;
CREATE TABLE IF NOT EXISTS intake.enum_definedtechnologytype
(
	enum_id SMALLINT NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num SMALLINT NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_definedtechnologytype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_deleteditemtype;

--DROP TABLE intake.enum_deleteditemtype;
CREATE TABLE IF NOT EXISTS intake.enum_deleteditemtype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_deleteditemtype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_deploymentmethod;

--DROP TABLE intake.enum_deploymentmethod;
CREATE TABLE IF NOT EXISTS intake.enum_deploymentmethod
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_deploymentmethod owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_directoryrestrictions;

--DROP TABLE intake.enum_directoryrestrictions;
CREATE TABLE IF NOT EXISTS intake.enum_directoryrestrictions
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_directoryrestrictions owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_dirtype;

--DROP TABLE intake.enum_dirtype;
CREATE TABLE IF NOT EXISTS intake.enum_dirtype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_dirtype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_discoveryconfidencelevel;

--DROP TABLE intake.enum_discoveryconfidencelevel;
CREATE TABLE IF NOT EXISTS intake.enum_discoveryconfidencelevel
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_discoveryconfidencelevel owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_discoverydynmpcandidate;

--DROP TABLE intake.enum_discoverydynmpcandidate;
CREATE TABLE IF NOT EXISTS intake.enum_discoverydynmpcandidate
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_discoverydynmpcandidate owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_discoveryhttpvshttps;

--DROP TABLE intake.enum_discoveryhttpvshttps;
CREATE TABLE IF NOT EXISTS intake.enum_discoveryhttpvshttps
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_discoveryhttpvshttps owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_discoveryininputdomain;

--DROP TABLE intake.enum_discoveryininputdomain;
CREATE TABLE IF NOT EXISTS intake.enum_discoveryininputdomain
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_discoveryininputdomain owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_discoveryresponsecode;

--DROP TABLE intake.enum_discoveryresponsecode;
CREATE TABLE IF NOT EXISTS intake.enum_discoveryresponsecode
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_discoveryresponsecode owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_discoverystatus;

--DROP TABLE intake.enum_discoverystatus;
CREATE TABLE IF NOT EXISTS intake.enum_discoverystatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_discoverystatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_dynamicmpstatus;

--DROP TABLE intake.enum_dynamicmpstatus;
CREATE TABLE IF NOT EXISTS intake.enum_dynamicmpstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_dynamicmpstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_dynamicprescanmessage;

--DROP TABLE intake.enum_dynamicprescanmessage;
CREATE TABLE IF NOT EXISTS intake.enum_dynamicprescanmessage
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_dynamicprescanmessage owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_dynamicrescantype;

--DROP TABLE intake.enum_dynamicrescantype;
CREATE TABLE IF NOT EXISTS intake.enum_dynamicrescantype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_dynamicrescantype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_dynamicscantype;

--DROP TABLE intake.enum_dynamicscantype;
CREATE TABLE IF NOT EXISTS intake.enum_dynamicscantype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_dynamicscantype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_dynamictemplatesource;

--DROP TABLE intake.enum_dynamictemplatesource;
CREATE TABLE IF NOT EXISTS intake.enum_dynamictemplatesource
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_dynamictemplatesource owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_electionstate;

--DROP TABLE intake.enum_electionstate;
CREATE TABLE IF NOT EXISTS intake.enum_electionstate
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_electionstate owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_enginejobop;

--DROP TABLE intake.enum_enginejobop;
CREATE TABLE IF NOT EXISTS intake.enum_enginejobop
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_enginejobop owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_enginemachineop;

--DROP TABLE intake.enum_enginemachineop;
CREATE TABLE IF NOT EXISTS intake.enum_enginemachineop
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_enginemachineop owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_enginephase;

--DROP TABLE intake.enum_enginephase;
CREATE TABLE IF NOT EXISTS intake.enum_enginephase
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_enginephase owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_enginephasereport;

--DROP TABLE intake.enum_enginephasereport;
CREATE TABLE IF NOT EXISTS intake.enum_enginephasereport
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_enginephasereport owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_etatype;

--DROP TABLE intake.enum_etatype;
CREATE TABLE IF NOT EXISTS intake.enum_etatype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_etatype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_exploitdifficulty;

--DROP TABLE intake.enum_exploitdifficulty;
CREATE TABLE IF NOT EXISTS intake.enum_exploitdifficulty
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_exploitdifficulty owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_externalnonvisiblerole;

--DROP TABLE intake.enum_externalnonvisiblerole;
CREATE TABLE IF NOT EXISTS intake.enum_externalnonvisiblerole
(
	enum_id SMALLINT NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num SMALLINT NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_externalnonvisiblerole owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_feature;

--DROP TABLE intake.enum_feature;
CREATE TABLE IF NOT EXISTS intake.enum_feature
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_feature owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_featuregroup;

--DROP TABLE intake.enum_featuregroup;
CREATE TABLE IF NOT EXISTS intake.enum_featuregroup
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_featuregroup owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_filecleanuptype;

--DROP TABLE intake.enum_filecleanuptype;
CREATE TABLE IF NOT EXISTS intake.enum_filecleanuptype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_filecleanuptype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_flawinfostate;

--DROP TABLE intake.enum_flawinfostate;
CREATE TABLE IF NOT EXISTS intake.enum_flawinfostate
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_flawinfostate owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_flawstatus;

--DROP TABLE intake.enum_flawstatus;
CREATE TABLE IF NOT EXISTS intake.enum_flawstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_flawstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_flawviewerversionpopup;

--DROP TABLE intake.enum_flawviewerversionpopup;
CREATE TABLE IF NOT EXISTS intake.enum_flawviewerversionpopup
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_flawviewerversionpopup owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_imageformat;

--DROP TABLE intake.enum_imageformat;
CREATE TABLE IF NOT EXISTS intake.enum_imageformat
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_imageformat owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_indexstate;

--DROP TABLE intake.enum_indexstate;
CREATE TABLE IF NOT EXISTS intake.enum_indexstate
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_indexstate owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_internalreason;

--DROP TABLE intake.enum_internalreason;
CREATE TABLE IF NOT EXISTS intake.enum_internalreason
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_internalreason owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_issueexploitability;

--DROP TABLE intake.enum_issueexploitability;
CREATE TABLE IF NOT EXISTS intake.enum_issueexploitability
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_issueexploitability owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_issuehistorystate;

--DROP TABLE intake.enum_issuehistorystate;
CREATE TABLE IF NOT EXISTS intake.enum_issuehistorystate
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_issuehistorystate owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_issuelevel;

--DROP TABLE intake.enum_issuelevel;
CREATE TABLE IF NOT EXISTS intake.enum_issuelevel
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_issuelevel owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_issueseverity;

--DROP TABLE intake.enum_issueseverity;
CREATE TABLE IF NOT EXISTS intake.enum_issueseverity
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_issueseverity owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_issuestate;

--DROP TABLE intake.enum_issuestate;
CREATE TABLE IF NOT EXISTS intake.enum_issuestate
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_issuestate owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_issuetype;

--DROP TABLE intake.enum_issuetype;
CREATE TABLE IF NOT EXISTS intake.enum_issuetype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_issuetype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_jobcategoryvalues;

--DROP TABLE intake.enum_jobcategoryvalues;
CREATE TABLE IF NOT EXISTS intake.enum_jobcategoryvalues
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_jobcategoryvalues owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_jobleveltype;

--DROP TABLE intake.enum_jobleveltype;
CREATE TABLE IF NOT EXISTS intake.enum_jobleveltype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE KEY
 DISTKEY (enum_id)
 SORTKEY (
	enum_id
	)
;
ALTER TABLE intake.enum_jobleveltype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_jobstatus;

--DROP TABLE intake.enum_jobstatus;
CREATE TABLE IF NOT EXISTS intake.enum_jobstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_jobstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_jobsubmittedvalues;

--DROP TABLE intake.enum_jobsubmittedvalues;
CREATE TABLE IF NOT EXISTS intake.enum_jobsubmittedvalues
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_jobsubmittedvalues owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_jobtype;

--DROP TABLE intake.enum_jobtype;
CREATE TABLE IF NOT EXISTS intake.enum_jobtype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_jobtype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_jobtypevalues;

--DROP TABLE intake.enum_jobtypevalues;
CREATE TABLE IF NOT EXISTS intake.enum_jobtypevalues
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_jobtypevalues owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_lastattempt;

--DROP TABLE intake.enum_lastattempt;
CREATE TABLE IF NOT EXISTS intake.enum_lastattempt
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_lastattempt owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_legalagreestate;

--DROP TABLE intake.enum_legalagreestate;
CREATE TABLE IF NOT EXISTS intake.enum_legalagreestate
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_legalagreestate owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_linkappjobstatus;

--DROP TABLE intake.enum_linkappjobstatus;
CREATE TABLE IF NOT EXISTS intake.enum_linkappjobstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_linkappjobstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_linkexchangechildtype;

--DROP TABLE intake.enum_linkexchangechildtype;
CREATE TABLE IF NOT EXISTS intake.enum_linkexchangechildtype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_linkexchangechildtype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_loginaccounttype;

--DROP TABLE intake.enum_loginaccounttype;
CREATE TABLE IF NOT EXISTS intake.enum_loginaccounttype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_loginaccounttype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_logoutdetectortype;

--DROP TABLE intake.enum_logoutdetectortype;
CREATE TABLE IF NOT EXISTS intake.enum_logoutdetectortype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_logoutdetectortype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_machinecapabilities;

--DROP TABLE intake.enum_machinecapabilities;
CREATE TABLE IF NOT EXISTS intake.enum_machinecapabilities
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_machinecapabilities owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_machinestatus;

--DROP TABLE intake.enum_machinestatus;
CREATE TABLE IF NOT EXISTS intake.enum_machinestatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_machinestatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_manenvtype;

--DROP TABLE intake.enum_manenvtype;
CREATE TABLE IF NOT EXISTS intake.enum_manenvtype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_manenvtype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_mannonprodconfigparam;

--DROP TABLE intake.enum_mannonprodconfigparam;
CREATE TABLE IF NOT EXISTS intake.enum_mannonprodconfigparam
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_mannonprodconfigparam owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_mitigationaction;

--DROP TABLE intake.enum_mitigationaction;
CREATE TABLE IF NOT EXISTS intake.enum_mitigationaction
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_mitigationaction owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_mitigationstatus;

--DROP TABLE intake.enum_mitigationstatus;
CREATE TABLE IF NOT EXISTS intake.enum_mitigationstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_mitigationstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_notetarget;

--DROP TABLE intake.enum_notetarget;
CREATE TABLE IF NOT EXISTS intake.enum_notetarget
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_notetarget owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_notetype;

--DROP TABLE intake.enum_notetype;
CREATE TABLE IF NOT EXISTS intake.enum_notetype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_notetype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_notificationcause;

--DROP TABLE intake.enum_notificationcause;
CREATE TABLE IF NOT EXISTS intake.enum_notificationcause
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_notificationcause owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_notificationstatus;

--DROP TABLE intake.enum_notificationstatus;
CREATE TABLE IF NOT EXISTS intake.enum_notificationstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_notificationstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_notificationtype;

--DROP TABLE intake.enum_notificationtype;
CREATE TABLE IF NOT EXISTS intake.enum_notificationtype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_notificationtype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_opensourcestatus;

--DROP TABLE intake.enum_opensourcestatus;
CREATE TABLE IF NOT EXISTS intake.enum_opensourcestatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_opensourcestatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_outreachsummarystatus;

--DROP TABLE intake.enum_outreachsummarystatus;
CREATE TABLE IF NOT EXISTS intake.enum_outreachsummarystatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_outreachsummarystatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_parallelscantype;

--DROP TABLE intake.enum_parallelscantype;
CREATE TABLE IF NOT EXISTS intake.enum_parallelscantype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_parallelscantype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_permission;

--DROP TABLE intake.enum_permission;
CREATE TABLE IF NOT EXISTS intake.enum_permission
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_permission owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_phonenumbertype;

--DROP TABLE intake.enum_phonenumbertype;
CREATE TABLE IF NOT EXISTS intake.enum_phonenumbertype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_phonenumbertype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_policycompliancestatus;

--DROP TABLE intake.enum_policycompliancestatus;
CREATE TABLE IF NOT EXISTS intake.enum_policycompliancestatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_policycompliancestatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_policyrulestandard;

--DROP TABLE intake.enum_policyrulestandard;
CREATE TABLE IF NOT EXISTS intake.enum_policyrulestandard
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_policyrulestandard owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_policyruletype;

--DROP TABLE intake.enum_policyruletype;
CREATE TABLE IF NOT EXISTS intake.enum_policyruletype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_policyruletype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_policytype;

--DROP TABLE intake.enum_policytype;
CREATE TABLE IF NOT EXISTS intake.enum_policytype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_policytype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_policyupdatereason;

--DROP TABLE intake.enum_policyupdatereason;
CREATE TABLE IF NOT EXISTS intake.enum_policyupdatereason
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_policyupdatereason owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_prescanstatus;

--DROP TABLE intake.enum_prescanstatus;
CREATE TABLE IF NOT EXISTS intake.enum_prescanstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_prescanstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_pubissuetype;

--DROP TABLE intake.enum_pubissuetype;
CREATE TABLE IF NOT EXISTS intake.enum_pubissuetype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_pubissuetype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_rating;

--DROP TABLE intake.enum_rating;
CREATE TABLE IF NOT EXISTS intake.enum_rating
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_rating owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_readoutstate;

--DROP TABLE intake.enum_readoutstate;
CREATE TABLE IF NOT EXISTS intake.enum_readoutstate
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_readoutstate owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_reasoncode;

--DROP TABLE intake.enum_reasoncode;
CREATE TABLE IF NOT EXISTS intake.enum_reasoncode
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_reasoncode owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_remediatedreopenedstate;

--DROP TABLE intake.enum_remediatedreopenedstate;
CREATE TABLE IF NOT EXISTS intake.enum_remediatedreopenedstate
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_remediatedreopenedstate owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_remeffort;

--DROP TABLE intake.enum_remeffort;
CREATE TABLE IF NOT EXISTS intake.enum_remeffort
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_remeffort owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_reportgenstatus;

--DROP TABLE intake.enum_reportgenstatus;
CREATE TABLE IF NOT EXISTS intake.enum_reportgenstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_reportgenstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_reviewstatus;

--DROP TABLE intake.enum_reviewstatus;
CREATE TABLE IF NOT EXISTS intake.enum_reviewstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_reviewstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_scaartifactsource;

--DROP TABLE intake.enum_scaartifactsource;
CREATE TABLE IF NOT EXISTS intake.enum_scaartifactsource
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_scaartifactsource owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_scacvemanualreview;

--DROP TABLE intake.enum_scacvemanualreview;
CREATE TABLE IF NOT EXISTS intake.enum_scacvemanualreview
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_scacvemanualreview owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_scacvematchtype;

--DROP TABLE intake.enum_scacvematchtype;
CREATE TABLE IF NOT EXISTS intake.enum_scacvematchtype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_scacvematchtype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_scafilepathsha1status;

--DROP TABLE intake.enum_scafilepathsha1status;
CREATE TABLE IF NOT EXISTS intake.enum_scafilepathsha1status
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_scafilepathsha1status owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_scafilepreferencetype;

--DROP TABLE intake.enum_scafilepreferencetype;
CREATE TABLE IF NOT EXISTS intake.enum_scafilepreferencetype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_scafilepreferencetype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_scamatchartifacttype;

--DROP TABLE intake.enum_scamatchartifacttype;
CREATE TABLE IF NOT EXISTS intake.enum_scamatchartifacttype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_scamatchartifacttype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_scanexitstatus;

--DROP TABLE intake.enum_scanexitstatus;
CREATE TABLE IF NOT EXISTS intake.enum_scanexitstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_scanexitstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_scanfrequency;

--DROP TABLE intake.enum_scanfrequency;
CREATE TABLE IF NOT EXISTS intake.enum_scanfrequency
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_scanfrequency owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_scanjoberrortype;

--DROP TABLE intake.enum_scanjoberrortype;
CREATE TABLE IF NOT EXISTS intake.enum_scanjoberrortype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_scanjoberrortype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_scanresultserrortype;

--DROP TABLE intake.enum_scanresultserrortype;
CREATE TABLE IF NOT EXISTS intake.enum_scanresultserrortype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_scanresultserrortype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_semviolationtype;

--DROP TABLE intake.enum_semviolationtype;
CREATE TABLE IF NOT EXISTS intake.enum_semviolationtype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_semviolationtype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_semviolationtypedetail;

--DROP TABLE intake.enum_semviolationtypedetail;
CREATE TABLE IF NOT EXISTS intake.enum_semviolationtypedetail
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_semviolationtypedetail owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_setting;

--DROP TABLE intake.enum_setting;
CREATE TABLE IF NOT EXISTS intake.enum_setting
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_setting owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_sfpublishstate;

--DROP TABLE intake.enum_sfpublishstate;
CREATE TABLE IF NOT EXISTS intake.enum_sfpublishstate
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_sfpublishstate owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_staticautopubsetting;

--DROP TABLE intake.enum_staticautopubsetting;
CREATE TABLE IF NOT EXISTS intake.enum_staticautopubsetting
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_staticautopubsetting owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_structureerror;

--DROP TABLE intake.enum_structureerror;
CREATE TABLE IF NOT EXISTS intake.enum_structureerror
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_structureerror owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_subscriptionanalysistype;

--DROP TABLE intake.enum_subscriptionanalysistype;
CREATE TABLE IF NOT EXISTS intake.enum_subscriptionanalysistype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_subscriptionanalysistype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_subscriptiontype;

--DROP TABLE intake.enum_subscriptiontype;
CREATE TABLE IF NOT EXISTS intake.enum_subscriptiontype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_subscriptiontype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_summarizationsetting;

--DROP TABLE intake.enum_summarizationsetting;
CREATE TABLE IF NOT EXISTS intake.enum_summarizationsetting
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_summarizationsetting owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_summarygenstatus;

--DROP TABLE intake.enum_summarygenstatus;
CREATE TABLE IF NOT EXISTS intake.enum_summarygenstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_summarygenstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_teamvisibleoptions;

--DROP TABLE intake.enum_teamvisibleoptions;
CREATE TABLE IF NOT EXISTS intake.enum_teamvisibleoptions
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_teamvisibleoptions owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_templatecompilestatus;

--DROP TABLE intake.enum_templatecompilestatus;
CREATE TABLE IF NOT EXISTS intake.enum_templatecompilestatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_templatecompilestatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_templatestate;

--DROP TABLE intake.enum_templatestate;
CREATE TABLE IF NOT EXISTS intake.enum_templatestate
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_templatestate owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_trackcstatus;

--DROP TABLE intake.enum_trackcstatus;
CREATE TABLE IF NOT EXISTS intake.enum_trackcstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_trackcstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_uniqissuchksumpopstatus;

--DROP TABLE intake.enum_uniqissuchksumpopstatus;
CREATE TABLE IF NOT EXISTS intake.enum_uniqissuchksumpopstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_uniqissuchksumpopstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_uniqueissuepopstatus;

--DROP TABLE intake.enum_uniqueissuepopstatus;
CREATE TABLE IF NOT EXISTS intake.enum_uniqueissuepopstatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_uniqueissuepopstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_uniqueissuestatus;

--DROP TABLE intake.enum_uniqueissuestatus;
CREATE TABLE IF NOT EXISTS intake.enum_uniqueissuestatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_uniqueissuestatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_useractivitylogstatus;

--DROP TABLE intake.enum_useractivitylogstatus;
CREATE TABLE IF NOT EXISTS intake.enum_useractivitylogstatus
(
	enum_id SMALLINT NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num SMALLINT NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_useractivitylogstatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_usereventtype;

--DROP TABLE intake.enum_usereventtype;
CREATE TABLE IF NOT EXISTS intake.enum_usereventtype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_usereventtype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_vendorrequeststatus;

--DROP TABLE intake.enum_vendorrequeststatus;
CREATE TABLE IF NOT EXISTS intake.enum_vendorrequeststatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_vendorrequeststatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_vendortype;

--DROP TABLE intake.enum_vendortype;
CREATE TABLE IF NOT EXISTS intake.enum_vendortype
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_vendortype owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_veracodedynamicthreshold;

--DROP TABLE intake.enum_veracodedynamicthreshold;
CREATE TABLE IF NOT EXISTS intake.enum_veracodedynamicthreshold
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_veracodedynamicthreshold owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_veracodelevel;

--DROP TABLE intake.enum_veracodelevel;
CREATE TABLE IF NOT EXISTS intake.enum_veracodelevel
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(512) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(512) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_veracodelevel owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_vsaavailability;

--DROP TABLE intake.enum_vsaavailability;
CREATE TABLE IF NOT EXISTS intake.enum_vsaavailability
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_vsaavailability owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_vsastatus;

--DROP TABLE intake.enum_vsastatus;
CREATE TABLE IF NOT EXISTS intake.enum_vsastatus
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_vsastatus owner to vcoderoot;

-- Drop table

-- DROP TABLE enum_wafruleseverity;

--DROP TABLE intake.enum_wafruleseverity;
CREATE TABLE IF NOT EXISTS intake.enum_wafruleseverity
(
	enum_id NUMERIC(38,0) NOT NULL  ENCODE lzo
	,enum_val VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_desc VARCHAR(256) NOT NULL  ENCODE lzo
	,enum_cat VARCHAR(256) NOT NULL  ENCODE lzo
	,order_num NUMERIC(38,0) NOT NULL  ENCODE lzo
	,PRIMARY KEY (enum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.enum_wafruleseverity owner to vcoderoot;

-- Drop table

-- DROP TABLE feature_subscription_link;

--DROP TABLE intake.feature_subscription_link;
CREATE TABLE IF NOT EXISTS intake.feature_subscription_link
(
	feature_subscription_link_id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,feature_id NUMERIC(20,0)   ENCODE lzo
	,subscription_id NUMERIC(20,0)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(256)   ENCODE lzo
	,record_ver NUMERIC(38,18)   ENCODE lzo
	,PRIMARY KEY (feature_subscription_link_id)
)
DISTSTYLE KEY
 DISTKEY (feature_subscription_link_id)
 SORTKEY (
	feature_subscription_link_id
	)
;
ALTER TABLE intake.feature_subscription_link owner to etl_user;

-- Drop table

-- DROP TABLE feature_switches;

--DROP TABLE intake.feature_switches;
CREATE TABLE IF NOT EXISTS intake.feature_switches
(
	feature_switch_id NUMERIC(20,0) NOT NULL  ENCODE lzo
	,feature_group_id NUMERIC(20,0)   ENCODE lzo
	,feature_id NUMERIC(20,0)   ENCODE lzo
	,account_override SMALLINT   ENCODE lzo
	,feature_status SMALLINT   ENCODE lzo
	,inserted_by VARCHAR(32)   ENCODE lzo
	,link_to_subscription SMALLINT   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(256)   ENCODE lzo
	,record_ver NUMERIC(38,18)   ENCODE lzo
	,is_disabled_from_admin_view SMALLINT   ENCODE lzo
	,PRIMARY KEY (feature_switch_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.feature_switches owner to etl_user;

-- Drop table

-- DROP TABLE learn_course;

--DROP TABLE intake.learn_course;
CREATE TABLE IF NOT EXISTS intake.learn_course
(
	learn_course_id NUMERIC(20,0) NOT NULL  ENCODE lzo
	,course_provider_id VARCHAR(256)   ENCODE lzo
	,course_name VARCHAR(256)   ENCODE lzo
	,length_minutes NUMERIC(20,0)   ENCODE lzo
	,has_exam SMALLINT   ENCODE lzo
	,has_pages SMALLINT   ENCODE lzo
	,is_assess SMALLINT   ENCODE lzo
	,short_desc VARCHAR(4000)   ENCODE lzo
	,nas_dir VARCHAR(1024)   ENCODE lzo
	,pub_date TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,is_intro SMALLINT   ENCODE lzo
	,exam_page_link NUMERIC(20,0)   ENCODE lzo
	,course_url VARCHAR(4000)   ENCODE lzo
	,exam_url VARCHAR(4000)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(256)   ENCODE lzo
	,record_ver NUMERIC(38,18)   ENCODE lzo
	,appear_enrolled SMALLINT   ENCODE lzo
	,intro_order NUMERIC(20,0)   ENCODE lzo
	,intro_text VARCHAR(4000)   ENCODE lzo
	,course_level NUMERIC(20,0)   ENCODE lzo
	,PRIMARY KEY (learn_course_id)
)
DISTSTYLE AUTO
;
ALTER TABLE intake.learn_course owner to etl_user;

-- Drop table

-- DROP TABLE learn_curriculum_course;

--DROP TABLE intake.learn_curriculum_course;
CREATE TABLE IF NOT EXISTS intake.learn_curriculum_course
(
	learn_curriculum_course_id NUMERIC(20,0) NOT NULL  ENCODE lzo
	,curriculum_id NUMERIC(20,0)   ENCODE lzo
	,learn_course_id NUMERIC(20,0)   ENCODE lzo
	,course_visibility NUMERIC(20,0)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(256)   ENCODE lzo
	,record_ver NUMERIC(38,18)   ENCODE lzo
	,PRIMARY KEY (learn_curriculum_course_id)
)
DISTSTYLE AUTO
;
ALTER TABLE intake.learn_curriculum_course owner to etl_user;

-- Drop table

-- DROP TABLE learn_track;

--DROP TABLE intake.learn_track;
CREATE TABLE IF NOT EXISTS intake.learn_track
(
	learn_track_id BIGINT NOT NULL  ENCODE lzo
	,name VARCHAR(512) NOT NULL  ENCODE lzo
	,visible_to_all SMALLINT NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,description VARCHAR(8000)   ENCODE lzo
	,can_access_knowledge_base SMALLINT NOT NULL DEFAULT (1)::smallint ENCODE lzo
	,PRIMARY KEY (learn_track_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.learn_track owner to vcoderoot;

-- Drop table

-- DROP TABLE min_val;

--DROP TABLE intake.min_val;
CREATE TABLE IF NOT EXISTS intake.min_val
(
	count BIGINT   ENCODE lzo
)
DISTSTYLE EVEN
;
ALTER TABLE intake.min_val owner to etl_user;

-- Drop table

-- DROP TABLE policy_rule;

--DROP TABLE intake.policy_rule;
CREATE TABLE IF NOT EXISTS intake.policy_rule
(
	policy_rule_id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,policy_id NUMERIC(20,0)   ENCODE lzo
	,"type" NUMERIC(20,0)   ENCODE lzo
	,reference_id NUMERIC(20,0)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(256)   ENCODE lzo
	,record_ver NUMERIC(38,18)   ENCODE lzo
	,PRIMARY KEY (policy_rule_id)
)
DISTSTYLE KEY
 DISTKEY (policy_rule_id)
 SORTKEY (
	policy_rule_id
	)
;
ALTER TABLE intake.policy_rule owner to etl_user;

-- Drop table

-- DROP TABLE red;

--DROP TABLE intake.red;
CREATE TABLE IF NOT EXISTS intake.red
(
	account INTEGER   ENCODE lzo
)
DISTSTYLE EVEN
;
ALTER TABLE intake.red owner to etl_user;

-- Drop table

-- DROP TABLE rollup_category;

--DROP TABLE intake.rollup_category;
CREATE TABLE IF NOT EXISTS intake.rollup_category
(
	rollup_category_id BIGINT NOT NULL  ENCODE lzo
	,name VARCHAR(256) NOT NULL  ENCODE lzo
	,prevalence_rank BIGINT   ENCODE lzo
	,PRIMARY KEY (rollup_category_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.rollup_category owner to vcoderoot;

-- Drop table

-- DROP TABLE run_ver_dyn_issue_status;

--DROP TABLE intake.run_ver_dyn_issue_status;
CREATE TABLE IF NOT EXISTS intake.run_ver_dyn_issue_status
(
	run_ver_dyn_issue_status_id NUMERIC(20,0) NOT NULL  ENCODE lzo
	,flaw_status NUMERIC(20,0)   ENCODE lzo
	,run_ver_id NUMERIC(20,0)   ENCODE lzo
	,latest_issue_id NUMERIC(20,0)   ENCODE lzo
	,original_issue_id NUMERIC(20,0)   ENCODE lzo
	,deleted SMALLINT   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(256)   ENCODE lzo
	,record_ver NUMERIC(38,18)   ENCODE lzo
	,PRIMARY KEY (run_ver_dyn_issue_status_id)
)
DISTSTYLE EVEN
;
ALTER TABLE intake.run_ver_dyn_issue_status owner to etl_user;

-- Drop table

-- DROP TABLE sca_scan;

--DROP TABLE intake.sca_scan;
CREATE TABLE IF NOT EXISTS intake.sca_scan
(
	sca_scan_id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,analysis_unit_id NUMERIC(20,0)   ENCODE lzo
	,app_ver_id NUMERIC(20,0)   ENCODE lzo
	,app_id NUMERIC(20,0)   ENCODE lzo
	,scan_id VARCHAR(36)   ENCODE lzo
	,num_components NUMERIC(20,0)   ENCODE lzo
	,publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,legacy_num_components NUMERIC(20,0)   ENCODE lzo
	,legacy_publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,enable_legacy SMALLINT   ENCODE lzo
	,enable_cots_vendor SMALLINT   ENCODE lzo
	,enable_cots_enterprise SMALLINT   ENCODE lzo
	,submitter_login_account_id NUMERIC(20,0)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(256)   ENCODE lzo
	,record_ver NUMERIC(38,18)   ENCODE lzo
	,PRIMARY KEY (sca_scan_id)
)
DISTSTYLE KEY
 DISTKEY (sca_scan_id)
 SORTKEY (
	sca_scan_id
	)
;
ALTER TABLE intake.sca_scan owner to etl_user;

-- Drop table

-- DROP TABLE sca_scan_job;

--DROP TABLE intake.sca_scan_job;
CREATE TABLE IF NOT EXISTS intake.sca_scan_job
(
	scan_job_id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,account_id NUMERIC(20,0)   ENCODE lzo
	,app_id NUMERIC(20,0)   ENCODE lzo
	,sandbox_id NUMERIC(20,0)   ENCODE lzo
	,app_ver_id NUMERIC(20,0)   ENCODE lzo
	,analysis_unit_id NUMERIC(20,0)   ENCODE lzo
	,sandbox SMALLINT   ENCODE lzo
	,sca_enabled SMALLINT   ENCODE lzo
	,binary_path VARCHAR(1024)   ENCODE lzo
	,alias VARCHAR(256)   ENCODE lzo
	,encrypt_key VARCHAR(440)   ENCODE lzo
	,encrypt_iv VARCHAR(256)   ENCODE lzo
	,scan_id VARCHAR(36)   ENCODE lzo
	,scan_status NUMERIC(20,0)   ENCODE lzo
	,scan_status_info VARCHAR(1024)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(256)   ENCODE lzo
	,record_ver NUMERIC(38,18)   ENCODE lzo
	,encrypt_method NUMERIC(20,0)   ENCODE lzo
	,scan_config_id VARCHAR(36)   ENCODE lzo
	,s3_bucket_id NUMERIC(20,0)   ENCODE lzo
	,PRIMARY KEY (scan_job_id)
)
DISTSTYLE KEY
 DISTKEY (scan_job_id)
 SORTKEY (
	scan_job_id
	)
;
ALTER TABLE intake.sca_scan_job owner to etl_user;

-- Drop table

-- DROP TABLE scan_license;

--DROP TABLE intake.scan_license;
CREATE TABLE IF NOT EXISTS intake.scan_license
(
	"licensed account" VARCHAR(512)   ENCODE lzo
	,"application id" BIGINT   ENCODE lzo
	,"application name" VARCHAR(512)   ENCODE lzo
	,"license type" VARCHAR(512)   ENCODE lzo
	,"scanning account" VARCHAR(512)   ENCODE lzo
	,"application state" VARCHAR(14)   ENCODE lzo
	,"3rd party state" VARCHAR(23)   ENCODE lzo
	,"build id" BIGINT   ENCODE lzo
	,"scan name" VARCHAR(512)   ENCODE lzo
	,"sandbox type" VARCHAR(11)   ENCODE lzo
	,"scan state" VARCHAR(14)   ENCODE lzo
	,"scan type" VARCHAR(512)   ENCODE lzo
	,"business unit" VARCHAR(512)   ENCODE lzo
	,"teams -list" VARCHAR(65535)   ENCODE lzo
	,"business owner name" VARCHAR(256)   ENCODE lzo
	,bus_owner_email VARCHAR(512)   ENCODE lzo
	,"created date" DATE   ENCODE lzo
	,"submitted date" DATE   ENCODE lzo
	,"first publish date" DATE   ENCODE lzo
	,"published to vendor date" TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,"published to enterprise date" TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,"language" VARCHAR(128)   ENCODE lzo
	,"salesforce account id" VARCHAR(512)   ENCODE lzo
	,"analysis size - mb" NUMERIC(27,6)   ENCODE lzo
	,"upload size - mb" NUMERIC(38,4)   ENCODE lzo
	,"potential license used" NUMERIC(18,0)   ENCODE lzo
	,"purchased sku per scan" VARCHAR(128)   ENCODE lzo
	,contract VARCHAR(128)   ENCODE lzo
	,"subscription start date" DATE   ENCODE lzo
	,"contract year id" VARCHAR(128)   ENCODE lzo
	,"times within subscription year" NUMERIC(18,0)   ENCODE lzo
)
DISTSTYLE EVEN
;
ALTER TABLE intake.scan_license owner to etl_user;

-- Drop table

-- DROP TABLE scan_request_analytics;

--DROP TABLE intake.scan_request_analytics;
CREATE TABLE IF NOT EXISTS intake.scan_request_analytics
(
	id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,analysis_unit_id NUMERIC(20,0)   ENCODE zstd
	,audit_type NUMERIC(20,0)   ENCODE zstd
	,audit_action NUMERIC(20,0)   ENCODE zstd
	,user_agent VARCHAR(1024)   ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,PRIMARY KEY (id)
)
DISTSTYLE KEY
 DISTKEY (id)
 SORTKEY (
	id
	)
;
ALTER TABLE intake.scan_request_analytics owner to etl_user;

-- Drop table

-- DROP TABLE scan_size_tracking;

--DROP TABLE intake.scan_size_tracking;
CREATE TABLE IF NOT EXISTS intake.scan_size_tracking
(
	scan_size_tracking_id NUMERIC(20,0) NOT NULL  ENCODE lzo
	,account_id NUMERIC(20,0)   ENCODE lzo
	,app_id NUMERIC(20,0)   ENCODE lzo
	,app_ver_id NUMERIC(20,0)   ENCODE lzo
	,analysis_size NUMERIC(20,0)   ENCODE lzo
	,record_ver NUMERIC(20,0)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(256)   ENCODE lzo
	,PRIMARY KEY (scan_size_tracking_id)
)
DISTSTYLE EVEN
;
ALTER TABLE intake.scan_size_tracking owner to etl_user;

-- Drop table

-- DROP TABLE scrubbed_flaws;

--DROP TABLE intake.scrubbed_flaws;
CREATE TABLE IF NOT EXISTS intake.scrubbed_flaws
(
	run_ver_issue_id BIGINT   ENCODE zstd
	,analysis_unit_id BIGINT   ENCODE RAW
	,"language" VARCHAR(8000)   ENCODE zstd
	,cwe_id BIGINT NOT NULL  ENCODE zstd
	,severity BIGINT NOT NULL  ENCODE zstd
	,issue_state BIGINT NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512)   ENCODE zstd
	,last_modified TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,auto_mark_valid SMALLINT   ENCODE zstd
	,analysis_type_code BIGINT   ENCODE zstd
	,vsa SMALLINT   ENCODE zstd
)
DISTSTYLE KEY
 DISTKEY (run_ver_issue_id)
 SORTKEY (
	analysis_unit_id
	)
;
ALTER TABLE intake.scrubbed_flaws owner to vcoderoot;

-- Drop table

-- DROP TABLE src_file_mapping;

--DROP TABLE intake.src_file_mapping;
CREATE TABLE IF NOT EXISTS intake.src_file_mapping
(
	sandbox_src_file_id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,app_src_file_id NUMERIC(20,0)   ENCODE RAW
	,sandbox_id NUMERIC(20,0)   ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,PRIMARY KEY (sandbox_src_file_id)
)
DISTSTYLE KEY
 DISTKEY (sandbox_src_file_id)
 SORTKEY (
	sandbox_src_file_id
	, app_src_file_id
	)
;
ALTER TABLE intake.src_file_mapping owner to etl_user;

-- Drop table

-- DROP TABLE unique_issue;

--DROP TABLE intake.unique_issue;
CREATE TABLE IF NOT EXISTS intake.unique_issue
(
	unique_issue_id NUMERIC(18,0) NOT NULL  ENCODE RAW
	,sandbox_id NUMERIC(18,0) NOT NULL  ENCODE lzo
	,analysis_type NUMERIC(18,0) NOT NULL  ENCODE lzo
	,original_issue_id NUMERIC(18,0)   ENCODE lzo
	,cust_issue_id NUMERIC(18,0) NOT NULL  ENCODE lzo
	,introduced_analysis_unit_id NUMERIC(18,0)   ENCODE RAW
	,fixed_analysis_unit_id NUMERIC(18,0)   ENCODE RAW
	,internal_status NUMERIC(18,0) NOT NULL  ENCODE lzo
	,latest_run_ver_issue_id NUMERIC(18,0) NOT NULL  ENCODE RAW
	,latest_change_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,deleted SMALLINT NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(256) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,checksum VARCHAR(256)   ENCODE lzo
	,checksum_version NUMERIC(18,0)   ENCODE lzo
	,latest_analysis_unit_id NUMERIC(18,0) NOT NULL  ENCODE RAW
	,latest_pub_run_ver_issue_id NUMERIC(18,0)   ENCODE RAW
	,latest_pub_analysis_unit_id NUMERIC(18,0)   ENCODE RAW
	,external_status NUMERIC(18,0)   ENCODE lzo
	,PRIMARY KEY (unique_issue_id)
)
DISTSTYLE KEY
 DISTKEY (unique_issue_id)
 SORTKEY (
	unique_issue_id
	, fixed_analysis_unit_id
	, introduced_analysis_unit_id
	, latest_analysis_unit_id
	, latest_pub_analysis_unit_id
	, latest_pub_run_ver_issue_id
	, latest_run_ver_issue_id
	)
;
ALTER TABLE intake.unique_issue owner to vcoderoot;

-- Drop table

-- DROP TABLE veracode_level_policy;

--DROP TABLE intake.veracode_level_policy;
CREATE TABLE IF NOT EXISTS intake.veracode_level_policy
(
	veracode_level_policy_id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,policy_id NUMERIC(20,0)   ENCODE lzo
	,veracode_level NUMERIC(20,0)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(256)   ENCODE lzo
	,record_ver NUMERIC(38,18)   ENCODE lzo
	,PRIMARY KEY (veracode_level_policy_id)
)
DISTSTYLE KEY
 DISTKEY (veracode_level_policy_id)
 SORTKEY (
	veracode_level_policy_id
	)
;
ALTER TABLE intake.veracode_level_policy owner to etl_user;

-- Drop table

-- DROP TABLE was_import_app;

--DROP TABLE intake.was_import_app;
CREATE TABLE IF NOT EXISTS intake.was_import_app
(
	was_import_app_id NUMERIC(20,0) NOT NULL  ENCODE lzo
	,import_request_id VARCHAR(45)   ENCODE lzo
	,result_import_app_id NUMERIC(20,0)   ENCODE lzo
	,account_id NUMERIC(20,0)   ENCODE lzo
	,login_account_id NUMERIC(20,0)   ENCODE lzo
	,result_import_version_name VARCHAR(256)   ENCODE lzo
	,status NUMERIC(20,0)   ENCODE lzo
	,no_of_retry_attempts NUMERIC(20,0)   ENCODE lzo
	,last_retry_time TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,dynamic_analysis_scan_job_id NUMERIC(20,0)   ENCODE lzo
	,is_deleted SMALLINT   ENCODE lzo
	,error_string VARCHAR(512)   ENCODE lzo
	,import_app_ver_id NUMERIC(20,0)   ENCODE lzo
	,exec_unit_ver_id NUMERIC(20,0)   ENCODE lzo
	,dynamicmp_job_id NUMERIC(20,0)   ENCODE lzo
	,scan_occurrence_id VARCHAR(45)   ENCODE lzo
	,scan_id VARCHAR(45)   ENCODE lzo
	,analysis_occurrence_id VARCHAR(45)   ENCODE lzo
	,ack_sent_time TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,was_app_id VARCHAR(45)   ENCODE lzo
	,scan_config_id VARCHAR(45)   ENCODE lzo
	,link_app_name VARCHAR(256)   ENCODE lzo
	,import_schedule_id NUMERIC(20,0)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(256)   ENCODE lzo
	,record_ver NUMERIC(38,18)   ENCODE lzo
	,internal_status NUMERIC(20,0)   ENCODE lzo
	,PRIMARY KEY (was_import_app_id)
)
DISTSTYLE EVEN
;
ALTER TABLE intake.was_import_app owner to etl_user;

-- Drop table

-- DROP TABLE acl_entry;

--DROP TABLE intake.acl_entry;
CREATE TABLE IF NOT EXISTS intake.acl_entry
(
	acl_entry_id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,subject_id NUMERIC(20,0) NOT NULL  ENCODE lzo
	,subject_type NUMERIC(20,0) NOT NULL  ENCODE lzo
	,resource_id NUMERIC(20,0) NOT NULL  ENCODE lzo
	,resource_type NUMERIC(20,0) NOT NULL  ENCODE lzo
	,acl_permission NUMERIC(20,0) NOT NULL  ENCODE lzo
	,description VARCHAR(256)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(256) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,acl_reason NUMERIC(20,0) NOT NULL DEFAULT 1 ENCODE lzo
	,PRIMARY KEY (acl_entry_id)
)
DISTSTYLE ALL
 SORTKEY (
	acl_entry_id
	)
;
ALTER TABLE intake.acl_entry owner to vcoderoot;

-- Drop table

-- DROP TABLE analysis_unit_man_params;

--DROP TABLE intake.analysis_unit_man_params;
CREATE TABLE IF NOT EXISTS intake.analysis_unit_man_params
(
	analysis_unit_man_params_id BIGINT NOT NULL  ENCODE lzo
	,analysis_unit_id BIGINT NOT NULL  ENCODE lzo
	,account_name VARCHAR(512)   ENCODE lzo
	,app_name VARCHAR(512)   ENCODE lzo
	,version_label VARCHAR(512)   ENCODE lzo
	,generation_dt TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,delivery_consultants VARCHAR(8000)   ENCODE lzo
	,cia_adjustment BIGINT   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,target_url VARCHAR(512)   ENCODE lzo
	,man_env_type BIGINT NOT NULL DEFAULT 1 ENCODE lzo
	,restrictions VARCHAR(512)   ENCODE lzo
	,login_required BIGINT NOT NULL DEFAULT 0 ENCODE lzo
	,multitenancy SMALLINT  DEFAULT 0 ENCODE lzo
	,vpn_required SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,it_first_name VARCHAR(512)   ENCODE lzo
	,it_last_name VARCHAR(512)   ENCODE lzo
	,it_telephone VARCHAR(512)   ENCODE lzo
	,it_email VARCHAR(512)   ENCODE lzo
	,man_scan_needs_approval SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,man_scan_holds_released SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,PRIMARY KEY (analysis_unit_man_params_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.analysis_unit_man_params owner to vcoderoot;

-- Drop table

-- DROP TABLE app_server_job;

--DROP TABLE intake.app_server_job;
CREATE TABLE IF NOT EXISTS intake.app_server_job
(
	app_server_job_id BIGINT NOT NULL  ENCODE zstd
	,"type" BIGINT NOT NULL  ENCODE zstd
	,status BIGINT NOT NULL DEFAULT 1 ENCODE RAW
	,app_server_id BIGINT   ENCODE zstd
	,object_identifier BIGINT NOT NULL  ENCODE zstd
	,priority BIGINT NOT NULL DEFAULT 5 ENCODE zstd
	,assign_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,start_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,stop_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,lock_string VARCHAR(100)   ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,target_string VARCHAR(2048)   ENCODE zstd
	,start_after_date TIMESTAMP WITHOUT TIME ZONE   ENCODE RAW
	,s3_bucket_id NUMERIC(20,0)   ENCODE lzo
	,PRIMARY KEY (app_server_job_id)
)
DISTSTYLE KEY
 DISTKEY (app_server_job_id)
 SORTKEY (
	status
	, start_after_date
	)
;
ALTER TABLE intake.app_server_job owner to vcoderoot;

-- Drop table

-- DROP TABLE auditor;

--DROP TABLE intake.auditor;
CREATE TABLE IF NOT EXISTS intake.auditor
(
	auditor_id VARCHAR(72) NOT NULL  ENCODE zstd
	,event_time TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE RAW
	,account_id BIGINT   ENCODE zstd
	,login_account_id BIGINT   ENCODE zstd
	,principal_id BIGINT   ENCODE zstd
	,audit_type BIGINT NOT NULL  ENCODE zstd
	,audit_action BIGINT NOT NULL  ENCODE zstd
	,source_host VARCHAR(512) NOT NULL  ENCODE zstd
	,observed_host VARCHAR(512)   ENCODE zstd
	,target_type VARCHAR(256) NOT NULL  ENCODE zstd
	,target_id VARCHAR(72)   ENCODE zstd
	,"data" VARCHAR(2048)   ENCODE zstd
	,user_full_name VARCHAR(512)   ENCODE zstd
	,app_id BIGINT   ENCODE zstd
	,app_ver_id BIGINT   ENCODE zstd
	,analysis_unit_id BIGINT   ENCODE zstd
	,sandbox_id BIGINT   ENCODE zstd
	,is_proxy SMALLINT   ENCODE zstd
	,PRIMARY KEY (auditor_id)
)
DISTSTYLE KEY
 DISTKEY (auditor_id)
 SORTKEY (
	event_time
	)
;
ALTER TABLE intake.auditor owner to vcoderoot;

-- Drop table

-- DROP TABLE cwe;

--DROP TABLE intake.cwe;
CREATE TABLE IF NOT EXISTS intake.cwe
(
	cwe_id BIGINT NOT NULL  ENCODE lzo
	,name VARCHAR(256) NOT NULL  ENCODE lzo
	,base_severity BIGINT   ENCODE lzo
	,description VARCHAR(8000)   ENCODE lzo
	,rem_effort BIGINT   ENCODE lzo
	,confidentiality VARCHAR(2)   ENCODE lzo
	,integrity VARCHAR(2)   ENCODE lzo
	,availability VARCHAR(2)   ENCODE lzo
	,rollup_category_id BIGINT NOT NULL DEFAULT -1 ENCODE lzo
	,owasp04 VARCHAR(256)   ENCODE lzo
	,owasp07 VARCHAR(256)   ENCODE lzo
	,sanstop25 BIGINT   ENCODE lzo
	,owasp10 VARCHAR(256)   ENCODE lzo
	,static SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,dynamic SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,certc BIGINT   ENCODE lzo
	,certcpp BIGINT   ENCODE lzo
	,certjava BIGINT   ENCODE lzo
	,wasc2 VARCHAR(256)   ENCODE lzo
	,owasp BIGINT   ENCODE lzo
	,owasptext VARCHAR(256)   ENCODE lzo
	,owasp13 VARCHAR(256)   ENCODE lzo
	,owaspmobile NUMERIC(20,0)   ENCODE lzo
	,PRIMARY KEY (cwe_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.cwe owner to vcoderoot;

-- Drop table

-- DROP TABLE "role";

--DROP TABLE intake.role;
CREATE TABLE IF NOT EXISTS intake.role
(
	role_id BIGINT NOT NULL  ENCODE lzo
	,role_label VARCHAR(512) NOT NULL  ENCODE lzo
	,role_handle VARCHAR(512) NOT NULL  ENCODE lzo
	,is_internal SMALLINT NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,role_type BIGINT NOT NULL DEFAULT 1 ENCODE lzo
	,requires_token SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,assign_to_proxy_users SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,PRIMARY KEY (role_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.role owner to vcoderoot;

-- Drop table

-- DROP TABLE sf_publish;

--DROP TABLE intake.sf_publish;
CREATE TABLE IF NOT EXISTS intake.sf_publish
(
	sf_publish_id BIGINT NOT NULL  ENCODE zstd
	,analysis_unit_id BIGINT NOT NULL  ENCODE zstd
	,account_service_id BIGINT   ENCODE zstd
	,free_rescan SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,unpublish_id BIGINT  DEFAULT 0 ENCODE zstd
	,state BIGINT NOT NULL  ENCODE RAW
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,notes VARCHAR(512)   ENCODE zstd
	,PRIMARY KEY (sf_publish_id)
)
DISTSTYLE KEY
 DISTKEY (sf_publish_id)
 SORTKEY (
	state
	)
;
ALTER TABLE intake.sf_publish owner to vcoderoot;

-- Drop table

-- DROP TABLE subscription;

--DROP TABLE intake.subscription;
CREATE TABLE IF NOT EXISTS intake.subscription
(
	subscription_id BIGINT NOT NULL  ENCODE lzo
	,product_code VARCHAR(512) NOT NULL  ENCODE lzo
	,description VARCHAR(4096)   ENCODE lzo
	,default_scans_purchased BIGINT   ENCODE lzo
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,subscription_type BIGINT NOT NULL DEFAULT 1 ENCODE RAW
	,discontinued SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,subscription_analysis_type BIGINT NOT NULL DEFAULT 4 ENCODE lzo
	,learn_track_id BIGINT   ENCODE lzo
	,track_locked SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,publish_type NUMERIC(20,0)   ENCODE lzo
	,PRIMARY KEY (subscription_id)
)
DISTSTYLE ALL
 SORTKEY (
	subscription_type
	)
;
ALTER TABLE intake.subscription owner to vcoderoot;

-- Drop table

-- DROP TABLE account;

--DROP TABLE intake.account;
CREATE TABLE IF NOT EXISTS intake.account
(
	account_id BIGINT NOT NULL  ENCODE RAW
	,account_name VARCHAR(512) NOT NULL  ENCODE lzo
	,nas_dir VARCHAR(512) NOT NULL  ENCODE lzo
	,is_internal SMALLINT NOT NULL  ENCODE lzo
	,login_enabled SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,industry_vertical_id BIGINT NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,modified_by VARCHAR(512)   ENCODE lzo
	,record_ver BIGINT  DEFAULT 1 ENCODE lzo
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,allow_os SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,account_type BIGINT NOT NULL  ENCODE lzo
	,manager_id BIGINT   ENCODE lzo
	,sales_lead VARCHAR(512)   ENCODE lzo
	,has_branding_image SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,account_status INTEGER NOT NULL DEFAULT 1 ENCODE lzo
	,creator_login_account_id BIGINT   ENCODE lzo
	,creator_account_id BIGINT   ENCODE lzo
	,enable_learn_managers SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,learn_group_id BIGINT   ENCODE lzo
	,learn_course_selection_id BIGINT NOT NULL DEFAULT 1 ENCODE lzo
	,external_account_id VARCHAR(512)   ENCODE lzo
	,default_course_visibility BIGINT NOT NULL DEFAULT 2 ENCODE lzo
	,sales_lead_email VARCHAR(512)   ENCODE lzo
	,sa_name VARCHAR(512)   ENCODE lzo
	,sa_email VARCHAR(512)   ENCODE lzo
	,csm_name VARCHAR(512)   ENCODE lzo
	,csm_email VARCHAR(512)   ENCODE lzo
	,cots_agree BIGINT NOT NULL DEFAULT 1 ENCODE lzo
	,cots_agree_id BIGINT   ENCODE lzo
	,cots_agree_date TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,last_contracted_id BIGINT   ENCODE lzo
	,last_contracted_date TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,deleted_files SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,is_test SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,vendor_type BIGINT NOT NULL DEFAULT 1 ENCODE lzo
	,auto_pub_disabled SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,machine_group_id BIGINT   ENCODE lzo
	,dynamic_scan_type BIGINT NOT NULL DEFAULT 3 ENCODE lzo
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,saml_enabled SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,keep_binary_days BIGINT NOT NULL DEFAULT 0 ENCODE lzo
	,dyn_scan_req_aprvl SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,enable_custom_templates SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,allow_adv_dyn_scan_opt SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,self_assign_curriculum SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,suppress_preflight_login_error SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,birst_group_name VARCHAR(600)   ENCODE lzo
	,readout_state BIGINT NOT NULL DEFAULT 3 ENCODE lzo
	,dyn_auto_publish SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,discovery_scan SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,dynamicmp_scan SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,manual_scan SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,vsa_enabled SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,vsa_scan_req_aprvl SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,waf_rules_generation_enabled SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,dynamic_machine_group_id BIGINT   ENCODE lzo
	,sandbox_enabled SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,concurrent_sandbox_scan_limit BIGINT NOT NULL DEFAULT 5 ENCODE lzo
	,remediation_scan SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,expand_ears SMALLINT   ENCODE lzo
	,vendor_rescan SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,dyn_prescan_err_enable_submit SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,custom_severity SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,pov_in_progress SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,disco_auto_publish SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,vendor_directory_enabled SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,sca_enabled SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,fingerprint_enabled SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,auto_scan_default SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,hide_template_errors SMALLINT   ENCODE lzo
	,vendor_display_name VARCHAR(512)   ENCODE lzo
	,vendor_contact_name VARCHAR(512)   ENCODE lzo
	,vendor_contact_email VARCHAR(512)   ENCODE lzo
	,asset_inventory_enabled SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,delay_rollout_months BIGINT   ENCODE lzo
	,vendor_contact_job_title VARCHAR(512)   ENCODE lzo
	,is_vpn_enabled SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,dyn_login_err_enable_submit SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,associate_name VARCHAR(512)   ENCODE lzo
	,associate_email VARCHAR(512)   ENCODE lzo
	,api_credential_ttl BIGINT NOT NULL DEFAULT 365 ENCODE lzo
	,static_auto_pub_setting BIGINT NOT NULL DEFAULT 1 ENCODE lzo
	,auto_pub_size_threshold_mb BIGINT NOT NULL DEFAULT 5 ENCODE lzo
	,is_scan_funded_by_vendor SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,custom_cleanser_setting BIGINT  DEFAULT 1 ENCODE lzo
	,looker_group_space_id NUMERIC(20,0)   ENCODE lzo
	,s3_only SMALLINT   ENCODE lzo
	,s3_bucket_id NUMERIC(20,0)   ENCODE lzo
	,sandbox_cap NUMERIC(38,18)   ENCODE zstd
	,concurrent_scans_cap NUMERIC(38,18)   ENCODE zstd
	,PRIMARY KEY (account_id)
)
DISTSTYLE ALL
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.account owner to vcoderoot;

-- Drop table

-- DROP TABLE account_feature_switches;

--DROP TABLE intake.account_feature_switches;
CREATE TABLE IF NOT EXISTS intake.account_feature_switches
(
	account_feature_switch_id BIGINT NOT NULL  ENCODE lzo
	,account_id BIGINT NOT NULL  ENCODE RAW
	,feature_id BIGINT NOT NULL  ENCODE lzo
	,feature_status SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,inserted_by VARCHAR(64)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,PRIMARY KEY (account_feature_switch_id)
)
DISTSTYLE ALL
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.account_feature_switches owner to vcoderoot;

-- Drop table

-- DROP TABLE account_service;

--DROP TABLE intake.account_service;
CREATE TABLE IF NOT EXISTS intake.account_service
(
	account_service_id BIGINT NOT NULL  ENCODE lzo
	,account_id BIGINT NOT NULL  ENCODE RAW
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,sub_start_date TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,sub_end_date TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,subscription BIGINT NOT NULL  ENCODE lzo
	,purchase_order VARCHAR(256)   ENCODE lzo
	,scans_purchased BIGINT   ENCODE lzo
	,active SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,instructions VARCHAR(8000)   ENCODE lzo
	,app_size_definition BIGINT NOT NULL DEFAULT 50 ENCODE lzo
	,contract_line_item VARCHAR(256)   ENCODE lzo
	,sf_contract_id VARCHAR(64)   ENCODE lzo
	,sf_contract_line_item_id VARCHAR(64)   ENCODE lzo
	,type_of_contract VARCHAR(512)   ENCODE lzo
	,publish_consumption SMALLINT   ENCODE lzo
	,flex_licensing NUMERIC(5,2)   ENCODE lzo
	,PRIMARY KEY (account_service_id)
)
DISTSTYLE ALL
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.account_service owner to vcoderoot;

-- Drop table

-- DROP TABLE analysis;

--DROP TABLE intake.analysis;
CREATE TABLE IF NOT EXISTS intake.analysis
(
	analysis_id BIGINT NOT NULL  ENCODE zstd
	,analysis_name VARCHAR(512) NOT NULL  ENCODE zstd
	,app_ver_id BIGINT NOT NULL  ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,reviewed_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE RAW
	,PRIMARY KEY (analysis_id)
)
DISTSTYLE KEY
 DISTKEY (analysis_id)
 SORTKEY (
	reviewed_ts
	)
;
ALTER TABLE intake.analysis owner to vcoderoot;

-- Drop table

-- DROP TABLE analysis_unit;

--DROP TABLE intake.analysis_unit;
CREATE TABLE IF NOT EXISTS intake.analysis_unit
(
	analysis_unit_id BIGINT NOT NULL  ENCODE lzo
	,analysis_id BIGINT NOT NULL  ENCODE lzo
	,analysis_type BIGINT NOT NULL  ENCODE lzo
	,nas_dir VARCHAR(2048)   ENCODE lzo
	,reports_avail SMALLINT NOT NULL  ENCODE lzo
	,scan_status_id BIGINT NOT NULL  ENCODE lzo
	,error_msg_id BIGINT   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL  ENCODE lzo
	,has_preflight_result SMALLINT NOT NULL  ENCODE lzo
	,submitter_login_account_id BIGINT   ENCODE lzo
	,publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,analysis_size_bytes BIGINT   ENCODE lzo
	,num_scans BIGINT   ENCODE lzo
	,submit_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,estimate_ready_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,slo_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,extend_slo BIGINT   ENCODE lzo
	,enhanced_dyn SMALLINT NOT NULL  ENCODE lzo
	,sales_publish_priority BIGINT   ENCODE lzo
	,progress_score BIGINT   ENCODE lzo
	,webapp SMALLINT   ENCODE lzo
	,auto_pub_status BIGINT NOT NULL  ENCODE lzo
	,beta_slo BIGINT   ENCODE lzo
	,dynamic_scan_type BIGINT NOT NULL  ENCODE lzo
	,has_nested_archive SMALLINT   ENCODE lzo
	,eta_hours BIGINT   ENCODE lzo
	,eta_estimate_type BIGINT   ENCODE lzo
	,first_publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,estimate_ready_ts_orig TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,suppressed_login_error_only SMALLINT NOT NULL  ENCODE lzo
	,suppressed_login_error SMALLINT NOT NULL  ENCODE lzo
	,creator_login_account_id BIGINT   ENCODE lzo
	,scan_running_offline SMALLINT NOT NULL  ENCODE lzo
	,deleted SMALLINT NOT NULL  ENCODE lzo
	,engine_version VARCHAR(64)   ENCODE lzo
	,vendor_rescan SMALLINT NOT NULL  ENCODE lzo
	,promote_from_scan_id BIGINT   ENCODE lzo
	,auto_scan SMALLINT NOT NULL  ENCODE lzo
	,sanitized SMALLINT   ENCODE lzo
	,fixed_flaw_state BIGINT   ENCODE lzo
	,is_partial_results_ready SMALLINT   ENCODE lzo
	,needs_fixed_reopened_recalc SMALLINT   ENCODE lzo
	,sent_incremental_publish_email SMALLINT   ENCODE lzo
	,codebase_pricing_size BIGINT   ENCODE lzo
	,used_codebase_pricing_size SMALLINT   ENCODE lzo
	,unsubmitted_static_notice_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,PRIMARY KEY (analysis_unit_id)
)
DISTSTYLE KEY
 DISTKEY (analysis_id)
 SORTKEY (
	submit_ts
	)
;
ALTER TABLE intake.analysis_unit owner to vcoderoot;

-- Drop table

-- DROP TABLE analysis_unit_dyn_op;

--DROP TABLE intake.analysis_unit_dyn_op;
CREATE TABLE IF NOT EXISTS intake.analysis_unit_dyn_op
(
	analysis_unit_dyn_op_id BIGINT NOT NULL  ENCODE zstd
	,analysis_unit_id BIGINT NOT NULL  ENCODE zstd
	,engine_job_id BIGINT  DEFAULT 0 ENCODE zstd
	,is_pre_scan SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,have_scan_results SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,free_disk_mb BIGINT NOT NULL DEFAULT 0 ENCODE zstd
	,svn_revision VARCHAR(512)   ENCODE zstd
	,exit_status VARCHAR(512)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,stop_time TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,state VARCHAR(512)   ENCODE lzo
	,login_successes BIGINT   ENCODE zstd
	,login_failures BIGINT   ENCODE zstd
	,duration BIGINT   ENCODE zstd
	,requests BIGINT   ENCODE zstd
	,responses BIGINT   ENCODE zstd
	,links BIGINT   ENCODE zstd
	,network_errors BIGINT   ENCODE zstd
	,port_shutdowns BIGINT   ENCODE zstd
	,bytes_sent BIGINT   ENCODE zstd
	,bytes_received BIGINT   ENCODE zstd
	,force_complete_exit_status VARCHAR(512)   ENCODE lzo
	,exec_unit_ver_id BIGINT   ENCODE zstd
	,scan_exit_status BIGINT   ENCODE zstd
	,PRIMARY KEY (analysis_unit_dyn_op_id)
)
DISTSTYLE KEY
 DISTKEY (analysis_unit_dyn_op_id)
;
ALTER TABLE intake.analysis_unit_dyn_op owner to vcoderoot;

-- Drop table

-- DROP TABLE analysis_unit_dyn_params;

--DROP TABLE intake.analysis_unit_dyn_params;
CREATE TABLE IF NOT EXISTS intake.analysis_unit_dyn_params
(
	analysis_unit_dyn_params_id BIGINT NOT NULL  ENCODE delta32k
	,analysis_unit_id BIGINT NOT NULL  ENCODE zstd
	,target_url VARCHAR(512) NOT NULL  ENCODE zstd
	,restrict_to_dir SMALLINT NOT NULL  ENCODE lzo
	,max_links BIGINT NOT NULL DEFAULT 5000 ENCODE zstd
	,exclude_portions VARCHAR(8000)   ENCODE lzo
	,login_required SMALLINT NOT NULL  ENCODE lzo
	,login_username VARCHAR(512)   ENCODE lzo
	,login_password VARCHAR(512)   ENCODE lzo
	,login_sso_url VARCHAR(512)   ENCODE lzo
	,ntlm_domain_name VARCHAR(512)   ENCODE lzo
	,it_cont_first_name VARCHAR(512) NOT NULL  ENCODE zstd
	,it_cont_last_name VARCHAR(512) NOT NULL  ENCODE zstd
	,it_cont_phone VARCHAR(512) NOT NULL  ENCODE zstd
	,it_cont_email VARCHAR(512) NOT NULL  ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,enhanced_dyn SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,special_instructions VARCHAR(8000)   ENCODE lzo
	,login_seq_file_name VARCHAR(512)   ENCODE lzo
	,verification_url VARCHAR(512)   ENCODE zstd
	,verification_text VARCHAR(512)   ENCODE lzo
	,spec_crawl_seq_required SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,crawl_seq_file_name VARCHAR(512)   ENCODE lzo
	,user_agent VARCHAR(8000)   ENCODE zstd
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,browser_based_login SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,script_based_login SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,veracode_dynamic_threshold BIGINT NOT NULL DEFAULT 60 ENCODE zstd
	,custom_plugin_configuration SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,dynamic_template_source BIGINT NOT NULL DEFAULT 0 ENCODE zstd
	,disable_html_crawler SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,crawl_seq_file_type BIGINT NOT NULL DEFAULT 0 ENCODE zstd
	,response_timeout BIGINT NOT NULL DEFAULT 100 ENCODE zstd
	,certified SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,allow_auto_scan SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,client_cert_file_name VARCHAR(512)   ENCODE lzo
	,client_cert_auth SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,target_ip VARCHAR(512)   ENCODE lzo
	,dyn_scan_needs_approval SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,dyn_scan_holds_released SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,both_http_https SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,logout_detector SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,vsa_id BIGINT   ENCODE lzo
	,dyn_scan_on_vsa_needs_approval SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,vsa_group_id BIGINT   ENCODE lzo
	,scan_use_custom_json SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,override_group_id BIGINT   ENCODE lzo
	,auto_login_required SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,auto_login_username VARCHAR(512)   ENCODE lzo
	,shell_shock_only_scan SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,auto_login_verify_command VARCHAR(512)   ENCODE lzo
	,is_scan_resume SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,multithreading SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,rescan_type BIGINT   ENCODE lzo
	,verify_flaw_count BIGINT   ENCODE lzo
	,alternator_mode SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,logout_seq_file_name VARCHAR(512)   ENCODE lzo
	,custom_defined_technologies SMALLINT   ENCODE lzo
	,crawl_depth_limit BIGINT   ENCODE zstd
	,max_exchanges_per_link BIGINT   ENCODE zstd
	,subdirectory_limit BIGINT   ENCODE zstd
	,PRIMARY KEY (analysis_unit_dyn_params_id)
)
DISTSTYLE KEY
 DISTKEY (analysis_unit_dyn_params_id)
;
ALTER TABLE intake.analysis_unit_dyn_params owner to vcoderoot;

-- Drop table

-- DROP TABLE annot;

--DROP TABLE intake.annot;
CREATE TABLE IF NOT EXISTS intake.annot
(
	annot_id BIGINT NOT NULL  ENCODE zstd
	,run_ver_issue_id BIGINT NOT NULL  ENCODE zstd
	,original_issue_id BIGINT NOT NULL  ENCODE zstd
	,annot_text VARCHAR(8000)   ENCODE zstd
	,source BIGINT NOT NULL  ENCODE zstd
	,ext_visible SMALLINT   ENCODE zstd
	,reviewer_comment SMALLINT  DEFAULT 0 ENCODE zstd
	,request_followup SMALLINT  DEFAULT 0 ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,internal_action BIGINT   ENCODE zstd
	,external_action BIGINT   ENCODE zstd
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,user_name VARCHAR(512)   ENCODE zstd
	,account_id BIGINT   ENCODE RAW
	,copied_from_id BIGINT   ENCODE zstd
	,PRIMARY KEY (annot_id)
)
DISTSTYLE KEY
 DISTKEY (annot_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.annot owner to vcoderoot;

-- Drop table

-- DROP TABLE app;

--DROP TABLE intake.app;
CREATE TABLE IF NOT EXISTS intake.app
(
	app_id BIGINT NOT NULL  ENCODE zstd
	,app_name VARCHAR(512) NOT NULL  ENCODE zstd
	,app_type_id BIGINT NOT NULL DEFAULT 17 ENCODE zstd
	,description VARCHAR(8000)   ENCODE zstd
	,industry_vertical_id BIGINT   ENCODE zstd
	,nas_dir VARCHAR(512) NOT NULL  ENCODE zstd
	,app_source_id BIGINT NOT NULL  ENCODE zstd
	,account_id BIGINT NOT NULL  ENCODE RAW
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,login_account_id BIGINT   ENCODE zstd
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,app_assur_id BIGINT NOT NULL  ENCODE zstd
	,is_webapp SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,enterprise_account_id BIGINT   ENCODE lzo
	,assess_type BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,web_site_url VARCHAR(512)   ENCODE lzo
	,rdb_project_id BIGINT   ENCODE lzo
	,nas_logo_path VARCHAR(2048)   ENCODE lzo
	,deleted_files SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,profile_complete SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,archer_app_name VARCHAR(512)   ENCODE lzo
	,last_auditor_id VARCHAR(72)   ENCODE zstd
	,multi_platform SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,bus_unit_id BIGINT NOT NULL  ENCODE zstd
	,bus_owner_name VARCHAR(256)   ENCODE lzo
	,bus_owner_email VARCHAR(512)   ENCODE lzo
	,deployment_method BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,policy_group_id BIGINT NOT NULL  ENCODE zstd
	,send_policy_notifications SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,last_assigned_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,policy_locked SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,summarization_setting BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,dyn_scan_not_req_aprvl SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,dynamic_scan_type BIGINT   ENCODE lzo
	,s2_setting BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,is_dynamicmp SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,vendor_account_id BIGINT   ENCODE lzo
	,vendor_app_id BIGINT   ENCODE lzo
	,vendor_rescan SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,allow_dep_as_top_level_modules SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,did_set_original_issue_date SMALLINT   ENCODE lzo
	,original_issue_date_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,enterprise_enabled_sca SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,vendor_enabled_sca SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,remediation_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,app_remediation_status SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,xpa_enabled SMALLINT   ENCODE lzo
	,is_demo SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,next_day_scheduling_enabled SMALLINT   ENCODE lzo
	,keep_binary_days NUMERIC(20,0)   ENCODE lzo
	,s3_only SMALLINT   ENCODE lzo
	,s3_bucket_id NUMERIC(20,0)   ENCODE lzo
	,prescan_workflow SMALLINT   ENCODE zstd
	,PRIMARY KEY (app_id)
)
DISTSTYLE KEY
 DISTKEY (app_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.app owner to vcoderoot;

-- Drop table

-- DROP TABLE app_custom_field_name;

--DROP TABLE intake.app_custom_field_name;
CREATE TABLE IF NOT EXISTS intake.app_custom_field_name
(
	app_custom_field_name_id BIGINT NOT NULL  ENCODE lzo
	,account_id BIGINT NOT NULL  ENCODE RAW
	,app_custom_field_name VARCHAR(256) NOT NULL  ENCODE lzo
	,user_input_required SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,display_to_user SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,sort_order BIGINT NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,PRIMARY KEY (app_custom_field_name_id)
)
DISTSTYLE ALL
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.app_custom_field_name owner to vcoderoot;

-- Drop table

-- DROP TABLE app_custom_field_value;

--DROP TABLE intake.app_custom_field_value;
CREATE TABLE IF NOT EXISTS intake.app_custom_field_value
(
	app_custom_field_value_id BIGINT NOT NULL  ENCODE lzo
	,app_id BIGINT NOT NULL  ENCODE RAW
	,app_custom_field_name_id BIGINT NOT NULL  ENCODE lzo
	,app_custom_field_value VARCHAR(512)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,PRIMARY KEY (app_custom_field_value_id)
)
DISTSTYLE ALL
 SORTKEY (
	app_id
	)
;
ALTER TABLE intake.app_custom_field_value owner to vcoderoot;

-- Drop table

-- DROP TABLE app_file;

--DROP TABLE intake.app_file;
CREATE TABLE IF NOT EXISTS intake.app_file
(
	app_file_id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,app_file_name VARCHAR(2048) NOT NULL  ENCODE zstd
	,alias_name VARCHAR(256)   ENCODE zstd
	,cust_source_path VARCHAR(1024)   ENCODE zstd
	,san_path VARCHAR(1024)   ENCODE zstd
	,checksum VARCHAR(256)   ENCODE zstd
	,size_bytes NUMERIC(20,0)   ENCODE zstd
	,encrypted_checksum VARCHAR(256)   ENCODE zstd
	,encrypted_size_bytes NUMERIC(20,0)   ENCODE zstd
	,file_type_id NUMERIC(20,0)   ENCODE zstd
	,app_ver_id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,upload_status_id NUMERIC(20,0) NOT NULL  ENCODE zstd
	,file_last_mod_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,parent_app_file_dir_id NUMERIC(20,0)   ENCODE zstd
	,has_debug SMALLINT   ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(256) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,generated_from_app_file_id NUMERIC(20,0)   ENCODE zstd
	,structure_error NUMERIC(20,0)   ENCODE zstd
	,structure_error_component VARCHAR(1024)   ENCODE zstd
	,last_upload_status_id NUMERIC(20,0)   ENCODE zstd
	,is_debug_file SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,analysis_size_bytes NUMERIC(20,0)   ENCODE zstd
	,is_php SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,has_backup SMALLINT   ENCODE zstd
	,has_dechunked SMALLINT   ENCODE zstd
	,unzip_dir VARCHAR(512)   ENCODE zstd
	,is_classic_asp SMALLINT   ENCODE zstd
	,archive_file_name VARCHAR(256)   ENCODE zstd
	,original_app_file_name VARCHAR(256)   ENCODE zstd
	,distinct_dup_module SMALLINT   ENCODE zstd
	,from_app_file_id NUMERIC(20,0)   ENCODE lzo
	,codebase_pricing_size BIGINT   ENCODE lzo
	,s3_key VARCHAR(1024)   ENCODE lzo
	,on_s3_only SMALLINT   ENCODE lzo
	,s3_bucket_id NUMERIC(20,0)   ENCODE lzo
	,PRIMARY KEY (app_file_id)
)
DISTSTYLE KEY
 DISTKEY (app_file_id)
 SORTKEY (
	app_file_id
	, app_ver_id
	)
;
ALTER TABLE intake.app_file owner to vcoderoot;

-- Drop table

-- DROP TABLE app_tag;

--DROP TABLE intake.app_tag;
CREATE TABLE IF NOT EXISTS intake.app_tag
(
	app_tag_id BIGINT NOT NULL  ENCODE lzo
	,app_id BIGINT NOT NULL  ENCODE RAW
	,tagname VARCHAR(4096) NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,PRIMARY KEY (app_tag_id)
)
DISTSTYLE ALL
 SORTKEY (
	app_id
	)
;
ALTER TABLE intake.app_tag owner to vcoderoot;

-- Drop table

-- DROP TABLE app_team;

--DROP TABLE intake.app_team;
CREATE TABLE IF NOT EXISTS intake.app_team
(
	app_team_id BIGINT NOT NULL  ENCODE zstd
	,app_id BIGINT NOT NULL  ENCODE RAW
	,team_id BIGINT NOT NULL  ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,PRIMARY KEY (app_team_id)
)
DISTSTYLE KEY
 DISTKEY (app_team_id)
 SORTKEY (
	app_id
	)
;
ALTER TABLE intake.app_team owner to vcoderoot;

-- Drop table

-- DROP TABLE app_ver;

--DROP TABLE intake.app_ver;
CREATE TABLE IF NOT EXISTS intake.app_ver
(
	app_ver_id BIGINT NOT NULL  ENCODE zstd
	,sandbox_id BIGINT NOT NULL  ENCODE zstd
	,prev_app_ver_id BIGINT   ENCODE zstd
	,account_id BIGINT NOT NULL  ENCODE RAW
	,login_account_id BIGINT   ENCODE zstd
	,version_label VARCHAR(512) NOT NULL  ENCODE zstd
	,comments VARCHAR(8000)   ENCODE lzo
	,nas_dir VARCHAR(2048) NOT NULL  ENCODE zstd
	,lifecycle_stage_id BIGINT   ENCODE zstd
	,ship_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,tpl_search_index_ver BIGINT   ENCODE lzo
	,tpl_search_index_state BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,cmp_tpl_search_index_ver BIGINT   ENCODE lzo
	,cmp_tpl_search_index_state BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,platform_id BIGINT   ENCODE zstd
	,deleted_files SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,last_auditor_id VARCHAR(72)   ENCODE zstd
	,autopub_disabled SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,policy_id BIGINT NOT NULL  ENCODE zstd
	,score_grace_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,min_score_status BIGINT NOT NULL DEFAULT 2 ENCODE zstd
	,last_comp_check_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,grace_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,scan_freq_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,policy_status BIGINT NOT NULL DEFAULT 2 ENCODE zstd
	,rules_status BIGINT NOT NULL DEFAULT 2 ENCODE zstd
	,policy_reason BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,binaries_deleted SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,is_dynamicmp SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,remediation_scan SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,policy_evaluated_on_issue SMALLINT   ENCODE zstd
	,reevaluate_severity SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,auto_scan SMALLINT  DEFAULT 0 ENCODE zstd
	,preflight_file_name VARCHAR(512)   ENCODE zstd
	,non_fatal_scan SMALLINT  DEFAULT 0 ENCODE zstd
	,sca_rule_status BIGINT   ENCODE zstd
	,sca_grace_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,summaries_deleted SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,rt_deleted SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,templates_deleted SMALLINT  DEFAULT 0 ENCODE zstd
	,last_sca_mitigation_id BIGINT   ENCODE lzo
	,last_policy_update_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,needs_report_data_update SMALLINT   ENCODE zstd
	,is_onewas SMALLINT   ENCODE lzo
	,binaries_archived SMALLINT   ENCODE lzo
	,templates_archived SMALLINT   ENCODE lzo
	,deleted_empty_exec_dirs SMALLINT   ENCODE lzo
	,binaries_on_s3 SMALLINT   ENCODE lzo
	,s3_bucket_id NUMERIC(20,0)   ENCODE lzo
	,PRIMARY KEY (app_ver_id)
)
DISTSTYLE KEY
 DISTKEY (app_ver_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.app_ver owner to vcoderoot;

-- Drop table

-- DROP TABLE bus_unit;

--DROP TABLE intake.bus_unit;
CREATE TABLE IF NOT EXISTS intake.bus_unit
(
	bus_unit_id BIGINT NOT NULL  ENCODE lzo
	,bus_unit_name VARCHAR(512) NOT NULL  ENCODE lzo
	,account_id BIGINT NOT NULL  ENCODE RAW
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,default_bus_unit SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,PRIMARY KEY (bus_unit_id)
)
DISTSTYLE ALL
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.bus_unit owner to vcoderoot;

-- Drop table

-- DROP TABLE cust_dataset_appver;

--DROP TABLE intake.cust_dataset_appver;
CREATE TABLE IF NOT EXISTS intake.cust_dataset_appver
(
	cust_dataset_appver_id NUMERIC(18,0) NOT NULL  ENCODE zstd
	,app_ver_id NUMERIC(18,0) NOT NULL  ENCODE zstd
	,account_id NUMERIC(18,0) NOT NULL  ENCODE RAW
	,issue_dataset_id NUMERIC(18,0) NOT NULL  ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(256) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,analysis_type NUMERIC(18,0) NOT NULL DEFAULT 1 ENCODE zstd
	,rating_id NUMERIC(18,0)  DEFAULT 6 ENCODE zstd
	,PRIMARY KEY (cust_dataset_appver_id)
)
DISTSTYLE KEY
 DISTKEY (cust_dataset_appver_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.cust_dataset_appver owner to vcoderoot;

-- Drop table

-- DROP TABLE cust_dataset_runver;

--DROP TABLE intake.cust_dataset_runver;
CREATE TABLE IF NOT EXISTS intake.cust_dataset_runver
(
	cust_dataset_runver_id BIGINT NOT NULL  ENCODE zstd
	,run_ver_id BIGINT NOT NULL  ENCODE zstd
	,account_id BIGINT NOT NULL  ENCODE RAW
	,issue_dataset_id BIGINT NOT NULL  ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,app_issue_dataset_id BIGINT   ENCODE zstd
	,PRIMARY KEY (cust_dataset_runver_id)
)
DISTSTYLE KEY
 DISTKEY (cust_dataset_runver_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.cust_dataset_runver owner to vcoderoot;

-- Drop table

-- DROP TABLE custom_cleanser;

--DROP TABLE intake.custom_cleanser;
CREATE TABLE IF NOT EXISTS intake.custom_cleanser
(
	custom_cleanser_id NUMERIC(18,0) NOT NULL  ENCODE RAW
	,run_ver_issue_id NUMERIC(18,0) NOT NULL  ENCODE RAW
	,name VARCHAR(512)   ENCODE lzo
	,annotation VARCHAR(1024)   ENCODE lzo
	,frame_id NUMERIC(18,0) NOT NULL  ENCODE lzo
	,cleanser_prototype VARCHAR(1024)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(256) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,run_ver_id NUMERIC(18,0) NOT NULL DEFAULT 0 ENCODE RAW
	,user_comment VARCHAR(2048)   ENCODE lzo
	,PRIMARY KEY (custom_cleanser_id)
)
DISTSTYLE KEY
 DISTKEY (custom_cleanser_id)
 SORTKEY (
	custom_cleanser_id
	, run_ver_id
	, run_ver_issue_id
	)
;
ALTER TABLE intake.custom_cleanser owner to vcoderoot;

-- Drop table

-- DROP TABLE custom_severity;

--DROP TABLE intake.custom_severity;
CREATE TABLE IF NOT EXISTS intake.custom_severity
(
	custom_severity_id BIGINT NOT NULL  ENCODE lzo
	,policy_id BIGINT NOT NULL  ENCODE lzo
	,cwe_id BIGINT   ENCODE lzo
	,severity BIGINT NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,PRIMARY KEY (custom_severity_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.custom_severity owner to vcoderoot;

-- Drop table

-- DROP TABLE disco_decommissioned_asset;

--DROP TABLE intake.disco_decommissioned_asset;
CREATE TABLE IF NOT EXISTS intake.disco_decommissioned_asset
(
	id BIGINT NOT NULL  ENCODE zstd
	,asset_id BIGINT NOT NULL  ENCODE zstd
	,scan_ver_id BIGINT NOT NULL  ENCODE zstd
	,account_id BIGINT NOT NULL  ENCODE RAW
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,PRIMARY KEY (id)
)
DISTSTYLE KEY
 DISTKEY (id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.disco_decommissioned_asset owner to vcoderoot;

-- Drop table

-- DROP TABLE discovery_additional_host;

--DROP TABLE intake.discovery_additional_host;
CREATE TABLE IF NOT EXISTS intake.discovery_additional_host
(
	id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,discovery_scan_ver_id NUMERIC(20,0) NOT NULL  ENCODE zstd
	,agent VARCHAR(256) NOT NULL  ENCODE zstd
	,host_id NUMERIC(20,0) NOT NULL  ENCODE zstd
	,"port" NUMERIC(20,0) NOT NULL DEFAULT 0 ENCODE zstd
	,scheme VARCHAR(256)   ENCODE zstd
	,agent_key VARCHAR(256) NOT NULL  ENCODE zstd
	,ordinal NUMERIC(20,0) NOT NULL DEFAULT 0 ENCODE zstd
	,agent_value VARCHAR(1024)   ENCODE zstd
	,create_time TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(256) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,account_id NUMERIC(20,0)   ENCODE RAW
	,PRIMARY KEY (id)
)
DISTSTYLE KEY
 DISTKEY (id)
 SORTKEY (
	id
	, account_id
	)
;
ALTER TABLE intake.discovery_additional_host owner to vcoderoot;

-- Drop table

-- DROP TABLE discovery_asset_inventory;

--DROP TABLE intake.discovery_asset_inventory;
CREATE TABLE IF NOT EXISTS intake.discovery_asset_inventory
(
	id BIGINT NOT NULL  ENCODE zstd
	,scheme VARCHAR(20) NOT NULL  ENCODE zstd
	,"host" VARCHAR(512) NOT NULL  ENCODE zstd
	,"port" BIGINT NOT NULL  ENCODE zstd
	,url VARCHAR(600)   ENCODE zstd
	,account_id BIGINT NOT NULL  ENCODE RAW
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,scan_ver_id_first_found BIGINT NOT NULL  ENCODE zstd
	,scan_ver_id_last_found BIGINT NOT NULL  ENCODE zstd
	,bus_unit_id BIGINT   ENCODE lzo
	,bus_owner_name VARCHAR(256)   ENCODE lzo
	,PRIMARY KEY (id)
)
DISTSTYLE KEY
 DISTKEY (id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.discovery_asset_inventory owner to vcoderoot;

-- Drop table

-- DROP TABLE discovery_job;

--DROP TABLE intake.discovery_job;
CREATE TABLE IF NOT EXISTS intake.discovery_job
(
	discovery_job_id BIGINT NOT NULL  ENCODE lzo
	,account_id BIGINT NOT NULL  ENCODE RAW
	,name VARCHAR(512) NOT NULL  ENCODE lzo
	,input_file VARCHAR(512)   ENCODE lzo
	,discovery_status BIGINT NOT NULL DEFAULT 0 ENCODE lzo
	,request_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,upload_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,estimated_delivery_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,completed_time TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,input_site_list_count BIGINT   ENCODE lzo
	,scan_start_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,scan_end_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,start_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,s3_only SMALLINT   ENCODE lzo
	,s3_bucket_id NUMERIC(20,0)   ENCODE lzo
	,PRIMARY KEY (discovery_job_id)
)
DISTSTYLE ALL
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.discovery_job owner to vcoderoot;

-- Drop table

-- DROP TABLE discovery_scan_data;

--DROP TABLE intake.discovery_scan_data;
CREATE TABLE IF NOT EXISTS intake.discovery_scan_data
(
	id BIGINT NOT NULL  ENCODE zstd
	,scheme VARCHAR(20) NOT NULL  ENCODE zstd
	,"host" VARCHAR(512) NOT NULL  ENCODE zstd
	,open_ports VARCHAR(512)   ENCODE zstd
	,ip_address VARCHAR(512)   ENCODE zstd
	,"port" BIGINT NOT NULL  ENCODE zstd
	,registered_domain VARCHAR(512)   ENCODE zstd
	,confidence_level BIGINT   ENCODE zstd
	,in_input_ip_range VARCHAR(400)   ENCODE zstd
	,in_input_domain BIGINT   ENCODE zstd
	,found_by_display VARCHAR(512)   ENCODE zstd
	,found_from VARCHAR(512)   ENCODE zstd
	,found_from_confidence_level VARCHAR(512)   ENCODE zstd
	,found_from_in_host_list VARCHAR(512)   ENCODE lzo
	,found_from_resolved_ip VARCHAR(512)   ENCODE zstd
	,found_from_ip_match VARCHAR(512)   ENCODE lzo
	,found_from_ip_in_input_list VARCHAR(512)   ENCODE lzo
	,found_from_resolved_found_by VARCHAR(90)   ENCODE zstd
	,server_header VARCHAR(8000)   ENCODE zstd
	,location_redirect VARCHAR(8000)   ENCODE zstd
	,meta_redirect VARCHAR(8000)   ENCODE zstd
	,http_authentication VARCHAR(512)   ENCODE zstd
	,forms_authentication VARCHAR(512)   ENCODE zstd
	,response_code BIGINT   ENCODE zstd
	,dom_hash VARCHAR(8000)   ENCODE zstd
	,ie_response_code BIGINT   ENCODE zstd
	,ie9_hash VARCHAR(8000)   ENCODE zstd
	,firefox_response_code BIGINT   ENCODE zstd
	,firefox_dom_hash VARCHAR(8000)   ENCODE zstd
	,firefox_6_page_hash VARCHAR(8000)   ENCODE zstd
	,iphone_response_code BIGINT   ENCODE zstd
	,iphone_page_hash VARCHAR(8000)   ENCODE zstd
	,dynamic_mp_candidate BIGINT  DEFAULT 0 ENCODE zstd
	,authentication_display VARCHAR(90)   ENCODE zstd
	,redirected_display VARCHAR(90)   ENCODE zstd
	,response_code_display BIGINT   ENCODE zstd
	,found_from_duplicate VARCHAR(512)   ENCODE zstd
	,http_vs_https BIGINT   ENCODE zstd
	,host_id BIGINT NOT NULL  ENCODE zstd
	,visited SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,"domain" VARCHAR(512)   ENCODE zstd
	,found_by VARCHAR(512)   ENCODE zstd
	,tld VARCHAR(512)   ENCODE zstd
	,scan_scope SMALLINT   ENCODE zstd
	,wildcard SMALLINT   ENCODE zstd
	,discovery_method VARCHAR(600)   ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,is_ip SMALLINT   ENCODE zstd
	,discovery_scan_ver_id BIGINT NOT NULL  ENCODE zstd
	,time_found_date TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,ie_dom_hash VARCHAR(8000)   ENCODE zstd
	,iphone_dom_hash VARCHAR(8000)   ENCODE zstd
	,page_title VARCHAR(2048)   ENCODE zstd
	,ip_address_num BIGINT   ENCODE zstd
	,PRIMARY KEY (id)
)
DISTSTYLE KEY
 DISTKEY (id)
;
ALTER TABLE intake.discovery_scan_data owner to vcoderoot;

-- Drop table

-- DROP TABLE discovery_scan_ver;

--DROP TABLE intake.discovery_scan_ver;
CREATE TABLE IF NOT EXISTS intake.discovery_scan_ver
(
	discovery_scan_ver_id BIGINT NOT NULL  ENCODE lzo
	,discovery_job_id BIGINT NOT NULL  ENCODE lzo
	,no_of_host_list_host BIGINT   ENCODE lzo
	,hosts_found VARCHAR(512)   ENCODE lzo
	,build_version VARCHAR(512)   ENCODE lzo
	,name VARCHAR(512)   ENCODE lzo
	,host_list_id BIGINT   ENCODE lzo
	,discover_start TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,state VARCHAR(512)   ENCODE lzo
	,version BIGINT   ENCODE lzo
	,end_time TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,no_of_discovery_data BIGINT   ENCODE lzo
	,report_gen_status BIGINT  DEFAULT 1 ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,scan_publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,is_delta_populated SMALLINT  DEFAULT 0 ENCODE lzo
	,PRIMARY KEY (discovery_scan_ver_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.discovery_scan_ver owner to vcoderoot;

-- Drop table

-- DROP TABLE dynamicmp_job;

--DROP TABLE intake.dynamicmp_job;
CREATE TABLE IF NOT EXISTS intake.dynamicmp_job
(
	dynamicmp_job_id BIGINT NOT NULL  ENCODE lzo
	,account_id BIGINT NOT NULL  ENCODE lzo
	,app_id BIGINT NOT NULL  ENCODE lzo
	,name VARCHAR(512) NOT NULL  ENCODE lzo
	,sitelist_file VARCHAR(512) NOT NULL  ENCODE lzo
	,site_count BIGINT NOT NULL  ENCODE lzo
	,dynamicmp_status BIGINT NOT NULL DEFAULT 0 ENCODE lzo
	,request_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,start_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,upload_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,end_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT trunc(('now'::character varying)::timestamp without time zone) ENCODE lzo
	,scan_start_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,job_group_id VARCHAR(512)   ENCODE lzo
	,communication_type BIGINT   ENCODE lzo
	,is_onewas SMALLINT   ENCODE lzo
	,PRIMARY KEY (dynamicmp_job_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.dynamicmp_job owner to vcoderoot;

-- Drop table

-- DROP TABLE dynamicmp_link_app;

--DROP TABLE intake.dynamicmp_link_app;
CREATE TABLE IF NOT EXISTS intake.dynamicmp_link_app
(
	link_id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,dynamicmp_job_id NUMERIC(20,0) NOT NULL  ENCODE zstd
	,exec_unit_ver_id NUMERIC(20,0) NOT NULL  ENCODE zstd
	,link_app_id NUMERIC(20,0)   ENCODE zstd
	,login_account_id NUMERIC(20,0)   ENCODE RAW
	,link_app_name VARCHAR(256) NOT NULL  ENCODE zstd
	,status NUMERIC(20,0) NOT NULL DEFAULT 0 ENCODE zstd
	,error_string VARCHAR(512)  DEFAULT ''::character varying ENCODE lzo
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(256) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,no_of_retry_attempts NUMERIC(20,0)  DEFAULT 0 ENCODE zstd
	,last_retry_time TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,link_app_request_id NUMERIC(20,0) NOT NULL DEFAULT 0 ENCODE zstd
	,no_of_retry_attempts_ignored NUMERIC(20,0)  DEFAULT 0 ENCODE zstd
	,last_retry_time_for_ignored TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,PRIMARY KEY (link_id)
)
DISTSTYLE KEY
 DISTKEY (link_id)
 SORTKEY (
	link_id
	, login_account_id
	)
;
ALTER TABLE intake.dynamicmp_link_app owner to vcoderoot;

-- Drop table

-- DROP TABLE engine_job;

--DROP TABLE intake.engine_job;
CREATE TABLE IF NOT EXISTS intake.engine_job
(
	engine_job_id BIGINT NOT NULL  ENCODE zstd
	,status BIGINT NOT NULL  ENCODE zstd
	,status_string VARCHAR(2048)   ENCODE zstd
	,"type" BIGINT NOT NULL  ENCODE zstd
	,engine_machine_id BIGINT   ENCODE lzo
	,app_ver_id BIGINT NOT NULL  ENCODE zstd
	,exec_unit_ver_id BIGINT   ENCODE zstd
	,name VARCHAR(2048)   ENCODE zstd
	,login_account_id BIGINT NOT NULL  ENCODE zstd
	,account_id BIGINT NOT NULL  ENCODE RAW
	,run_ver_id BIGINT   ENCODE zstd
	,priority BIGINT NOT NULL  ENCODE zstd
	,submit_time TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,start_time TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,stop_time TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,machine_name VARCHAR(128)   ENCODE zstd
	,xml_path VARCHAR(2048)   ENCODE zstd
	,binary_path VARCHAR(2048)   ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,primary_job_id BIGINT   ENCODE lzo
	,is_backup SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,engine_version VARCHAR(64)   ENCODE zstd
	,status_xml VARCHAR(8000)   ENCODE zstd
	,start_phase BIGINT   ENCODE lzo
	,analysis_unit_id BIGINT   ENCODE zstd
	,llrn_time TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,analysis_type BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,summary_file_path VARCHAR(2048)   ENCODE zstd
	,uses_summaries SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,read_results SMALLINT NOT NULL DEFAULT 1 ENCODE zstd
	,machine_group_id BIGINT NOT NULL  ENCODE zstd
	,xml_result_file VARCHAR(512)   ENCODE zstd
	,copy_result_path VARCHAR(2048)   ENCODE zstd
	,copy_summarization_path VARCHAR(2048)   ENCODE zstd
	,maxvm BIGINT   ENCODE zstd
	,track_c BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,is_latest SMALLINT NOT NULL DEFAULT 1 ENCODE zstd
	,saf_pid BIGINT   ENCODE zstd
	,dynamic_scan_type BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,copy_result_run_ver_id BIGINT   ENCODE zstd
	,copy_summary_run_ver_id BIGINT   ENCODE zstd
	,s2_file_path VARCHAR(2048)   ENCODE zstd
	,s2_previous_path VARCHAR(2048)   ENCODE zstd
	,tmp_status NUMERIC(38,18) NOT NULL DEFAULT -1 ENCODE zstd
	,ownership_token VARCHAR(256)   ENCODE zstd
	,communication_type BIGINT   ENCODE zstd
	,results_file_track_c BIGINT   ENCODE zstd
	,is_recent_job SMALLINT   ENCODE zstd
	,skip_fast_lane SMALLINT  DEFAULT 0 ENCODE zstd
	,parallel_scan_type BIGINT   ENCODE lzo
	,parallel_scan_slice BIGINT   ENCODE lzo
	,model_job_id BIGINT   ENCODE lzo
	,dyn_sum_stats SMALLINT  DEFAULT 0 ENCODE zstd
	,scanner_name VARCHAR(512)   ENCODE zstd
	,remote_job_id VARCHAR(128)   ENCODE zstd
	,result_files_deleted SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,xpa_enabled SMALLINT   ENCODE zstd
	,s3_only SMALLINT   ENCODE lzo
	,s3_bucket_id NUMERIC(20,0)   ENCODE lzo
	,PRIMARY KEY (engine_job_id)
)
DISTSTYLE KEY
 DISTKEY (engine_job_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.engine_job owner to vcoderoot;

-- Drop table

-- DROP TABLE exec_unit_ver;

--DROP TABLE intake.exec_unit_ver;
CREATE TABLE IF NOT EXISTS intake.exec_unit_ver
(
	exec_unit_ver_id BIGINT NOT NULL  ENCODE zstd
	,account_id BIGINT NOT NULL  ENCODE RAW
	,prev_exec_unit_ver_id BIGINT   ENCODE zstd
	,app_ver_id BIGINT   ENCODE zstd
	,name VARCHAR(512)   ENCODE zstd
	,san_path VARCHAR(2048)   ENCODE zstd
	,description VARCHAR(2048)   ENCODE lzo
	,compiler VARCHAR(128)   ENCODE zstd
	,os VARCHAR(128)   ENCODE zstd
	,architecture VARCHAR(128)   ENCODE zstd
	,scan_status_id BIGINT NOT NULL  ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,exec_size BIGINT  DEFAULT 0 ENCODE zstd
	,executable_size BIGINT  DEFAULT 0 ENCODE zstd
	,has_checkpoint_data SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,analysis_unit_id BIGINT NOT NULL  ENCODE zstd
	,compiler_supported BIGINT NOT NULL DEFAULT -1 ENCODE zstd
	,arch_supported BIGINT NOT NULL DEFAULT -1 ENCODE zstd
	,platform_supported BIGINT NOT NULL DEFAULT -1 ENCODE zstd
	,analysis_supported BIGINT NOT NULL DEFAULT -1 ENCODE zstd
	,analysis_size_bytes BIGINT  DEFAULT 0 ENCODE zstd
	,summary_gen_status BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,dep_exec_unit SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,obfuscated SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,optimized SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,engine_version VARCHAR(64)   ENCODE zstd
	,dependency_checksum VARCHAR(512)   ENCODE zstd
	,enhanced_dyn SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,platform_deprecated SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,incrementally_linked SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,corrupt_headers SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,resolution_code VARCHAR(128)   ENCODE zstd
	,webapp SMALLINT  DEFAULT 0 ENCODE zstd
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,compiler_deprecated BIGINT NOT NULL DEFAULT -1 ENCODE zstd
	,links_crawled BIGINT   ENCODE zstd
	,prescan_status BIGINT   ENCODE zstd
	,prescan_response_code BIGINT   ENCODE zstd
	,redirect_url VARCHAR(2048)   ENCODE zstd
	,scan_exit_status BIGINT   ENCODE zstd
	,scanner_name VARCHAR(512)   ENCODE zstd
	,thirdparty SMALLINT  DEFAULT 0 ENCODE zstd
	,module_selected SMALLINT   ENCODE zstd
	,display_name VARCHAR(512)   ENCODE zstd
	,publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,first_party_size BIGINT   ENCODE zstd
	,first_party_executable_size BIGINT   ENCODE zstd
	,first_party_compressed_size BIGINT   ENCODE lzo
	,codebase_pricing_size BIGINT   ENCODE lzo
	,s3_only SMALLINT   ENCODE lzo
	,s3_bucket_id NUMERIC(20,0)   ENCODE lzo
	,is_scs SMALLINT   ENCODE zstd
	,PRIMARY KEY (exec_unit_ver_id)
)
DISTSTYLE KEY
 DISTKEY (exec_unit_ver_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.exec_unit_ver owner to vcoderoot;

-- Drop table

-- DROP TABLE exec_unit_ver_file;

--DROP TABLE intake.exec_unit_ver_file;
CREATE TABLE IF NOT EXISTS intake.exec_unit_ver_file
(
	exec_unit_ver_file_id BIGINT NOT NULL  ENCODE zstd
	,exec_unit_ver_id BIGINT NOT NULL  ENCODE zstd
	,app_ver_id BIGINT   ENCODE RAW
	,is_primary SMALLINT   ENCODE zstd
	,app_file_id BIGINT   ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,stub_confidence BIGINT NOT NULL DEFAULT -1 ENCODE zstd
	,loadable SMALLINT  DEFAULT 0 ENCODE zstd
	,platform_deprecated SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,incrementally_linked SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,corrupt_headers SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,obfuscated SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,optimized SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,compiler_deprecated SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,PRIMARY KEY (exec_unit_ver_file_id)
)
DISTSTYLE KEY
 DISTKEY (exec_unit_ver_file_id)
 SORTKEY (
	app_ver_id
	)
;
ALTER TABLE intake.exec_unit_ver_file owner to vcoderoot;

-- Drop table

-- DROP TABLE fixed_issue_summary;

--DROP TABLE intake.fixed_issue_summary;
CREATE TABLE IF NOT EXISTS intake.fixed_issue_summary
(
	fixed_issue_summary_id BIGINT NOT NULL  ENCODE zstd
	,issue_dataset_id BIGINT   ENCODE zstd
	,cwe_id BIGINT   ENCODE bytedict
	,count BIGINT   ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,severity BIGINT   ENCODE zstd
	,PRIMARY KEY (fixed_issue_summary_id)
)
DISTSTYLE KEY
 DISTKEY (fixed_issue_summary_id)
;
ALTER TABLE intake.fixed_issue_summary owner to vcoderoot;

-- Drop table

-- DROP TABLE green_light_issue;

--DROP TABLE intake.green_light_issue;
CREATE TABLE IF NOT EXISTS intake.green_light_issue
(
	green_light_issue_id NUMERIC(20,0) NOT NULL  ENCODE zstd
	,cust_issue_id NUMERIC(20,0) NOT NULL  ENCODE zstd
	,title VARCHAR(512)   ENCODE zstd
	,source_file_line NUMERIC(20,0)   ENCODE zstd
	,source_function_name VARCHAR(1024) NOT NULL DEFAULT 'N/A'::character varying ENCODE zstd
	,source_function_prototype VARCHAR(4000)   ENCODE zstd
	,source_relative_location NUMERIC(20,0)   ENCODE zstd
	,source_scope VARCHAR(1024)   ENCODE zstd
	,source_function_line NUMERIC(20,0)   ENCODE zstd
	,rt_file_line NUMERIC(20,0)   ENCODE zstd
	,rt_function_name VARCHAR(1024)   ENCODE zstd
	,rt_function_line NUMERIC(20,0)   ENCODE zstd
	,template_file_id NUMERIC(20,0) NOT NULL  ENCODE zstd
	,template_file_line NUMERIC(20,0)   ENCODE zstd
	,display_text_old VARCHAR(4000)   ENCODE zstd
	,issue_type VARCHAR(128)   ENCODE zstd
	,issue_type_id VARCHAR(128) NOT NULL DEFAULT 'Internal'::character varying ENCODE zstd
	,run_ver_id NUMERIC(20,0)   ENCODE zstd
	,rt_ver_file_id NUMERIC(20,0) NOT NULL  ENCODE zstd
	,sandbox_src_file_id NUMERIC(20,0) NOT NULL  ENCODE zstd
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,account_id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,current_severity NUMERIC(20,0)   ENCODE zstd
	,cwe_id NUMERIC(20,0) NOT NULL DEFAULT -666 ENCODE zstd
	,vc_id VARCHAR(32)   ENCODE zstd
	,denorm_module_name VARCHAR(256)   ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(256) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,display_text_id NUMERIC(20,0)   ENCODE lzo
	,app_src_file_id NUMERIC(20,0)   ENCODE zstd
	,PRIMARY KEY (green_light_issue_id)
)
DISTSTYLE KEY
 DISTKEY (green_light_issue_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.green_light_issue owner to vcoderoot;

-- Drop table

-- DROP TABLE issue_dataset;

--DROP TABLE intake.issue_dataset;
CREATE TABLE IF NOT EXISTS intake.issue_dataset
(
	issue_dataset_id BIGINT NOT NULL  ENCODE zstd
	,account_id BIGINT NOT NULL  ENCODE RAW
	,publish_reason BIGINT   ENCODE zstd
	,raw_score_legacy BIGINT   ENCODE zstd
	,norm_score_legacy BIGINT   ENCODE zstd
	,num_new_flaws BIGINT   ENCODE zstd
	,num_old_flaws BIGINT   ENCODE zstd
	,num_reopened_flaws BIGINT   ENCODE zstd
	,num_fixed_flaws BIGINT   ENCODE zstd
	,num_not_fixed_flaws BIGINT   ENCODE zstd
	,num_s5_flaws BIGINT   ENCODE zstd
	,num_s5_flaws_change BIGINT   ENCODE zstd
	,num_s4_flaws BIGINT   ENCODE zstd
	,num_s4_flaws_change BIGINT   ENCODE zstd
	,num_s3_flaws BIGINT   ENCODE zstd
	,num_s3_flaws_change BIGINT   ENCODE zstd
	,num_s2_flaws BIGINT   ENCODE zstd
	,num_s2_flaws_change BIGINT   ENCODE zstd
	,num_s1_flaws BIGINT   ENCODE zstd
	,num_s1_flaws_change BIGINT   ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,raw_mitigated_score_legacy BIGINT   ENCODE zstd
	,norm_mit_score_legacy BIGINT   ENCODE zstd
	,mitigated_rating BIGINT   ENCODE zstd
	,num_mitigated_flaws BIGINT   ENCODE zstd
	,num_s5_flaws_mitigated BIGINT   ENCODE zstd
	,num_s5_flaws_prop_mitigated BIGINT   ENCODE zstd
	,num_s4_flaws_mitigated BIGINT   ENCODE zstd
	,num_s4_flaws_prop_mitigated BIGINT   ENCODE zstd
	,num_s3_flaws_mitigated BIGINT   ENCODE zstd
	,num_s3_flaws_prop_mitigated BIGINT   ENCODE zstd
	,num_s2_flaws_mitigated BIGINT   ENCODE zstd
	,num_s2_flaws_prop_mitigated BIGINT   ENCODE zstd
	,num_s1_flaws_mitigated BIGINT   ENCODE zstd
	,num_s1_flaws_prop_mitigated BIGINT   ENCODE zstd
	,num_s0_flaws BIGINT  DEFAULT 0 ENCODE zstd
	,num_s0_flaws_change BIGINT  DEFAULT 0 ENCODE zstd
	,num_s0_flaws_mitigated BIGINT  DEFAULT 0 ENCODE zstd
	,num_s0_flaws_prop_mitigated BIGINT  DEFAULT 0 ENCODE zstd
	,num_owasp_04 BIGINT   ENCODE zstd
	,num_owasp_07 BIGINT   ENCODE zstd
	,num_sanstop_25 BIGINT   ENCODE zstd
	,num_owasp_10 BIGINT   ENCODE zstd
	,cond_level BIGINT   ENCODE zstd
	,cond_mitigated_level BIGINT   ENCODE zstd
	,raw_score BIGINT   ENCODE zstd
	,raw_mitigated_score BIGINT   ENCODE zstd
	,normalized_score BIGINT   ENCODE zstd
	,normalized_mitigated_score BIGINT   ENCODE zstd
	,num_s0_flaws_rej_mitigated BIGINT   ENCODE zstd
	,num_s1_flaws_rej_mitigated BIGINT   ENCODE zstd
	,num_s2_flaws_rej_mitigated BIGINT   ENCODE zstd
	,num_s3_flaws_rej_mitigated BIGINT   ENCODE zstd
	,num_s4_flaws_rej_mitigated BIGINT   ENCODE zstd
	,num_s5_flaws_rej_mitigated BIGINT   ENCODE zstd
	,num_owasp_text BIGINT   ENCODE zstd
	,num_cant_reproduce_flaws BIGINT   ENCODE zstd
	,num_s5_flaws_mit_aff_score BIGINT   ENCODE zstd
	,num_s4_flaws_mit_aff_score BIGINT   ENCODE zstd
	,num_s3_flaws_mit_aff_score BIGINT   ENCODE zstd
	,num_s2_flaws_mit_aff_score BIGINT   ENCODE zstd
	,num_s1_flaws_mit_aff_score BIGINT   ENCODE zstd
	,num_s0_flaws_mit_aff_score BIGINT   ENCODE zstd
	,num_remediated_reopened_flaws BIGINT   ENCODE zstd
	,PRIMARY KEY (issue_dataset_id)
)
DISTSTYLE KEY
 DISTKEY (issue_dataset_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.issue_dataset owner to vcoderoot;

-- Drop table

-- DROP TABLE issue_summary;

--DROP TABLE intake.issue_summary;
CREATE TABLE IF NOT EXISTS intake.issue_summary
(
	issue_summary_id BIGINT NOT NULL  ENCODE RAW
	,issue_dataset_id BIGINT   ENCODE zstd
	,cwe_id BIGINT   ENCODE bytedict
	,raw_score BIGINT   ENCODE zstd
	,count BIGINT   ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,mitigated_count BIGINT   ENCODE zstd
	,severity BIGINT   ENCODE zstd
	,PRIMARY KEY (issue_summary_id)
)
DISTSTYLE KEY
 DISTKEY (issue_dataset_id)
 SORTKEY (
	issue_summary_id
	)
;
ALTER TABLE intake.issue_summary owner to vcoderoot;

-- Drop table

-- DROP TABLE learn_course_access_rec;

--DROP TABLE intake.learn_course_access_rec;
CREATE TABLE IF NOT EXISTS intake.learn_course_access_rec
(
	learn_course_access_history_id BIGINT NOT NULL  ENCODE lzo
	,learn_course_id BIGINT NOT NULL  ENCODE lzo
	,login_account_id BIGINT NOT NULL  ENCODE lzo
	,access_time TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,PRIMARY KEY (learn_course_access_history_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.learn_course_access_rec owner to vcoderoot;

-- Drop table

-- DROP TABLE learn_curriculum;

--DROP TABLE intake.learn_curriculum;
CREATE TABLE IF NOT EXISTS intake.learn_curriculum
(
	curriculum_id BIGINT NOT NULL  ENCODE lzo
	,accountid BIGINT NOT NULL  ENCODE lzo
	,curriculum_name VARCHAR(512) NOT NULL  ENCODE lzo
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,learn_track_id BIGINT NOT NULL  ENCODE lzo
	,due_date_type_id NUMERIC(20,0)   ENCODE zstd
	,fixed_due_date TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,relative_due_date NUMERIC(20,0)   ENCODE zstd
	,relative_due_date_unit_id NUMERIC(20,0)   ENCODE zstd
	,reset_date TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,reset_duration NUMERIC(20,0)   ENCODE zstd
	,reset_type_id NUMERIC(20,0)   ENCODE zstd
	,start_date TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,end_date TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,PRIMARY KEY (curriculum_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.learn_curriculum owner to vcoderoot;

-- Drop table

-- DROP TABLE learn_track_acct_access;

--DROP TABLE intake.learn_track_acct_access;
CREATE TABLE IF NOT EXISTS intake.learn_track_acct_access
(
	learn_track_acct_access_id BIGINT NOT NULL  ENCODE lzo
	,learn_track_id BIGINT NOT NULL  ENCODE lzo
	,account_id BIGINT NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,PRIMARY KEY (learn_track_acct_access_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.learn_track_acct_access owner to vcoderoot;

-- Drop table

-- DROP TABLE login_account;

--DROP TABLE intake.login_account;
CREATE TABLE IF NOT EXISTS intake.login_account
(
	login_account_id BIGINT NOT NULL  ENCODE lzo
	,login_username_lower VARCHAR(512) NOT NULL  ENCODE zstd
	,login_password_enc VARCHAR(512) NOT NULL  ENCODE zstd
	,login_password_salt VARCHAR(512)   ENCODE lzo
	,user_first_name VARCHAR(512) NOT NULL  ENCODE lzo
	,user_last_name VARCHAR(512) NOT NULL  ENCODE lzo
	,login_enabled SMALLINT NOT NULL  ENCODE lzo
	,account_id BIGINT NOT NULL  ENCODE RAW
	,email_address VARCHAR(512) NOT NULL  ENCODE zstd
	,requires_pin SMALLINT NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,last_login_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,num_failed_logins BIGINT NOT NULL DEFAULT 0 ENCODE lzo
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,login_question VARCHAR(256)   ENCODE lzo
	,agree_terms SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,last_password_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,adv_algo SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,lms_user_identifier VARCHAR(128)   ENCODE lzo
	,lms_user_password VARCHAR(64)   ENCODE lzo
	,learn_group_id BIGINT   ENCODE lzo
	,is_learn_manager SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,curriculum_id BIGINT   ENCODE lzo
	,allow_notifications SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,login_account_type BIGINT NOT NULL DEFAULT 1 ENCODE lzo
	,last_proxy_account_id BIGINT   ENCODE lzo
	,has_ip_restriction SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,allow_multiple_sessions SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,session_id BIGINT NOT NULL DEFAULT 0 ENCODE lzo
	,show_welcome SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,show_release SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,allow_maintenance SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,last_security_lockout_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,num_failed_security_answers BIGINT NOT NULL DEFAULT 0 ENCODE lzo
	,custom_id VARCHAR(512)   ENCODE lzo
	,login_answer_enc VARCHAR(512)   ENCODE zstd
	,login_answer_salt VARCHAR(512)   ENCODE lzo
	,title VARCHAR(512)   ENCODE lzo
	,use_saml SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,learn_track_id BIGINT   ENCODE lzo
	,account_service_id BIGINT   ENCODE lzo
	,last_course_access TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,keep_elearn_active SMALLINT  DEFAULT 0 ENCODE lzo
	,custom_one VARCHAR(512)   ENCODE lzo
	,custom_two VARCHAR(512)   ENCODE lzo
	,custom_three VARCHAR(512)   ENCODE lzo
	,custom_four VARCHAR(512)   ENCODE lzo
	,custom_five VARCHAR(512)   ENCODE lzo
	,phone VARCHAR(512)   ENCODE lzo
	,active SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,birst_user_name VARCHAR(512)   ENCODE lzo
	,show_edu_video SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,show_html5_flaw_viewer_check BIGINT NOT NULL DEFAULT 1 ENCODE lzo
	,local_storage_encrypt_key VARCHAR(512)   ENCODE zstd
	,last_birst_account_id BIGINT   ENCODE lzo
	,created_by VARCHAR(512)   ENCODE zstd
	,last_login_host VARCHAR(512)   ENCODE lzo
	,last_attempt BIGINT NOT NULL DEFAULT 0 ENCODE lzo
	,last_attempt_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,last_failed_host VARCHAR(512)   ENCODE lzo
	,last_successful_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,previous_successful_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,last_successful_host VARCHAR(512)   ENCODE lzo
	,previous_successful_host VARCHAR(512)   ENCODE lzo
	,login_password_hash_ver BIGINT NOT NULL DEFAULT 2 ENCODE lzo
	,login_answer_hash_ver BIGINT  DEFAULT 2 ENCODE lzo
	,num_user_lockouts NUMERIC(20,0)   ENCODE lzo
	,user_lockout_minutes NUMERIC(20,0)   ENCODE lzo
	,last_user_lockout_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,show_owasp_policy_update SMALLINT NOT NULL DEFAULT 1 ENCODE lzo
	,allow_sca_notifications SMALLINT   ENCODE lzo
	,PRIMARY KEY (login_account_id)
)
DISTSTYLE KEY
 DISTKEY (login_account_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.login_account owner to vcoderoot;

-- Drop table

-- DROP TABLE login_account_proxy;

--DROP TABLE intake.login_account_proxy;
CREATE TABLE IF NOT EXISTS intake.login_account_proxy
(
	login_account_proxy_id BIGINT NOT NULL  ENCODE zstd
	,login_account_id BIGINT NOT NULL  ENCODE RAW
	,account_id BIGINT NOT NULL  ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(256) NOT NULL  ENCODE zstd
	,record_ver BIGINT NOT NULL  ENCODE zstd
	,PRIMARY KEY (login_account_proxy_id)
)
DISTSTYLE ALL
 SORTKEY (
	login_account_id
	)
;
ALTER TABLE intake.login_account_proxy owner to vcoderoot;

-- Drop table

-- DROP TABLE login_account_role_assign;

--DROP TABLE intake.login_account_role_assign;
CREATE TABLE IF NOT EXISTS intake.login_account_role_assign
(
	login_account_role_assign_id BIGINT NOT NULL  ENCODE RAW
	,login_account_id BIGINT NOT NULL  ENCODE zstd
	,role_id BIGINT NOT NULL  ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,PRIMARY KEY (login_account_role_assign_id)
)
DISTSTYLE KEY
 DISTKEY (login_account_id)
 SORTKEY (
	login_account_role_assign_id
	)
;
ALTER TABLE intake.login_account_role_assign owner to vcoderoot;

-- Drop table

-- DROP TABLE login_account_team_assign;

--DROP TABLE intake.login_account_team_assign;
CREATE TABLE IF NOT EXISTS intake.login_account_team_assign
(
	login_account_team_assign_id BIGINT NOT NULL  ENCODE RAW
	,login_account_id BIGINT NOT NULL  ENCODE lzo
	,team_id BIGINT NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,PRIMARY KEY (login_account_team_assign_id)
)
DISTSTYLE KEY
 DISTKEY (login_account_id)
 SORTKEY (
	login_account_team_assign_id
	)
;
ALTER TABLE intake.login_account_team_assign owner to vcoderoot;

-- Drop table

-- DROP TABLE machine_group;

--DROP TABLE intake.machine_group;
CREATE TABLE IF NOT EXISTS intake.machine_group
(
	machine_group_id BIGINT NOT NULL  ENCODE lzo
	,enabled SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,name VARCHAR(128) NOT NULL  ENCODE lzo
	,engine_ver VARCHAR(64)   ENCODE lzo
	,branch VARCHAR(128)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,failover_machine_group_id BIGINT   ENCODE lzo
	,overflow_machine_group_id BIGINT   ENCODE lzo
	,account_id BIGINT   ENCODE lzo
	,description VARCHAR(512)   ENCODE lzo
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,dynamic_support SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,static_delay_enabled SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,PRIMARY KEY (machine_group_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.machine_group owner to vcoderoot;

-- Drop table

-- DROP TABLE mitigation_conformation;

--DROP TABLE intake.mitigation_conformation;
CREATE TABLE IF NOT EXISTS intake.mitigation_conformation
(
	mitigation_conformation_id NUMERIC(20,0) NOT NULL  ENCODE zstd
	,run_ver_issue_id NUMERIC(20,0) NOT NULL  ENCODE zstd
	,original_issue_id NUMERIC(20,0) NOT NULL  ENCODE zstd
	,mitigation_review_action NUMERIC(20,0) NOT NULL  ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(256) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,PRIMARY KEY (mitigation_conformation_id)
)
DISTSTYLE ALL
;
ALTER TABLE intake.mitigation_conformation owner to vcoderoot;

-- Drop table

-- DROP TABLE policy;

--DROP TABLE intake.policy;
CREATE TABLE IF NOT EXISTS intake.policy
(
	policy_id BIGINT NOT NULL  ENCODE lzo
	,policy_group_id BIGINT   ENCODE lzo
	,"type" BIGINT NOT NULL  ENCODE lzo
	,account_id BIGINT   ENCODE RAW
	,name VARCHAR(512) NOT NULL  ENCODE lzo
	,description VARCHAR(8000) NOT NULL  ENCODE lzo
	,version BIGINT   ENCODE lzo
	,min_vlevel BIGINT   ENCODE lzo
	,any_scan_frequency BIGINT NOT NULL  ENCODE lzo
	,static_scan_frequency BIGINT NOT NULL  ENCODE lzo
	,dynamic_scan_frequency BIGINT NOT NULL  ENCODE lzo
	,manual_scan_frequency BIGINT NOT NULL  ENCODE lzo
	,s5_grace_period_days BIGINT   ENCODE lzo
	,s4_grace_period_days BIGINT   ENCODE lzo
	,s3_grace_period_days BIGINT   ENCODE lzo
	,s2_grace_period_days BIGINT   ENCODE lzo
	,s1_grace_period_days BIGINT   ENCODE lzo
	,s0_grace_period_days BIGINT   ENCODE lzo
	,score_grace_period_days BIGINT   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,share_with_vendors SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,sca_blacklist_grace_period BIGINT   ENCODE lzo
	,PRIMARY KEY (policy_id)
)
DISTSTYLE ALL
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.policy owner to vcoderoot;

-- Drop table

-- DROP TABLE policy_group;

--DROP TABLE intake.policy_group;
CREATE TABLE IF NOT EXISTS intake.policy_group
(
	policy_group_id BIGINT NOT NULL  ENCODE lzo
	,latest_policy_id BIGINT   ENCODE lzo
	,account_id BIGINT   ENCODE RAW
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,PRIMARY KEY (policy_group_id)
)
DISTSTYLE ALL
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.policy_group owner to vcoderoot;

-- Drop table

-- DROP TABLE readout_request;

--DROP TABLE intake.readout_request;
CREATE TABLE IF NOT EXISTS intake.readout_request
(
	readout_request_id BIGINT NOT NULL  ENCODE lzo
	,account_id BIGINT NOT NULL  ENCODE RAW
	,app_ver_id BIGINT NOT NULL  ENCODE lzo
	,is_complete SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,login_account_id BIGINT NOT NULL  ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,sf_case_id BIGINT NOT NULL DEFAULT 0 ENCODE lzo
	,PRIMARY KEY (readout_request_id)
)
DISTSTYLE ALL
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.readout_request owner to vcoderoot;

-- Drop table

-- DROP TABLE rpt_latest_app_ver;

--DROP TABLE intake.rpt_latest_app_ver;
CREATE TABLE IF NOT EXISTS intake.rpt_latest_app_ver
(
	rpt_latest_app_ver_id BIGINT NOT NULL  ENCODE zstd
	,sandbox_id BIGINT NOT NULL  ENCODE zstd
	,latest_app_ver_id BIGINT   ENCODE zstd
	,latest_pub_app_ver_id BIGINT   ENCODE zstd
	,latest_pub_ent_app_ver_id BIGINT   ENCODE lzo
	,latest_static_id BIGINT   ENCODE lzo
	,latest_pub_static_id BIGINT   ENCODE lzo
	,latest_pub_ent_static_id BIGINT   ENCODE lzo
	,latest_dynamic_id BIGINT   ENCODE lzo
	,latest_pub_dynamic_id BIGINT   ENCODE lzo
	,latest_pub_ent_dynamic_id BIGINT   ENCODE lzo
	,latest_pub_manual_id BIGINT   ENCODE lzo
	,latest_manual_id BIGINT   ENCODE lzo
	,latest_pub_ent_manual_id BIGINT   ENCODE lzo
	,grace_period_expire TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,scan_frequency_expire TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,grace_prd_exp_notified SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,scan_freq_prd_exp_notified SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,static_scan_due SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,dyn_scan_due SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,man_scan_due SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,score_grace_prd_exp TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,score_grace_exp_notified SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,sca_grace_period_expire TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,sca_grace_prd_exp_notified SMALLINT   ENCODE lzo
	,PRIMARY KEY (rpt_latest_app_ver_id)
)
DISTSTYLE KEY
 DISTKEY (rpt_latest_app_ver_id)
;
ALTER TABLE intake.rpt_latest_app_ver owner to vcoderoot;

-- Drop table

-- DROP TABLE rpt_pub_analysis;

--DROP TABLE intake.rpt_pub_analysis;
CREATE TABLE IF NOT EXISTS intake.rpt_pub_analysis
(
	rpt_pub_analysis_id BIGINT NOT NULL  ENCODE zstd
	,account_id BIGINT NOT NULL  ENCODE RAW
	,sandbox_id BIGINT NOT NULL  ENCODE zstd
	,app_ver_id BIGINT NOT NULL  ENCODE zstd
	,analysis_id BIGINT NOT NULL  ENCODE zstd
	,static_analysis_unit_id BIGINT   ENCODE zstd
	,static_cust_dataset_app_ver_id BIGINT   ENCODE zstd
	,static_issue_dataset_id BIGINT   ENCODE zstd
	,static_rating BIGINT   ENCODE zstd
	,static_publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,dyn_analysis_unit_id BIGINT   ENCODE zstd
	,dyn_cust_dataset_app_ver_id BIGINT   ENCODE zstd
	,dyn_issue_dataset_id BIGINT   ENCODE zstd
	,dyn_rating BIGINT   ENCODE zstd
	,dyn_publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,man_analysis_unit_id BIGINT   ENCODE lzo
	,man_cust_dataset_app_ver_id BIGINT   ENCODE lzo
	,man_issue_dataset_id BIGINT   ENCODE lzo
	,man_rating BIGINT   ENCODE lzo
	,man_publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,static_score_legacy BIGINT   ENCODE zstd
	,dyn_score_legacy BIGINT   ENCODE zstd
	,man_score_legacy BIGINT   ENCODE lzo
	,mitigated_publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,static_mitigated_rating BIGINT   ENCODE zstd
	,static_mitigated_publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,static_mitigated_score_legacy BIGINT   ENCODE zstd
	,dyn_mitigated_rating BIGINT   ENCODE lzo
	,dyn_mitigated_publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,dyn_mitigated_score_legacy BIGINT   ENCODE zstd
	,published_rating VARCHAR(12) NOT NULL DEFAULT ''::character varying ENCODE zstd
	,mitigated_rating VARCHAR(6)   ENCODE zstd
	,edast SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,dep_static_builds_to_a BIGINT   ENCODE lzo
	,dep_static_time_to_a BIGINT   ENCODE lzo
	,dep_dyn_builds_to_a BIGINT   ENCODE lzo
	,dep_dyn_time_to_a BIGINT   ENCODE lzo
	,dep_man_builds_to_a BIGINT   ENCODE lzo
	,dep_man_time_to_a BIGINT   ENCODE lzo
	,rating_sort_order VARCHAR(6) NOT NULL  ENCODE zstd
	,published_level BIGINT   ENCODE zstd
	,mitigated_level BIGINT   ENCODE zstd
	,static_cond_level BIGINT   ENCODE zstd
	,static_cond_mit_level BIGINT   ENCODE zstd
	,dyn_cond_level BIGINT   ENCODE zstd
	,dyn_cond_mit_level BIGINT   ENCODE lzo
	,man_cond_level BIGINT   ENCODE lzo
	,static_score BIGINT   ENCODE zstd
	,static_mitigated_score BIGINT   ENCODE zstd
	,dyn_score BIGINT   ENCODE zstd
	,dyn_mitigated_score BIGINT   ENCODE zstd
	,man_score BIGINT   ENCODE lzo
	,prev_static_analysis_unit_id BIGINT   ENCODE zstd
	,prev_dyn_analysis_unit_id BIGINT   ENCODE zstd
	,prev_man_analysis_unit_id BIGINT   ENCODE lzo
	,bookmark_name VARCHAR(512)   ENCODE lzo
	,man_mitigated_rating BIGINT   ENCODE lzo
	,man_cond_mit_level BIGINT   ENCODE lzo
	,man_mitigated_publish_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,man_mitigated_score BIGINT   ENCODE zstd
	,man_mitigated_score_legacy BIGINT   ENCODE zstd
	,PRIMARY KEY (rpt_pub_analysis_id)
)
DISTSTYLE KEY
 DISTKEY (rpt_pub_analysis_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.rpt_pub_analysis owner to vcoderoot;

-- Drop table

-- DROP TABLE rt_ver_file;

--DROP TABLE intake.rt_ver_file;
CREATE TABLE IF NOT EXISTS intake.rt_ver_file
(
	rt_ver_file_id BIGINT NOT NULL  ENCODE zstd
	,run_ver_id BIGINT   ENCODE zstd
	,display_file_name VARCHAR(4000)   ENCODE zstd
	,text_file_name VARCHAR(512)   ENCODE zstd
	,san_path VARCHAR(1024)   ENCODE zstd
	,internal_rt_file_id BIGINT   ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,linker_name VARCHAR(1024)   ENCODE zstd
	,PRIMARY KEY (rt_ver_file_id)
)
DISTSTYLE KEY
 DISTKEY (rt_ver_file_id)
;
ALTER TABLE intake.rt_ver_file owner to vcoderoot;

-- Drop table

-- DROP TABLE run_ver;

--DROP TABLE intake.run_ver;
CREATE TABLE IF NOT EXISTS intake.run_ver
(
	run_ver_id BIGINT NOT NULL  ENCODE zstd
	,accountid BIGINT NOT NULL  ENCODE RAW
	,exec_unit_ver_id BIGINT NOT NULL  ENCODE zstd
	,comments VARCHAR(2048)   ENCODE lzo
	,san_path VARCHAR(2048)   ENCODE zstd
	,run_status_exception VARCHAR(2048)   ENCODE lzo
	,engine_version VARCHAR(64)   ENCODE zstd
	,image_size BIGINT   ENCODE zstd
	,exemem BIGINT   ENCODE zstd
	,proceduremem BIGINT   ENCODE zstd
	,risk_points BIGINT   ENCODE zstd
	,adjusted_risk_points BIGINT   ENCODE zstd
	,lines_of_code BIGINT   ENCODE zstd
	,end_mem BIGINT   ENCODE zstd
	,high_mem BIGINT   ENCODE zstd
	,engine_machine_id BIGINT   ENCODE zstd
	,start_date_time TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,end_date_time TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,eng_som_ver_id BIGINT   ENCODE lzo
	,eng_output_file_ver_id BIGINT   ENCODE lzo
	,eng_rem_text_file_id BIGINT   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,char_search_index_ver BIGINT   ENCODE zstd
	,char_search_index_state BIGINT NOT NULL DEFAULT 3 ENCODE zstd
	,ref_search_index_ver BIGINT   ENCODE zstd
	,ref_search_index_state BIGINT NOT NULL DEFAULT 3 ENCODE zstd
	,high_vm_bytes BIGINT   ENCODE zstd
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,image_language VARCHAR(512)   ENCODE zstd
	,read_hints SMALLINT   ENCODE zstd
	,wrote_hints SMALLINT   ENCODE zstd
	,total_modules BIGINT   ENCODE zstd
	,total_vuln_call_sites BIGINT   ENCODE zstd
	,total_call_sites BIGINT   ENCODE zstd
	,fop_version BIGINT   ENCODE zstd
	,PRIMARY KEY (run_ver_id)
)
DISTSTYLE KEY
 DISTKEY (run_ver_id)
 SORTKEY (
	accountid
	)
;
ALTER TABLE intake.run_ver owner to vcoderoot;

-- Drop table

-- DROP TABLE run_ver_issue;

--DROP TABLE intake.run_ver_issue;
CREATE TABLE IF NOT EXISTS intake.run_ver_issue
(
	run_ver_issue_id BIGINT NOT NULL  ENCODE zstd
	,cust_issue_id BIGINT NOT NULL  ENCODE zstd
	,title VARCHAR(1024)   ENCODE zstd
	,original_issue_id BIGINT   ENCODE zstd
	,source_file_line BIGINT   ENCODE zstd
	,source_col BIGINT   ENCODE zstd
	,source_function_name VARCHAR(2048) NOT NULL DEFAULT 'N/A'::character varying ENCODE zstd
	,source_function_line BIGINT   ENCODE zstd
	,rt_file_line BIGINT   ENCODE zstd
	,rt_function_name VARCHAR(2048)   ENCODE zstd
	,rt_function_line BIGINT   ENCODE zstd
	,display_text VARCHAR(8000)   ENCODE zstd
	,issue_type VARCHAR(256)   ENCODE zstd
	,issue_type_id VARCHAR(256) NOT NULL DEFAULT 'Internal'::character varying ENCODE zstd
	,run_ver_id BIGINT NOT NULL  ENCODE zstd
	,rt_ver_file_id BIGINT NOT NULL DEFAULT -1 ENCODE zstd
	,sandbox_src_file_id BIGINT NOT NULL DEFAULT -1 ENCODE zstd
	,stack_exact_checksum VARCHAR(512)   ENCODE zstd
	,stack_fuzzy_checksum VARCHAR(512)   ENCODE zstd
	,param_checksum VARCHAR(512)   ENCODE zstd
	,account_id BIGINT NOT NULL  ENCODE RAW
	,internal_review_status BIGINT NOT NULL  ENCODE zstd
	,external_review_status BIGINT NOT NULL  ENCODE zstd
	,issue_history_state BIGINT NOT NULL  ENCODE zstd
	,issue_state BIGINT NOT NULL  ENCODE zstd
	,prev_issue BIGINT   ENCODE zstd
	,current_severity BIGINT NOT NULL  ENCODE zstd
	,internal_owner BIGINT   ENCODE zstd
	,internal_review_done SMALLINT   ENCODE zstd
	,external_review_requested SMALLINT   ENCODE zstd
	,external_proposed_fixed SMALLINT   ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,internal_reason BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,duplicate_issue BIGINT   ENCODE zstd
	,cwe_id BIGINT NOT NULL DEFAULT -666 ENCODE zstd
	,template_file_id BIGINT NOT NULL DEFAULT -1 ENCODE zstd
	,template_file_line BIGINT   ENCODE zstd
	,original_exploit_level BIGINT   ENCODE zstd
	,current_exploit_level BIGINT   ENCODE zstd
	,current_exploit_desc VARCHAR(4096)   ENCODE zstd
	,current_severity_desc VARCHAR(4096)   ENCODE zstd
	,remediation_desc VARCHAR(4096)   ENCODE zstd
	,man_remediation_effort BIGINT   ENCODE zstd
	,cnt BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,cvss NUMERIC(5,3)   ENCODE zstd
	,capec_id BIGINT   ENCODE zstd
	,vc_id VARCHAR(64)   ENCODE zstd
	,exploit_difficulty BIGINT   ENCODE zstd
	,denorm_module_name VARCHAR(512)   ENCODE zstd
	,has_exploit_adjustment SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,mitigation_status BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,source_function_prototype VARCHAR(8000)   ENCODE zstd
	,source_relative_location BIGINT   ENCODE zstd
	,source_scope VARCHAR(2048)   ENCODE zstd
	,confidence BIGINT   ENCODE zstd
	,confidence_display BIGINT   ENCODE zstd
	,auto_mark_valid SMALLINT   ENCODE zstd
	,processing_note VARCHAR(4096)   ENCODE zstd
	,last_state_change_by VARCHAR(512)   ENCODE zstd
	,last_reopen_au_id BIGINT   ENCODE zstd
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,issue_seq_number BIGINT   ENCODE zstd
	,secure_and_unsecure_protocols SMALLINT   ENCODE zstd
	,procedure_name VARCHAR(8000)   ENCODE zstd
	,procedure_hash VARCHAR(512)   ENCODE zstd
	,prototype_hash VARCHAR(512)   ENCODE zstd
	,statement_hash VARCHAR(512)   ENCODE zstd
	,statement_hash_count BIGINT   ENCODE zstd
	,statement_hash_ordinal BIGINT   ENCODE zstd
	,match_query_type BIGINT   ENCODE zstd
	,policy_rule_passed SMALLINT   ENCODE zstd
	,eng_issue_id BIGINT   ENCODE zstd
	,cause_hash VARCHAR(512)   ENCODE zstd
	,cause_hash_count BIGINT   ENCODE zstd
	,cause_hash_ordinal BIGINT   ENCODE zstd
	,original_issue_date TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,previous_stack_exact_checksum VARCHAR(512)   ENCODE zstd
	,stack_exact_checksum_version BIGINT   ENCODE zstd
	,original_flaw_status BIGINT   ENCODE zstd
	,remediated_reopened_status BIGINT   ENCODE zstd
	,submodule_path VARCHAR(256)   ENCODE zstd
	,display_text_id NUMERIC(20,0)   ENCODE lzo
	,app_src_file_id NUMERIC(20,0)   ENCODE zstd
	,PRIMARY KEY (run_ver_issue_id)
)
DISTSTYLE KEY
 DISTKEY (run_ver_issue_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.run_ver_issue owner to vcoderoot;

-- Drop table

-- DROP TABLE sandbox;

--DROP TABLE intake.sandbox;
CREATE TABLE IF NOT EXISTS intake.sandbox
(
	sandbox_id BIGINT NOT NULL  ENCODE zstd
	,sandbox_name VARCHAR(512) NOT NULL  ENCODE zstd
	,account_id BIGINT NOT NULL  ENCODE RAW
	,app_id BIGINT NOT NULL  ENCODE zstd
	,policy_sandbox SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,creator_login_id BIGINT   ENCODE zstd
	,created_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,last_auditor_id VARCHAR(72)   ENCODE zstd
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,dyn_unique_issue_pop_status BIGINT  DEFAULT 1 ENCODE zstd
	,dyn_uniq_issu_chksm_pop_sts BIGINT  DEFAULT 1 ENCODE zstd
	,dyn_uniq_issu_pop_retry_cnt BIGINT   ENCODE lzo
	,deleted_files SMALLINT   ENCODE lzo
	,expired_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE zstd
	,purge_on_promote SMALLINT   ENCODE zstd
	,PRIMARY KEY (sandbox_id)
)
DISTSTYLE KEY
 DISTKEY (sandbox_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.sandbox owner to vcoderoot;

-- Drop table

-- DROP TABLE sandbox_src_file;

--DROP TABLE intake.sandbox_src_file;
CREATE TABLE IF NOT EXISTS intake.sandbox_src_file
(
	sandbox_src_file_id BIGINT NOT NULL  ENCODE zstd
	,sandbox_id BIGINT   ENCODE RAW
	,filename VARCHAR(8000)   ENCODE zstd
	,local_path VARCHAR(8000)   ENCODE zstd
	,lines_of_code BIGINT   ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,ref_guid VARCHAR(64)   ENCODE zstd
	,platform_generated SMALLINT  DEFAULT 0 ENCODE zstd
	,analysis_type BIGINT  DEFAULT 1 ENCODE zstd
	,sanitized SMALLINT   ENCODE zstd
	,dyn_type BIGINT   ENCODE zstd
	,crawl_status BIGINT   ENCODE zstd
	,orig_exec_unit_ver_id BIGINT   ENCODE lzo
	,filename_hashcode NUMERIC(38,18)   ENCODE lzo
	,PRIMARY KEY (sandbox_src_file_id)
)
DISTSTYLE KEY
 DISTKEY (sandbox_src_file_id)
 SORTKEY (
	sandbox_id
	)
;
ALTER TABLE intake.sandbox_src_file owner to vcoderoot;

-- Drop table

-- DROP TABLE shared_report;

--DROP TABLE intake.shared_report;
CREATE TABLE IF NOT EXISTS intake.shared_report
(
	shared_report_id NUMERIC(20,0) NOT NULL  ENCODE lzo
	,account_id NUMERIC(20,0) NOT NULL  ENCODE RAW
	,enterprise_account_id NUMERIC(20,0) NOT NULL  ENCODE lzo
	,name VARCHAR(256) NOT NULL  ENCODE lzo
	,name_version NUMERIC(20,0) NOT NULL DEFAULT 1 ENCODE lzo
	,policy_id NUMERIC(20,0) NOT NULL  ENCODE lzo
	,app_id NUMERIC(20,0) NOT NULL  ENCODE lzo
	,app_ver_id NUMERIC(20,0)   ENCODE lzo
	,static_id NUMERIC(20,0)   ENCODE lzo
	,static_scan_name VARCHAR(256)   ENCODE lzo
	,dynamic_id NUMERIC(20,0)   ENCODE lzo
	,dynamic_scan_name VARCHAR(256)   ENCODE lzo
	,manual_id NUMERIC(20,0)   ENCODE lzo
	,manual_scan_name VARCHAR(256)   ENCODE lzo
	,generated_by NUMERIC(20,0)   ENCODE lzo
	,generated_on TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,shared_by NUMERIC(20,0)   ENCODE lzo
	,file_exchange_id NUMERIC(20,0)   ENCODE lzo
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(256) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,policy_status NUMERIC(20,0)   ENCODE lzo
	,rule_status NUMERIC(20,0)   ENCODE lzo
	,scan_status NUMERIC(20,0)   ENCODE lzo
	,enterprise_app_id NUMERIC(20,0)   ENCODE lzo
	,shared_ts TIMESTAMP WITHOUT TIME ZONE   ENCODE lzo
	,published_level NUMERIC(20,0)   ENCODE lzo
	,mitigated_level NUMERIC(20,0)   ENCODE lzo
	,static_score NUMERIC(20,0)   ENCODE lzo
	,dyn_score NUMERIC(20,0)   ENCODE lzo
	,man_score NUMERIC(20,0)   ENCODE lzo
	,is_static_mitigated SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,is_dyn_mitigated SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,is_man_mitigated SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,shared_sca SMALLINT   ENCODE lzo
	,last_sca_mitigation_id NUMERIC(20,0)   ENCODE lzo
	,enable_sca_legacy SMALLINT   ENCODE lzo
	,PRIMARY KEY (shared_report_id)
)
DISTSTYLE KEY
 DISTKEY (shared_report_id)
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.shared_report owner to vcoderoot;

-- Drop table

-- DROP TABLE team;

--DROP TABLE intake.team;
CREATE TABLE IF NOT EXISTS intake.team
(
	team_id BIGINT NOT NULL  ENCODE lzo
	,team_name VARCHAR(512) NOT NULL  ENCODE lzo
	,account_id BIGINT NOT NULL  ENCODE RAW
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE lzo
	,modified_by VARCHAR(512) NOT NULL  ENCODE lzo
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE lzo
	,deleted SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,for_share_report SMALLINT NOT NULL DEFAULT 0 ENCODE lzo
	,bus_unit_id BIGINT   ENCODE lzo
	,PRIMARY KEY (team_id)
)
DISTSTYLE ALL
 SORTKEY (
	account_id
	)
;
ALTER TABLE intake.team owner to vcoderoot;

-- Drop table

-- DROP TABLE template_file;

--DROP TABLE intake.template_file;
CREATE TABLE IF NOT EXISTS intake.template_file
(
	template_file_id BIGINT NOT NULL  ENCODE zstd
	,app_file_id BIGINT   ENCODE zstd
	,template_name VARCHAR(8000)   ENCODE zstd
	,app_src_name VARCHAR(8000)   ENCODE zstd
	,template_src_path VARCHAR(8000)   ENCODE zstd
	,app_src_path VARCHAR(8000)   ENCODE zstd
	,template_san_path VARCHAR(8000)   ENCODE zstd
	,mapping_san_path VARCHAR(8000)   ENCODE zstd
	,app_src_san_path VARCHAR(8000)   ENCODE zstd
	,sandbox_src_file_id BIGINT   ENCODE zstd
	,included_by_other SMALLINT   ENCODE zstd
	,compile_status BIGINT  DEFAULT 1 ENCODE RAW
	,compile_error_filename VARCHAR(8000)   ENCODE zstd
	,compile_error_line BIGINT   ENCODE zstd
	,compile_error_msg VARCHAR(8000)   ENCODE lzo
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,platform_generated SMALLINT  DEFAULT 0 ENCODE zstd
	,component_name VARCHAR(8000)   ENCODE zstd
	,sanitized SMALLINT NOT NULL DEFAULT 0 ENCODE zstd
	,use_template_as_rt SMALLINT   ENCODE zstd
	,app_src_file_id NUMERIC(20,0)   ENCODE zstd
	,PRIMARY KEY (template_file_id)
)
DISTSTYLE KEY
 DISTKEY (template_file_id)
 SORTKEY (
	compile_status
	)
;
ALTER TABLE intake.template_file owner to vcoderoot;

-- Drop table

-- DROP TABLE unsup_frm_issue;

--DROP TABLE intake.unsup_frm_issue;
CREATE TABLE IF NOT EXISTS intake.unsup_frm_issue
(
	unsup_frm_issue_id BIGINT NOT NULL  ENCODE zstd
	,exec_unit_ver_id BIGINT NOT NULL  ENCODE zstd
	,framework_name VARCHAR(1024) NOT NULL  ENCODE zstd
	,token VARCHAR(256) NOT NULL  ENCODE zstd
	,insert_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL  ENCODE zstd
	,modified_by VARCHAR(512) NOT NULL  ENCODE zstd
	,record_ver NUMERIC(38,0) NOT NULL DEFAULT 1 ENCODE zstd
	,issue_level BIGINT NOT NULL DEFAULT 1 ENCODE zstd
	,"type" BIGINT NOT NULL DEFAULT 1 ENCODE RAW
	,PRIMARY KEY (unsup_frm_issue_id)
)
DISTSTYLE KEY
 DISTKEY (unsup_frm_issue_id)
 SORTKEY (
	"type"
	)
;
ALTER TABLE intake.unsup_frm_issue owner to vcoderoot;

ALTER TABLE intake.account ADD CONSTRAINT accountdefaultcoursevisibili FOREIGN KEY (default_course_visibility) REFERENCES enum_coursevisibility(enum_id);
ALTER TABLE intake.account ADD CONSTRAINT accountreadoutstatefk FOREIGN KEY (readout_state) REFERENCES enum_readoutstate(enum_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountaccountstatus FOREIGN KEY (account_status) REFERENCES enum_accountstatus(enum_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountaccounttype FOREIGN KEY (account_type) REFERENCES enum_accounttype(enum_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountcotsagreestate FOREIGN KEY (cots_agree) REFERENCES enum_legalagreestate(enum_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountcotsagreeuser FOREIGN KEY (cots_agree_id) REFERENCES login_account(login_account_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountcreatoraccountid FOREIGN KEY (creator_account_id) REFERENCES account(account_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountcreatorloginaccount FOREIGN KEY (creator_login_account_id) REFERENCES login_account(login_account_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountdynamicmachinegroup FOREIGN KEY (dynamic_machine_group_id) REFERENCES machine_group(machine_group_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountdynamicscantype FOREIGN KEY (dynamic_scan_type) REFERENCES enum_dynamicscantype(enum_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountindustryverticalid FOREIGN KEY (industry_vertical_id) REFERENCES code(code_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountlastcontacteduser FOREIGN KEY (last_contracted_id) REFERENCES login_account(login_account_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountmachinegroupid FOREIGN KEY (machine_group_id) REFERENCES machine_group(machine_group_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountmanagerid FOREIGN KEY (manager_id) REFERENCES login_account(login_account_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountstaticautopubsettin FOREIGN KEY (static_auto_pub_setting) REFERENCES enum_staticautopubsetting(enum_id);
ALTER TABLE intake.account ADD CONSTRAINT fkaccountvendortype FOREIGN KEY (vendor_type) REFERENCES enum_vendortype(enum_id);

ALTER TABLE intake.account_feature_switches ADD CONSTRAINT fkaccount_feature_switchesac FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.account_feature_switches ADD CONSTRAINT fkaccount_feature_switchesfe FOREIGN KEY (feature_id) REFERENCES enum_feature(enum_id);

ALTER TABLE intake.account_service ADD CONSTRAINT fkaccount_serviceaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.account_service ADD CONSTRAINT fkaccount_serviceappsizedefi FOREIGN KEY (app_size_definition) REFERENCES enum_appsizedefinition(enum_id);
ALTER TABLE intake.account_service ADD CONSTRAINT fkaccount_servicesubscriptio FOREIGN KEY (subscription) REFERENCES subscription(subscription_id);

ALTER TABLE intake.analysis ADD CONSTRAINT fkanalysisappverid FOREIGN KEY (app_ver_id) REFERENCES app_ver(app_ver_id);

ALTER TABLE intake.analysis_unit ADD CONSTRAINT fkanalysis_unitanalysisid FOREIGN KEY (analysis_id) REFERENCES analysis(analysis_id);
ALTER TABLE intake.analysis_unit ADD CONSTRAINT fkanalysis_unitanalysistype FOREIGN KEY (analysis_type) REFERENCES enum_analysistype(enum_id);
ALTER TABLE intake.analysis_unit ADD CONSTRAINT fkanalysis_unitautopubstatus FOREIGN KEY (auto_pub_status) REFERENCES enum_autopubstatus(enum_id);
ALTER TABLE intake.analysis_unit ADD CONSTRAINT fkanalysis_unitcreatorlogina FOREIGN KEY (creator_login_account_id) REFERENCES login_account(login_account_id);
ALTER TABLE intake.analysis_unit ADD CONSTRAINT fkanalysis_unitdynamicscanty FOREIGN KEY (dynamic_scan_type) REFERENCES enum_dynamicscantype(enum_id);
ALTER TABLE intake.analysis_unit ADD CONSTRAINT fkanalysis_uniterrormsgid FOREIGN KEY (error_msg_id) REFERENCES enum_scanjoberrortype(enum_id);
ALTER TABLE intake.analysis_unit ADD CONSTRAINT fkanalysis_unitestimateready FOREIGN KEY (eta_estimate_type) REFERENCES enum_etatype(enum_id);
ALTER TABLE intake.analysis_unit ADD CONSTRAINT fkanalysis_unitfixedflawstat FOREIGN KEY (fixed_flaw_state) REFERENCES enum_aufixedflawstate(enum_id);
ALTER TABLE intake.analysis_unit ADD CONSTRAINT fkanalysis_unitpromotefromsc FOREIGN KEY (promote_from_scan_id) REFERENCES analysis_unit(analysis_unit_id);
ALTER TABLE intake.analysis_unit ADD CONSTRAINT fkanalysis_unitscanstatusid FOREIGN KEY (scan_status_id) REFERENCES code(code_id);
ALTER TABLE intake.analysis_unit ADD CONSTRAINT fkanalysis_unitsubmitterlogi FOREIGN KEY (submitter_login_account_id) REFERENCES login_account(login_account_id);

ALTER TABLE intake.analysis_unit_dyn_op ADD CONSTRAINT fkanalysis_unit_dyn_opengine FOREIGN KEY (engine_job_id) REFERENCES engine_job(engine_job_id);
ALTER TABLE intake.analysis_unit_dyn_op ADD CONSTRAINT fkanalysis_unit_dyn_opexecun FOREIGN KEY (exec_unit_ver_id) REFERENCES exec_unit_ver(exec_unit_ver_id);
ALTER TABLE intake.analysis_unit_dyn_op ADD CONSTRAINT fkanalysis_unit_dyn_opscanex FOREIGN KEY (scan_exit_status) REFERENCES enum_scanexitstatus(enum_id);

ALTER TABLE intake.analysis_unit_dyn_params ADD CONSTRAINT fkanalysis_unit_dyn_paramscr FOREIGN KEY (crawl_seq_file_type) REFERENCES enum_crawlfiletype(enum_id);
ALTER TABLE intake.analysis_unit_dyn_params ADD CONSTRAINT fkanalysis_unit_dyn_paramsdy FOREIGN KEY (dynamic_template_source) REFERENCES enum_dynamictemplatesource(enum_id);
ALTER TABLE intake.analysis_unit_dyn_params ADD CONSTRAINT fkanalysis_unit_dyn_paramsov FOREIGN KEY (override_group_id) REFERENCES machine_group(machine_group_id);
ALTER TABLE intake.analysis_unit_dyn_params ADD CONSTRAINT fkanalysis_unit_dyn_paramsre FOREIGN KEY (rescan_type) REFERENCES enum_dynamicrescantype(enum_id);
ALTER TABLE intake.analysis_unit_dyn_params ADD CONSTRAINT fkanalysis_unit_dyn_paramsve FOREIGN KEY (veracode_dynamic_threshold) REFERENCES enum_veracodedynamicthreshold(enum_id);
ALTER TABLE intake.analysis_unit_dyn_params ADD CONSTRAINT fkanalysis_unit_dyn_paramsvg FOREIGN KEY (vsa_group_id) REFERENCES machine_group(machine_group_id);

ALTER TABLE intake.annot ADD CONSTRAINT fkannotaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.annot ADD CONSTRAINT fkannotcopiedfromid FOREIGN KEY (copied_from_id) REFERENCES annot(annot_id);
ALTER TABLE intake.annot ADD CONSTRAINT fkannotexternalaction FOREIGN KEY (external_action) REFERENCES enum_mitigationaction(enum_id);
ALTER TABLE intake.annot ADD CONSTRAINT fkannotinternalaction FOREIGN KEY (internal_action) REFERENCES enum_issuestate(enum_id);
ALTER TABLE intake.annot ADD CONSTRAINT fkannotoriginalissueid FOREIGN KEY (original_issue_id) REFERENCES run_ver_issue(run_ver_issue_id);
ALTER TABLE intake.annot ADD CONSTRAINT fkannotrunverissueid FOREIGN KEY (run_ver_issue_id) REFERENCES run_ver_issue(run_ver_issue_id);
ALTER TABLE intake.annot ADD CONSTRAINT fkannotsource FOREIGN KEY (source) REFERENCES code(code_id);

ALTER TABLE intake.app ADD CONSTRAINT fkappaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.app ADD CONSTRAINT fkappappassuranceid FOREIGN KEY (app_assur_id) REFERENCES code(code_id);
ALTER TABLE intake.app ADD CONSTRAINT fkappappremediationstatus FOREIGN KEY (app_remediation_status) REFERENCES enum_appremediationstatus(enum_id);
ALTER TABLE intake.app ADD CONSTRAINT fkappappsourceid FOREIGN KEY (app_source_id) REFERENCES code(code_id);
ALTER TABLE intake.app ADD CONSTRAINT fkappapptypeid FOREIGN KEY (app_type_id) REFERENCES code(code_id);
ALTER TABLE intake.app ADD CONSTRAINT fkappassessmenttype FOREIGN KEY (assess_type) REFERENCES enum_assessmenttype(enum_id);
ALTER TABLE intake.app ADD CONSTRAINT fkappbusunitid FOREIGN KEY (bus_unit_id) REFERENCES bus_unit(bus_unit_id);
ALTER TABLE intake.app ADD CONSTRAINT fkappdeploymentmethod FOREIGN KEY (deployment_method) REFERENCES enum_deploymentmethod(enum_id);
ALTER TABLE intake.app ADD CONSTRAINT fkappdynamicscantype FOREIGN KEY (dynamic_scan_type) REFERENCES enum_dynamicscantype(enum_id);
ALTER TABLE intake.app ADD CONSTRAINT fkappenterpriseaccountid FOREIGN KEY (enterprise_account_id) REFERENCES account(account_id);
ALTER TABLE intake.app ADD CONSTRAINT fkappindustryverticalid FOREIGN KEY (industry_vertical_id) REFERENCES code(code_id);
ALTER TABLE intake.app ADD CONSTRAINT fkapplastauditid FOREIGN KEY (last_auditor_id) REFERENCES auditor(auditor_id);
ALTER TABLE intake.app ADD CONSTRAINT fkapploginaccountid FOREIGN KEY (login_account_id) REFERENCES login_account(login_account_id);
ALTER TABLE intake.app ADD CONSTRAINT fkapppolicygroupid FOREIGN KEY (policy_group_id) REFERENCES policy_group(policy_group_id);
ALTER TABLE intake.app ADD CONSTRAINT fkappvendoraccountid FOREIGN KEY (vendor_account_id) REFERENCES account(account_id);
ALTER TABLE intake.app ADD CONSTRAINT fkappvendorappid FOREIGN KEY (vendor_app_id) REFERENCES app(app_id);

ALTER TABLE intake.app_custom_field_name ADD CONSTRAINT fkapp_custom_field_nameaccou FOREIGN KEY (account_id) REFERENCES account(account_id);

ALTER TABLE intake.app_custom_field_value ADD CONSTRAINT fkapp_custom_field_valueappc FOREIGN KEY (app_custom_field_name_id) REFERENCES app_custom_field_name(app_custom_field_name_id);
ALTER TABLE intake.app_custom_field_value ADD CONSTRAINT fkapp_custom_field_valueappi FOREIGN KEY (app_id) REFERENCES app(app_id);

ALTER TABLE intake.app_file ADD CONSTRAINT fkapp_fileappverid FOREIGN KEY (app_ver_id) REFERENCES app_ver(app_ver_id);
ALTER TABLE intake.app_file ADD CONSTRAINT fkapp_filefiletypeid FOREIGN KEY (file_type_id) REFERENCES code(code_id);
ALTER TABLE intake.app_file ADD CONSTRAINT fkapp_filefromappfileid FOREIGN KEY (from_app_file_id) REFERENCES app_file(app_file_id);
ALTER TABLE intake.app_file ADD CONSTRAINT fkapp_filegeneratedfromappfi FOREIGN KEY (generated_from_app_file_id) REFERENCES app_file(app_file_id);
ALTER TABLE intake.app_file ADD CONSTRAINT fkapp_filelastpreflightuploa FOREIGN KEY (last_upload_status_id) REFERENCES code(code_id);
ALTER TABLE intake.app_file ADD CONSTRAINT fkapp_filestructureerror FOREIGN KEY (structure_error) REFERENCES enum_structureerror(enum_id);
ALTER TABLE intake.app_file ADD CONSTRAINT fkapp_fileuploadstatusid FOREIGN KEY (upload_status_id) REFERENCES code(code_id);

ALTER TABLE intake.app_tag ADD CONSTRAINT fkapp_tagappid FOREIGN KEY (app_id) REFERENCES app(app_id);

ALTER TABLE intake.app_team ADD CONSTRAINT fkapp_teamappid FOREIGN KEY (app_id) REFERENCES app(app_id);
ALTER TABLE intake.app_team ADD CONSTRAINT fkapp_teamteamid FOREIGN KEY (team_id) REFERENCES team(team_id);

ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_veraccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_vercompiledtemplatesea FOREIGN KEY (cmp_tpl_search_index_state) REFERENCES enum_indexstate(enum_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_verlastauditid FOREIGN KEY (last_auditor_id) REFERENCES auditor(auditor_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_verlifecyclestageid FOREIGN KEY (lifecycle_stage_id) REFERENCES code(code_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_verloginaccountid FOREIGN KEY (login_account_id) REFERENCES login_account(login_account_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_verminscorestatus FOREIGN KEY (min_score_status) REFERENCES enum_policycompliancestatus(enum_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_verplatformid FOREIGN KEY (platform_id) REFERENCES code(code_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_verpolicyid FOREIGN KEY (policy_id) REFERENCES policy(policy_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_verpolicystatus FOREIGN KEY (policy_status) REFERENCES enum_policycompliancestatus(enum_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_verpolicyupdatereason FOREIGN KEY (policy_reason) REFERENCES enum_policyupdatereason(enum_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_verprevappverid FOREIGN KEY (prev_app_ver_id) REFERENCES app_ver(app_ver_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_verrulesstatus FOREIGN KEY (rules_status) REFERENCES enum_policycompliancestatus(enum_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_versandboxid FOREIGN KEY (sandbox_id) REFERENCES sandbox(sandbox_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_verscarulestatus FOREIGN KEY (sca_rule_status) REFERENCES enum_policycompliancestatus(enum_id);
ALTER TABLE intake.app_ver ADD CONSTRAINT fkapp_vertemplatesearchindex FOREIGN KEY (tpl_search_index_state) REFERENCES enum_indexstate(enum_id);

ALTER TABLE intake.bus_unit ADD CONSTRAINT fkbus_unitaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);

ALTER TABLE intake.cust_dataset_appver ADD CONSTRAINT fkcust_dataset_appveraccount FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.cust_dataset_appver ADD CONSTRAINT fkcust_dataset_appveranalysi FOREIGN KEY (analysis_type) REFERENCES enum_analysistype(enum_id);
ALTER TABLE intake.cust_dataset_appver ADD CONSTRAINT fkcust_dataset_appverappveri FOREIGN KEY (app_ver_id) REFERENCES app_ver(app_ver_id);
ALTER TABLE intake.cust_dataset_appver ADD CONSTRAINT fkcust_dataset_appverissueda FOREIGN KEY (issue_dataset_id) REFERENCES issue_dataset(issue_dataset_id);
ALTER TABLE intake.cust_dataset_appver ADD CONSTRAINT fkcust_dataset_appverratingi FOREIGN KEY (rating_id) REFERENCES enum_rating(enum_id);

ALTER TABLE intake.cust_dataset_runver ADD CONSTRAINT fkcust_dataset_runveraccount FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.cust_dataset_runver ADD CONSTRAINT fkcust_dataset_runverappissu FOREIGN KEY (app_issue_dataset_id) REFERENCES issue_dataset(issue_dataset_id);
ALTER TABLE intake.cust_dataset_runver ADD CONSTRAINT fkcust_dataset_runverissueda FOREIGN KEY (issue_dataset_id) REFERENCES issue_dataset(issue_dataset_id);
ALTER TABLE intake.cust_dataset_runver ADD CONSTRAINT fkcust_dataset_runverrunveri FOREIGN KEY (run_ver_id) REFERENCES run_ver(run_ver_id);

ALTER TABLE intake.custom_cleanser ADD CONSTRAINT fkcustom_cleanserrunverid FOREIGN KEY (run_ver_id) REFERENCES run_ver(run_ver_id);
ALTER TABLE intake.custom_cleanser ADD CONSTRAINT fkcustom_cleanserrunverissue FOREIGN KEY (run_ver_issue_id) REFERENCES run_ver_issue(run_ver_issue_id);

ALTER TABLE intake.custom_severity ADD CONSTRAINT fkcustom_severitycweid FOREIGN KEY (cwe_id) REFERENCES cwe(cwe_id);
ALTER TABLE intake.custom_severity ADD CONSTRAINT fkcustom_severitypolicyid FOREIGN KEY (policy_id) REFERENCES policy(policy_id);
ALTER TABLE intake.custom_severity ADD CONSTRAINT fkcustom_severityseverity FOREIGN KEY (severity) REFERENCES enum_issueseverity(enum_id);

ALTER TABLE intake.disco_decommissioned_asset ADD CONSTRAINT fkdisco_decomm_account_id FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.disco_decommissioned_asset ADD CONSTRAINT fkdisco_decomm_asset_id FOREIGN KEY (asset_id) REFERENCES discovery_asset_inventory(id);
ALTER TABLE intake.disco_decommissioned_asset ADD CONSTRAINT fkdisco_decomm_scan_ver_id FOREIGN KEY (scan_ver_id) REFERENCES discovery_scan_ver(discovery_scan_ver_id);

ALTER TABLE intake.discovery_additional_host ADD CONSTRAINT fkdiscovery_additional_hosta FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.discovery_additional_host ADD CONSTRAINT fkdiscovery_additional_hostd FOREIGN KEY (discovery_scan_ver_id) REFERENCES discovery_scan_ver(discovery_scan_ver_id);

ALTER TABLE intake.discovery_asset_inventory ADD CONSTRAINT fkasset_inventorysfirst FOREIGN KEY (scan_ver_id_first_found) REFERENCES discovery_scan_ver(discovery_scan_ver_id);
ALTER TABLE intake.discovery_asset_inventory ADD CONSTRAINT fkasset_inventoryslast FOREIGN KEY (scan_ver_id_last_found) REFERENCES discovery_scan_ver(discovery_scan_ver_id);
ALTER TABLE intake.discovery_asset_inventory ADD CONSTRAINT fkdiscovery_asset_inventorya FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.discovery_asset_inventory ADD CONSTRAINT fkdiscovery_asset_inventoryb FOREIGN KEY (bus_unit_id) REFERENCES bus_unit(bus_unit_id);

ALTER TABLE intake.discovery_job ADD CONSTRAINT fkdiscovery_jobaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.discovery_job ADD CONSTRAINT fkdiscovery_jobdiscoverystat FOREIGN KEY (discovery_status) REFERENCES enum_discoverystatus(enum_id);

ALTER TABLE intake.discovery_scan_data ADD CONSTRAINT fkdiscovery_scan_dataconfide FOREIGN KEY (confidence_level) REFERENCES enum_discoveryconfidencelevel(enum_id);
ALTER TABLE intake.discovery_scan_data ADD CONSTRAINT fkdiscovery_scan_datadiscove FOREIGN KEY (discovery_scan_ver_id) REFERENCES discovery_scan_ver(discovery_scan_ver_id);
ALTER TABLE intake.discovery_scan_data ADD CONSTRAINT fkdiscovery_scan_datadynamic FOREIGN KEY (dynamic_mp_candidate) REFERENCES enum_discoverydynmpcandidate(enum_id);
ALTER TABLE intake.discovery_scan_data ADD CONSTRAINT fkdiscovery_scan_datahttpvsh FOREIGN KEY (http_vs_https) REFERENCES enum_discoveryhttpvshttps(enum_id);
ALTER TABLE intake.discovery_scan_data ADD CONSTRAINT fkdiscovery_scan_dataininput FOREIGN KEY (in_input_domain) REFERENCES enum_discoveryininputdomain(enum_id);
ALTER TABLE intake.discovery_scan_data ADD CONSTRAINT fkdiscovery_scan_datarespons FOREIGN KEY (response_code_display) REFERENCES enum_discoveryresponsecode(enum_id);

ALTER TABLE intake.discovery_scan_ver ADD CONSTRAINT fkdiscovery_scan_verdiscover FOREIGN KEY (discovery_job_id) REFERENCES discovery_job(discovery_job_id);
ALTER TABLE intake.discovery_scan_ver ADD CONSTRAINT fkdiscovery_scan_verreportge FOREIGN KEY (report_gen_status) REFERENCES enum_reportgenstatus(enum_id);

ALTER TABLE intake.dynamicmp_job ADD CONSTRAINT fkdynamicmp_jobaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.dynamicmp_job ADD CONSTRAINT fkdynamicmp_jobappid FOREIGN KEY (app_id) REFERENCES app(app_id);
ALTER TABLE intake.dynamicmp_job ADD CONSTRAINT fkdynamicmp_jobcommunication FOREIGN KEY (communication_type) REFERENCES enum_communicationtype(enum_id);
ALTER TABLE intake.dynamicmp_job ADD CONSTRAINT fkdynamicmp_jobdynamicmpstat FOREIGN KEY (dynamicmp_status) REFERENCES enum_dynamicmpstatus(enum_id);

ALTER TABLE intake.dynamicmp_link_app ADD CONSTRAINT fkdynamicmp_link_appdynamicm FOREIGN KEY (dynamicmp_job_id) REFERENCES dynamicmp_job(dynamicmp_job_id);
ALTER TABLE intake.dynamicmp_link_app ADD CONSTRAINT fkdynamicmp_link_appexecunit FOREIGN KEY (exec_unit_ver_id) REFERENCES exec_unit_ver(exec_unit_ver_id);
ALTER TABLE intake.dynamicmp_link_app ADD CONSTRAINT fkdynamicmp_link_applinkappi FOREIGN KEY (link_app_id) REFERENCES app(app_id);
ALTER TABLE intake.dynamicmp_link_app ADD CONSTRAINT fkdynamicmp_link_apploginacc FOREIGN KEY (login_account_id) REFERENCES login_account(login_account_id);
ALTER TABLE intake.dynamicmp_link_app ADD CONSTRAINT fkdynamicmp_link_appstatus FOREIGN KEY (status) REFERENCES enum_linkappjobstatus(enum_id);

ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobanalysistype FOREIGN KEY (analysis_type) REFERENCES enum_analysistype(enum_id);
ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobappverid FOREIGN KEY (app_ver_id) REFERENCES app_ver(app_ver_id);
ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobdynamicscantype FOREIGN KEY (dynamic_scan_type) REFERENCES enum_dynamicscantype(enum_id);
ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobexecunitverid FOREIGN KEY (exec_unit_ver_id) REFERENCES exec_unit_ver(exec_unit_ver_id);
ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobloginaccountid FOREIGN KEY (login_account_id) REFERENCES login_account(login_account_id);
ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobmachinegroupid FOREIGN KEY (machine_group_id) REFERENCES machine_group(machine_group_id);
ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobmodeljobid FOREIGN KEY (model_job_id) REFERENCES engine_job(engine_job_id);
ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobparallelscantype FOREIGN KEY (parallel_scan_type) REFERENCES enum_parallelscantype(enum_id);
ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobprimaryjobid FOREIGN KEY (primary_job_id) REFERENCES engine_job(engine_job_id);
ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobstartphase FOREIGN KEY (start_phase) REFERENCES enum_enginephase(enum_id);
ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobstatus FOREIGN KEY (status) REFERENCES enum_jobstatus(enum_id);
ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobtrackc FOREIGN KEY (track_c) REFERENCES enum_trackcstatus(enum_id);
ALTER TABLE intake.engine_job ADD CONSTRAINT fkengine_jobtype FOREIGN KEY ("type") REFERENCES enum_jobtype(enum_id);

ALTER TABLE intake.exec_unit_ver ADD CONSTRAINT fkexec_unit_veraccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.exec_unit_ver ADD CONSTRAINT fkexec_unit_verappverid FOREIGN KEY (app_ver_id) REFERENCES app_ver(app_ver_id);
ALTER TABLE intake.exec_unit_ver ADD CONSTRAINT fkexec_unit_verprescanstatus FOREIGN KEY (prescan_status) REFERENCES enum_prescanstatus(enum_id);
ALTER TABLE intake.exec_unit_ver ADD CONSTRAINT fkexec_unit_verprevexecunitv FOREIGN KEY (prev_exec_unit_ver_id) REFERENCES exec_unit_ver(exec_unit_ver_id);
ALTER TABLE intake.exec_unit_ver ADD CONSTRAINT fkexec_unit_verscanexitstatu FOREIGN KEY (scan_exit_status) REFERENCES enum_scanexitstatus(enum_id);
ALTER TABLE intake.exec_unit_ver ADD CONSTRAINT fkexec_unit_verscanstatusid FOREIGN KEY (scan_status_id) REFERENCES code(code_id);
ALTER TABLE intake.exec_unit_ver ADD CONSTRAINT fkexec_unit_versummarygensta FOREIGN KEY (summary_gen_status) REFERENCES enum_summarygenstatus(enum_id);

ALTER TABLE intake.exec_unit_ver_file ADD CONSTRAINT fkexec_unit_ver_fileappfilei FOREIGN KEY (app_file_id) REFERENCES app_file(app_file_id);
ALTER TABLE intake.exec_unit_ver_file ADD CONSTRAINT fkexec_unit_ver_fileappverid FOREIGN KEY (app_ver_id) REFERENCES app_ver(app_ver_id);
ALTER TABLE intake.exec_unit_ver_file ADD CONSTRAINT fkexec_unit_ver_fileexecunit FOREIGN KEY (exec_unit_ver_id) REFERENCES exec_unit_ver(exec_unit_ver_id);

ALTER TABLE intake.fixed_issue_summary ADD CONSTRAINT fkfixed_issue_summarycweid FOREIGN KEY (cwe_id) REFERENCES cwe(cwe_id);
ALTER TABLE intake.fixed_issue_summary ADD CONSTRAINT fkfixed_issue_summaryissueda FOREIGN KEY (issue_dataset_id) REFERENCES issue_dataset(issue_dataset_id);
ALTER TABLE intake.fixed_issue_summary ADD CONSTRAINT fkfixed_issue_summaryseverit FOREIGN KEY (severity) REFERENCES enum_issueseverity(enum_id);

ALTER TABLE intake.green_light_issue ADD CONSTRAINT fkgreen_light_issueaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.green_light_issue ADD CONSTRAINT fkgreen_light_issuecurrentse FOREIGN KEY (current_severity) REFERENCES enum_issueseverity(enum_id);
ALTER TABLE intake.green_light_issue ADD CONSTRAINT fkgreen_light_issuecweid FOREIGN KEY (cwe_id) REFERENCES cwe(cwe_id);
ALTER TABLE intake.green_light_issue ADD CONSTRAINT fkgreen_light_issuertverfile FOREIGN KEY (rt_ver_file_id) REFERENCES rt_ver_file(rt_ver_file_id);
ALTER TABLE intake.green_light_issue ADD CONSTRAINT fkgreen_light_issuerunverid FOREIGN KEY (run_ver_id) REFERENCES run_ver(run_ver_id);
ALTER TABLE intake.green_light_issue ADD CONSTRAINT fkgreen_light_issuesandboxsr FOREIGN KEY (sandbox_src_file_id) REFERENCES sandbox_src_file(sandbox_src_file_id);
ALTER TABLE intake.green_light_issue ADD CONSTRAINT fkgreen_light_issuetemplatef FOREIGN KEY (template_file_id) REFERENCES template_file(template_file_id);

ALTER TABLE intake.issue_dataset ADD CONSTRAINT fkissue_datasetaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.issue_dataset ADD CONSTRAINT fkissue_datasetconditionalle FOREIGN KEY (cond_level) REFERENCES enum_veracodelevel(enum_id);
ALTER TABLE intake.issue_dataset ADD CONSTRAINT fkissue_datasetconditionalmi FOREIGN KEY (cond_mitigated_level) REFERENCES enum_veracodelevel(enum_id);
ALTER TABLE intake.issue_dataset ADD CONSTRAINT fkissue_datasetmitigatedrati FOREIGN KEY (mitigated_rating) REFERENCES enum_rating(enum_id);
ALTER TABLE intake.issue_dataset ADD CONSTRAINT fkissue_datasetpublishreason FOREIGN KEY (publish_reason) REFERENCES code(code_id);

ALTER TABLE intake.issue_summary ADD CONSTRAINT fkissue_summarycweid FOREIGN KEY (cwe_id) REFERENCES cwe(cwe_id);
ALTER TABLE intake.issue_summary ADD CONSTRAINT fkissue_summaryissuedataseti FOREIGN KEY (issue_dataset_id) REFERENCES issue_dataset(issue_dataset_id);
ALTER TABLE intake.issue_summary ADD CONSTRAINT fkissue_summaryseverity FOREIGN KEY (severity) REFERENCES enum_issueseverity(enum_id);

ALTER TABLE intake.learn_course_access_rec ADD CONSTRAINT fklearn_course_access_reclog FOREIGN KEY (login_account_id) REFERENCES login_account(login_account_id);

ALTER TABLE intake.learn_curriculum ADD CONSTRAINT fklearn_curriculumaccountid FOREIGN KEY (accountid) REFERENCES account(account_id);
ALTER TABLE intake.learn_curriculum ADD CONSTRAINT fklearn_curriculumlearntrack FOREIGN KEY (learn_track_id) REFERENCES learn_track(learn_track_id);

ALTER TABLE intake.learn_track_acct_access ADD CONSTRAINT fklearn_track_acct_accessacc FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.learn_track_acct_access ADD CONSTRAINT fklearn_track_acct_accesslea FOREIGN KEY (learn_track_id) REFERENCES learn_track(learn_track_id);

ALTER TABLE intake.login_account ADD CONSTRAINT fklogin_accountaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.login_account ADD CONSTRAINT fklogin_accountlastattempt FOREIGN KEY (last_attempt) REFERENCES enum_lastattempt(enum_id);
ALTER TABLE intake.login_account ADD CONSTRAINT fklogin_accountlastbirstacco FOREIGN KEY (last_birst_account_id) REFERENCES account(account_id);
ALTER TABLE intake.login_account ADD CONSTRAINT fklogin_accountlastproxyacco FOREIGN KEY (last_proxy_account_id) REFERENCES account(account_id);
ALTER TABLE intake.login_account ADD CONSTRAINT fklogin_accountlearnaccounts FOREIGN KEY (account_service_id) REFERENCES account_service(account_service_id);
ALTER TABLE intake.login_account ADD CONSTRAINT fklogin_accountlearntrackid FOREIGN KEY (learn_track_id) REFERENCES learn_track(learn_track_id);
ALTER TABLE intake.login_account ADD CONSTRAINT fklogin_accountloginaccountt FOREIGN KEY (login_account_type) REFERENCES enum_loginaccounttype(enum_id);
ALTER TABLE intake.login_account ADD CONSTRAINT fklogin_accountshowhtml5flaw FOREIGN KEY (show_html5_flaw_viewer_check) REFERENCES enum_flawviewerversionpopup(enum_id);
ALTER TABLE intake.login_account ADD CONSTRAINT learncurriculum FOREIGN KEY (curriculum_id) REFERENCES learn_curriculum(curriculum_id);

ALTER TABLE intake.login_account_proxy ADD CONSTRAINT fklogin_account_proxyaccount FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.login_account_proxy ADD CONSTRAINT fklogin_account_proxyloginac FOREIGN KEY (login_account_id) REFERENCES login_account(login_account_id);

ALTER TABLE intake.login_account_role_assign ADD CONSTRAINT fklogin_account_role_assignl FOREIGN KEY (login_account_id) REFERENCES login_account(login_account_id);
ALTER TABLE intake.login_account_role_assign ADD CONSTRAINT fklogin_account_role_assignr FOREIGN KEY (role_id) REFERENCES role(role_id);

ALTER TABLE intake.login_account_team_assign ADD CONSTRAINT fklogin_account_team_assignl FOREIGN KEY (login_account_id) REFERENCES login_account(login_account_id);
ALTER TABLE intake.login_account_team_assign ADD CONSTRAINT fklogin_account_team_assignt FOREIGN KEY (team_id) REFERENCES team(team_id);

ALTER TABLE intake.machine_group ADD CONSTRAINT fkmachine_groupaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.machine_group ADD CONSTRAINT fkmachine_groupfailovermachi FOREIGN KEY (failover_machine_group_id) REFERENCES machine_group(machine_group_id);
ALTER TABLE intake.machine_group ADD CONSTRAINT fkmachine_groupoverflowmachi FOREIGN KEY (overflow_machine_group_id) REFERENCES machine_group(machine_group_id);

ALTER TABLE intake.mitigation_conformation ADD CONSTRAINT fkmitigation_conformationori FOREIGN KEY (original_issue_id) REFERENCES run_ver_issue(run_ver_issue_id);
ALTER TABLE intake.mitigation_conformation ADD CONSTRAINT fkmitigation_conformationrun FOREIGN KEY (run_ver_issue_id) REFERENCES run_ver_issue(run_ver_issue_id);

ALTER TABLE intake.policy ADD CONSTRAINT fkpolicyaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.policy ADD CONSTRAINT fkpolicyanyscantypefrequency FOREIGN KEY (any_scan_frequency) REFERENCES enum_scanfrequency(enum_id);
ALTER TABLE intake.policy ADD CONSTRAINT fkpolicydynamicscanfrequency FOREIGN KEY (dynamic_scan_frequency) REFERENCES enum_scanfrequency(enum_id);
ALTER TABLE intake.policy ADD CONSTRAINT fkpolicymanualscanfrequency FOREIGN KEY (manual_scan_frequency) REFERENCES enum_scanfrequency(enum_id);
ALTER TABLE intake.policy ADD CONSTRAINT fkpolicyminvlevel FOREIGN KEY (min_vlevel) REFERENCES enum_veracodelevel(enum_id);
ALTER TABLE intake.policy ADD CONSTRAINT fkpolicypolicygroupid FOREIGN KEY (policy_group_id) REFERENCES policy_group(policy_group_id);
ALTER TABLE intake.policy ADD CONSTRAINT fkpolicystaticscanfrequency FOREIGN KEY (static_scan_frequency) REFERENCES enum_scanfrequency(enum_id);
ALTER TABLE intake.policy ADD CONSTRAINT fkpolicytype FOREIGN KEY ("type") REFERENCES enum_policytype(enum_id);

ALTER TABLE intake.policy_group ADD CONSTRAINT fkpolicy_groupaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.policy_group ADD CONSTRAINT fkpolicy_grouplatestpolicyid FOREIGN KEY (latest_policy_id) REFERENCES policy(policy_id);

ALTER TABLE intake.readout_request ADD CONSTRAINT fkreadout_requestaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.readout_request ADD CONSTRAINT fkreadout_requestappverid FOREIGN KEY (app_ver_id) REFERENCES app_ver(app_ver_id);
ALTER TABLE intake.readout_request ADD CONSTRAINT fkreadout_requestloginaccoun FOREIGN KEY (login_account_id) REFERENCES login_account(login_account_id);

ALTER TABLE intake.rpt_latest_app_ver ADD CONSTRAINT fkrpt_latest_app_verlatestap FOREIGN KEY (latest_app_ver_id) REFERENCES app_ver(app_ver_id);
ALTER TABLE intake.rpt_latest_app_ver ADD CONSTRAINT fkrpt_latest_app_verlatestpu FOREIGN KEY (latest_pub_app_ver_id) REFERENCES app_ver(app_ver_id);
ALTER TABLE intake.rpt_latest_app_ver ADD CONSTRAINT fkrpt_latest_app_versandboxi FOREIGN KEY (sandbox_id) REFERENCES sandbox(sandbox_id);
ALTER TABLE intake.rpt_latest_app_ver ADD CONSTRAINT rptlatest_pub_ent_appver FOREIGN KEY (latest_pub_ent_app_ver_id) REFERENCES app_ver(app_ver_id);

ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT dyncndmitlvl FOREIGN KEY (dyn_cond_mit_level) REFERENCES enum_veracodelevel(enum_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysisaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysisanalysisid FOREIGN KEY (analysis_id) REFERENCES analysis(analysis_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysisappverid FOREIGN KEY (app_ver_id) REFERENCES app_ver(app_ver_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysisdynamiccon FOREIGN KEY (dyn_cond_level) REFERENCES enum_veracodelevel(enum_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysisdynamiccus FOREIGN KEY (dyn_cust_dataset_app_ver_id) REFERENCES cust_dataset_appver(cust_dataset_appver_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysisdynamiciss FOREIGN KEY (dyn_issue_dataset_id) REFERENCES issue_dataset(issue_dataset_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysisdynamicmit FOREIGN KEY (dyn_mitigated_rating) REFERENCES enum_rating(enum_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysisdynamicrat FOREIGN KEY (dyn_rating) REFERENCES enum_rating(enum_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysismanconmitl FOREIGN KEY (man_cond_mit_level) REFERENCES enum_veracodelevel(enum_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysismanualcond FOREIGN KEY (man_cond_level) REFERENCES enum_veracodelevel(enum_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysismanualcust FOREIGN KEY (man_cust_dataset_app_ver_id) REFERENCES cust_dataset_appver(cust_dataset_appver_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysismanualissu FOREIGN KEY (man_issue_dataset_id) REFERENCES issue_dataset(issue_dataset_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysismanualmiti FOREIGN KEY (man_mitigated_rating) REFERENCES enum_rating(enum_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysismanualrati FOREIGN KEY (man_rating) REFERENCES enum_rating(enum_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysismitigatedl FOREIGN KEY (mitigated_level) REFERENCES enum_veracodelevel(enum_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysispublishedl FOREIGN KEY (published_level) REFERENCES enum_veracodelevel(enum_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysissandboxid FOREIGN KEY (sandbox_id) REFERENCES sandbox(sandbox_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysisstaticcond FOREIGN KEY (static_cond_level) REFERENCES enum_veracodelevel(enum_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysisstaticcust FOREIGN KEY (static_cust_dataset_app_ver_id) REFERENCES cust_dataset_appver(cust_dataset_appver_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysisstaticissu FOREIGN KEY (static_issue_dataset_id) REFERENCES issue_dataset(issue_dataset_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysisstaticmiti FOREIGN KEY (static_mitigated_rating) REFERENCES enum_rating(enum_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT fkrpt_pub_analysisstaticrati FOREIGN KEY (static_rating) REFERENCES enum_rating(enum_id);
ALTER TABLE intake.rpt_pub_analysis ADD CONSTRAINT stcndmitlvl FOREIGN KEY (static_cond_mit_level) REFERENCES enum_veracodelevel(enum_id);

ALTER TABLE intake.rt_ver_file ADD CONSTRAINT fkrt_ver_filerunverid FOREIGN KEY (run_ver_id) REFERENCES run_ver(run_ver_id);

ALTER TABLE intake.run_ver ADD CONSTRAINT fkrun_vercharsearchindexstat FOREIGN KEY (char_search_index_state) REFERENCES enum_indexstate(enum_id);
ALTER TABLE intake.run_ver ADD CONSTRAINT fkrun_verexecunitverid FOREIGN KEY (exec_unit_ver_id) REFERENCES exec_unit_ver(exec_unit_ver_id);
ALTER TABLE intake.run_ver ADD CONSTRAINT fkrun_verrefsearchindexstate FOREIGN KEY (ref_search_index_state) REFERENCES enum_indexstate(enum_id);

ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issueaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issuecurrentseveri FOREIGN KEY (current_severity) REFERENCES enum_issueseverity(enum_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issuecweid FOREIGN KEY (cwe_id) REFERENCES cwe(cwe_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issueduplicateissu FOREIGN KEY (duplicate_issue) REFERENCES run_ver_issue(run_ver_issue_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issueexploitdiffic FOREIGN KEY (exploit_difficulty) REFERENCES enum_exploitdifficulty(enum_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issueexternalrevie FOREIGN KEY (external_review_status) REFERENCES enum_reviewstatus(enum_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issueinternalowner FOREIGN KEY (internal_owner) REFERENCES login_account(login_account_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issueinternalreaso FOREIGN KEY (internal_reason) REFERENCES enum_internalreason(enum_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issueinternalrevie FOREIGN KEY (internal_review_status) REFERENCES enum_reviewstatus(enum_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issueissuehistorys FOREIGN KEY (issue_history_state) REFERENCES enum_issuehistorystate(enum_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issueissuestate FOREIGN KEY (issue_state) REFERENCES enum_issuestate(enum_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issuemitigationsta FOREIGN KEY (mitigation_status) REFERENCES enum_mitigationstatus(enum_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issueoriginalflaws FOREIGN KEY (original_flaw_status) REFERENCES enum_issuehistorystate(enum_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issueprevissue FOREIGN KEY (prev_issue) REFERENCES run_ver_issue(run_ver_issue_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issueremediatedreo FOREIGN KEY (remediated_reopened_status) REFERENCES enum_remediatedreopenedstate(enum_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issuertverfileid FOREIGN KEY (rt_ver_file_id) REFERENCES rt_ver_file(rt_ver_file_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issuerunverid FOREIGN KEY (run_ver_id) REFERENCES run_ver(run_ver_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issuesandboxsrcfil FOREIGN KEY (sandbox_src_file_id) REFERENCES sandbox_src_file(sandbox_src_file_id);
ALTER TABLE intake.run_ver_issue ADD CONSTRAINT fkrun_ver_issuetemplatefilei FOREIGN KEY (template_file_id) REFERENCES template_file(template_file_id);

ALTER TABLE intake.sandbox ADD CONSTRAINT fksandboxaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.sandbox ADD CONSTRAINT fksandboxappid FOREIGN KEY (app_id) REFERENCES app(app_id);
ALTER TABLE intake.sandbox ADD CONSTRAINT fksandboxcreatorloginid FOREIGN KEY (creator_login_id) REFERENCES login_account(login_account_id);
ALTER TABLE intake.sandbox ADD CONSTRAINT fksandboxdynuniqueissuecheck FOREIGN KEY (dyn_uniq_issu_chksm_pop_sts) REFERENCES enum_uniqissuchksumpopstatus(enum_id);
ALTER TABLE intake.sandbox ADD CONSTRAINT fksandboxdynuniqueissuepopst FOREIGN KEY (dyn_unique_issue_pop_status) REFERENCES enum_uniqueissuepopstatus(enum_id);
ALTER TABLE intake.sandbox ADD CONSTRAINT fksandboxlastauditid FOREIGN KEY (last_auditor_id) REFERENCES auditor(auditor_id);

ALTER TABLE intake.sandbox_src_file ADD CONSTRAINT fksandbox_src_fileanalysisty FOREIGN KEY (analysis_type) REFERENCES enum_analysistype(enum_id);
ALTER TABLE intake.sandbox_src_file ADD CONSTRAINT fksandbox_src_filecrawlstatu FOREIGN KEY (crawl_status) REFERENCES enum_crawlstatus(enum_id);
ALTER TABLE intake.sandbox_src_file ADD CONSTRAINT fksandbox_src_filedyntype FOREIGN KEY (dyn_type) REFERENCES enum_appdyntype(enum_id);
ALTER TABLE intake.sandbox_src_file ADD CONSTRAINT fksandbox_src_fileorigexecun FOREIGN KEY (orig_exec_unit_ver_id) REFERENCES exec_unit_ver(exec_unit_ver_id);
ALTER TABLE intake.sandbox_src_file ADD CONSTRAINT fksandbox_src_filesandboxid FOREIGN KEY (sandbox_id) REFERENCES sandbox(sandbox_id);

ALTER TABLE intake.shared_report ADD CONSTRAINT fkshared_reportaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.shared_report ADD CONSTRAINT fkshared_reportappid FOREIGN KEY (app_id) REFERENCES app(app_id);
ALTER TABLE intake.shared_report ADD CONSTRAINT fkshared_reportappverid FOREIGN KEY (app_ver_id) REFERENCES app_ver(app_ver_id);
ALTER TABLE intake.shared_report ADD CONSTRAINT fkshared_reportenterpriseacc FOREIGN KEY (enterprise_account_id) REFERENCES account(account_id);
ALTER TABLE intake.shared_report ADD CONSTRAINT fkshared_reportenterpriseapp FOREIGN KEY (enterprise_app_id) REFERENCES app(app_id);
ALTER TABLE intake.shared_report ADD CONSTRAINT fkshared_reportgeneratedby FOREIGN KEY (generated_by) REFERENCES login_account(login_account_id);
ALTER TABLE intake.shared_report ADD CONSTRAINT fkshared_reportmitigatedleve FOREIGN KEY (mitigated_level) REFERENCES enum_veracodelevel(enum_id);
ALTER TABLE intake.shared_report ADD CONSTRAINT fkshared_reportpolicyid FOREIGN KEY (policy_id) REFERENCES policy(policy_id);
ALTER TABLE intake.shared_report ADD CONSTRAINT fkshared_reportpolicystatus FOREIGN KEY (policy_status) REFERENCES enum_policycompliancestatus(enum_id);
ALTER TABLE intake.shared_report ADD CONSTRAINT fkshared_reportpublishedleve FOREIGN KEY (published_level) REFERENCES enum_veracodelevel(enum_id);
ALTER TABLE intake.shared_report ADD CONSTRAINT fkshared_reportrulestatus FOREIGN KEY (rule_status) REFERENCES enum_policycompliancestatus(enum_id);
ALTER TABLE intake.shared_report ADD CONSTRAINT fkshared_reportscanstatus FOREIGN KEY (scan_status) REFERENCES enum_policycompliancestatus(enum_id);
ALTER TABLE intake.shared_report ADD CONSTRAINT fkshared_reportsharedby FOREIGN KEY (shared_by) REFERENCES login_account(login_account_id);

ALTER TABLE intake.team ADD CONSTRAINT fkteamaccountid FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE intake.team ADD CONSTRAINT fkteambusinessunitid FOREIGN KEY (bus_unit_id) REFERENCES bus_unit(bus_unit_id);

ALTER TABLE intake.template_file ADD CONSTRAINT fktemplate_fileappfileid FOREIGN KEY (app_file_id) REFERENCES app_file(app_file_id);
ALTER TABLE intake.template_file ADD CONSTRAINT fktemplate_filecompilestatus FOREIGN KEY (compile_status) REFERENCES enum_templatecompilestatus(enum_id);
ALTER TABLE intake.template_file ADD CONSTRAINT fktemplate_filesandboxsrcfil FOREIGN KEY (sandbox_src_file_id) REFERENCES sandbox_src_file(sandbox_src_file_id);

ALTER TABLE intake.unsup_frm_issue ADD CONSTRAINT fkunsup_frm_issueexecunitver FOREIGN KEY (exec_unit_ver_id) REFERENCES exec_unit_ver(exec_unit_ver_id);
ALTER TABLE intake.unsup_frm_issue ADD CONSTRAINT fkunsup_frm_issueissuelevel FOREIGN KEY (issue_level) REFERENCES enum_issuelevel(enum_id);
ALTER TABLE intake.unsup_frm_issue ADD CONSTRAINT fkunsup_frm_issuetype FOREIGN KEY ("type") REFERENCES enum_issuetype(enum_id);