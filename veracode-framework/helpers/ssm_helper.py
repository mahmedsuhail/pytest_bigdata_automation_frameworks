import boto3


class get_ssm_params:
    # This method is created to read SSM keys from SSM
    def get_ssm_keys(self, ssm_key):
        try:
            client = boto3.client('ssm')
            response = client.get_parameter(
                Name=ssm_key, WithDecryption=True)
            value = response['Parameter']['Value']
            return value
        except Exception as e:
            print('There is some error, kindly check the SSM keys to get it fix')
            print(ssm_key)
            print(e)