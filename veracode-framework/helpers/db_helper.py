"""
----------------------------------------------------------------------------------------------------------
Description:

usage: DB Helper

Author  : Usman Zahid
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Usman Zahid        VDL-247              Initial draft.
03/19/2019        Usman Zahid        VDL-247              Initial draft.
-----------------------------------------------------------------------------------------------------------
"""
import pymysql
import pandas as pd
from helpers.ssm_helper import *
import json
from helpers.configurations_helper import *

environment = env

class db_helper:

    def build_connection(self,env):
        ssm_class_object = get_ssm_params()
        if env == 'source':
            host = ssm_class_object.get_ssm_keys('/qa/vdl/test/' + env + '/aurora/Host')
            username = ssm_class_object.get_ssm_keys('/qa/vdl/test/'+env +'/aurora/USER')
            password = ssm_class_object.get_ssm_keys('/qa/vdl/test/' + env + '/aurora/PASSWORD')
            port = ssm_class_object.get_ssm_keys('/qa/vdl/test/' + env + '/aurora/PORT')
            conn = pymysql.connect(host, user=username, port=3306, passwd=password)
            # print("Connection established")
            return conn

        elif env == 'auditing':
            auditing_db_credentials = ssm_class_object.get_ssm_keys(
                '/' + environment + '/vdl/auditing/database-credentials')
            auditing_db_credentials = json.loads(auditing_db_credentials)
            host = auditing_db_credentials['host']
            username = auditing_db_credentials['username']
            password = auditing_db_credentials['password']
            port = auditing_db_credentials['port']
            conn = pymysql.connect(host, user=username, port=port, passwd=password)
            # print("Connection established")
            return conn

    def execute_query(self, query, env):
        try:
            # creating the method object to getting connection string based on the received env param
            connection_string = self.build_connection(env)
            # print("Executing query on Aurora:\n  ",query)
            # executing query using pandas read_sql() function
            execute_query = pd.read_sql((query), con=connection_string)

            # handling null values with same paten on multiple data frames
            df_with_null_handling = execute_query.where(execute_query.notnull(), "")
            # handling values after decimal
            df_with_fixed_decimal = df_with_null_handling.round(decimals=3)
            # this function change the dtype of df to object
            query_results_in_object_type = df_with_fixed_decimal.astype('object')
            # below statement remove the headers from data frame
            query_results_in_object_type.columns = range(query_results_in_object_type.shape[1])
            # print(query_results_in_object_type)
            return query_results_in_object_type

        except Exception as e:
            print("There is some problem in the query or the content does not exist in the schema")
            print(e)

ssm_class_object = get_ssm_params()
auditing_db_credentials = ssm_class_object.get_ssm_keys('/datalake/vdl/auditing/database-credentials')
auditing_db_credentials = json.loads(auditing_db_credentials)
host =auditing_db_credentials['password']
print(host)