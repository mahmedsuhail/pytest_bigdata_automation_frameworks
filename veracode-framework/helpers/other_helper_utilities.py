"""
----------------------------------------------------------------------------------------------------------
Description:

usage: ohter helper functions

Author  : Usman Zahid
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Usman Zahid        VDL-247              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

from helpers.db_helper import *
import json

# this class helps to read json contents and return the elements in the list form
class get_json_contents:
    def read_json(self,config_param):
        json_file=self.json_file_path()
        # with open(json_file) as f:
        #     data = json.load(f)
        param_list = []
        # json_content = json.dumps(json_file)
        # print(type(json_content))
        json_1 = json.loads(json_file)
        for value in json_1[config_param].values():
            for i in value.values():
                param_list.append(i)
        return param_list

    def json_file_path(self):
        s3 = boto3.resource('s3')
        content_object = s3.Object('veracode-qa-testing-artifacts', 'automation_config/StructuredConfig.json')
        file_content = content_object.get()['Body'].read().decode('utf-8')
        return file_content