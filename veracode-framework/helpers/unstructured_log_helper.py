"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Unstructured Helper Methods

Author  : Adil Qayyum
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Adil Qayyum        VDL-247              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""


import os
from helpers.athena_helper import *
import json

S3_obj = S3()


def get_ac_log_data(filepath):
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, filepath)
    with open(filename, "r") as f:
        data = f.read()
    return(data)


def get_ac_log_keys(data):
    ac_keys = []
    for key in data.keys():
        ac_keys.append(key)

    return ac_keys


def get_ac_source_params(bucket, file_name, log_type, *params):
    try:
        ac_data = S3_obj.read_s3_file_logs(bucket, file_name, log_type)
        d = "}"
        ac_data = [e + d for e in ac_data[0].split(d) if e]
        ac_data = json.loads(json.dumps(ac_data))
        final_content = json.loads(ac_data[0])

        columns = {}
        for name in params:
            for content in name:
                columns[content] = final_content[content]
        return columns
    except Exception as e:
        print('File not found in the bucket')


def get_scan_source_params(bucket, file_name, log_type,  *params):
    try:
        content_object = S3_obj.read_s3_file_logs(bucket, file_name, log_type)
        content_object = str(content_object)
        content_object = content_object.replace("', '", '')
        content_object = content_object.replace("['", '')
        content_object = content_object.replace("']", '')
        content_object = json.loads(content_object)
        final_content = content_object[0]

        columns = {}
        for name in params:
            for content in name:
                columns[content] = final_content[content]
        print(columns)
        return columns
    except Exception as e:
        print('File not found in the bucket')


def get_flaw_source_params(bucket, file_name, *params):
    try:
        flaw_data = S3_obj.read_s3_file(bucket, file_name)
        flaw_data = json.loads(json.dumps(flaw_data))
        final_content = json.loads(flaw_data[0])

        columns = {}
        for name in params:
            for content in name:
                columns[content] = final_content[content]
        return columns
    except:
        print('File not found in the bucket')
