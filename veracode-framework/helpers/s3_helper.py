"""
----------------------------------------------------------------------------------------------------------
Description:

usage: S3 Helper Methods

Author  : Adil Qayyum
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Adil Qayyum        VDL-247              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import boto3, allure


class S3:
    s3 = boto3.resource('s3')
    s3_client = boto3.client('s3')

    def upload_s3_file(self, file_name, bucket, file_path):
        try:
            self.s3.meta.client.upload_file(file_name, bucket, file_path)
            print('Upload Success')
        except:
            print('Invalid file for S3')

    @allure.step("Getting all objects from a bucket")
    def get_s3_objects(self, bucket, value='Null', exclusion='N'):
        object_count = 0
        try:
            bucket_obj = self.s3.Bucket(bucket)
            obj_list = []
            for obj in bucket_obj.objects.all():
                if exclusion == 'Y':
                    if value not in obj.key:
                        object_count += 1
                        obj_list.append(obj.key)
                else:
                    object_count += 1
                    obj_list.append(obj.key)
            return obj_list, object_count
        except Exception:
            print('Invalid bucket name')

    @allure.step("Getting objects against key")
    def get_s3_object(self, bucket, s3_object, value='Null', exclusion='N'):
        object_count = 0
        try:
            bucket_obj = self.s3.Bucket(bucket)
            obj_list = []
            for obj in bucket_obj.objects.all():
                if s3_object in obj.key:
                    if exclusion == 'Y':
                        if value not in obj.key:
                            object_count += 1
                            obj_list.append(obj.key)
                    else:
                        object_count += 1
                        obj_list.append(obj.key)
            return obj_list, object_count
        except Exception:
            print('Invalid bucket name')

    def get_bucket_folders(self, bucket):
        object_count = 0
        try:
            bucket_obj = self.s3.Bucket(bucket)
            obj_list = []
            for obj in bucket_obj.objects.all():
                object_count += 1
                obj_list.append(obj.key)
            return obj_list, object_count
        except Exception as e:
            print(e)

    def list_folders(self, bucket, prefix="", delimiter='/'):
        response = self.s3_client.list_objects_v2(Bucket=bucket, Prefix=prefix, Delimiter=delimiter)
        #print("Full Response \n ", response)
        folders = [res['Prefix'] for res in response['CommonPrefixes']]
        return folders

    def read_s3_file(self, bucket, file_name):
        bucket_obj = self.s3.Bucket(bucket)
        try:
            for obj in bucket_obj.objects.all():
                key = obj.key
                if file_name in key:
                    body = obj.get()['Body'].read()
                    body = str(body, 'utf-8').strip('b''')
                    file_body = body.split('\n')
                    return(file_body)
        except Exception as e:
            print(e)

    def read_s3_file_logs(self, bucket, filename, log_type):
        try:
            bucket_obj = self.s3_client.list_objects(Bucket=bucket, Prefix='unstructured/'+log_type+'/')
            for key in bucket_obj['Contents']:
                if filename in key['Key']:
                    obj = self.s3.Object(bucket, key['Key'])
                    body = obj.get()['Body'].read()
                    body = str(body, 'utf-8').strip('b''')
                    file_body = body.split('\n')
                    return (file_body)
        except Exception as e:
            print(e)

    def delete_s3_file(self, bucket, file_name):
        bucket_obj = self.s3.Bucket(bucket)
        counter = 0
        response = 0
        for obj in bucket_obj.objects.all():
            key = obj.key
            if file_name in key:
                response = self.s3_client.delete_object(Bucket=bucket, Key=key)
                counter += 1
        if counter == 0:
            print('File not found')
        return response

    def get_all_s3_keys(self, bucket, exclusion, prefix='', suffix=''):
        """
        Generate objects in an S3 bucket.

        :param bucket: Name of the S3 bucket.
        :param prefix: Only fetch objects whose key starts with
            this prefix (optional).
        :param suffix: Only fetch objects whose keys end with
            this suffix (optional).
        """
        s3_client = boto3.client('s3')

        kwargs = {'Bucket': bucket}

        keys = []

        count = 0
        # If the prefix is a single string (not a tuple of strings), we can
        # do the filtering directly in the S3 API.
        if isinstance(prefix, str):
            kwargs['Prefix'] = prefix

        while True:

            # The S3 API response is a large blob of metadata.
            # 'Contents' contains information about the listed objects.
            resp = s3_client.list_objects_v2(**kwargs)
            if 'Contents' not in resp:
                return []
            for obj in resp['Contents']:
                key = obj['Key']
                storage_class = obj['StorageClass']
                if exclusion not in key:
                    if storage_class != 'GLACIER':
                        if key.startswith(prefix) and key.endswith(suffix):
                            count += 1
                            keys.append({key: storage_class})
            try:
                kwargs['ContinuationToken'] = resp['NextContinuationToken']
            except KeyError:
                break
        return keys, count

    def get_object(self, bucket, key):
        try:
            response = self.s3_client.get_object(Bucket=bucket, Key=key)
            return response['Body'].read()
        except Exception as exc:
            print(exc)
