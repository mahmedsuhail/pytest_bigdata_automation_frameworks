import findspark
findspark.init()
from pyspark.sql import SQLContext, functions
HUDI_FORMAT = 'org.apache.hudi'


class SparkHelper:
    def __init__(self, session=None):
        self.session = session
        self.sql_context = SQLContext(session)

    def load_hudi_data_frame(self, path):
        """
        Method to load the hudi data frame into session
        :param path: The path from where the data is being loaded
        :return: data frame
        """
        return self.session.read.format(HUDI_FORMAT).option("inferSchema", "true").load(path)

    def load_data_frame(self, path, format):
        """
        Method to load spark data frame into session
        :param path: The path from where the data is being loaded
        :param format: File format of the data i.e. csv/parquet
        :return: data frame
        """
        #return self.session.read.load( path, format=format)
        return self.session.read.format(format).load(path)

    def get_column_max_value(self, spark_df, column):
        """
        Method to extract the maximum value of a column data
        :param spark_df: The spark data frame
        :param column: The column name
        :return: The max value
        """
        return spark_df.agg({column: 'max'}).collect()[0][0]

    def get_column_max_length(self, spark_df, column):
        """
        Method to get the maximum length of a column
        :param spark_df: The spark data frame
        :param column: The column name
        :return: The max length
        """

        col_len_df = spark_df.select(functions.length(column))
        max_len = self.get_column_max_value(col_len_df, col_len_df.columns[0])
        if max_len:
            max_len = int(max_len)
        else:
            max_len = 0
        return max_len

    def get_column_null_count(self, spark_df, column):
        """
        Method to get the null count for a column i.e. number of null rows
        :param spark_df: The spark data frame
        :param column: The column name
        :return: Null Count
        """
        #return spark_df.where(functions.col(column).isNull()).count()
        return spark_df.select(column).where(functions.col(column).isNull()).count()

    def get_column_data_type(self, spark_df, column):
        """
        Method to get the data type of a column
        :param spark_df: The spark data frame
        :param column: The column name
        :return: Data type of the column
        """
        data_types = spark_df.dtypes
        for col, data_type in data_types:
            if col.lower() == column.lower():
                return data_type

    def drop_hudi_columns(self, spark_hudi_df):
        """
        Method to drop the extra columns that Hudi appends with the data
        :param spark_hudi_df: Hud Data Frame
        :return: Data Frame without Hudi Columns
        """

        hudi_columns = ['_hoodie_commit_time', '_hoodie_commit_seqno',
                        '_hoodie_record_key', '_hoodie_partition_path',
                        '_hoodie_file_name']
        return spark_hudi_df.drop(*hudi_columns)

    def execute_hive_query(self, query):
        """
        Method to execute a hive query
        :param query: Hive query
        """
        return self.session.sql(query)

    def write_dataframe(self, dataframe, path, format='parquet', mode='overwrite'):
        """
        Method to write a data frame to a file
        :param dataframe: The Spark Data frame
        :param path: The path where to write the files
        :param format: Format of the files
        :param mode: Mode of the write i.e. append or overwrite
        """
        dataframe.write.format(format).mode(mode).save(path)

    def write_dataframe_create_table(self, spark_df, path, database, table, format='parquet'):
        """
        Method to write a data frame and create a table alongside
        :param spark_df: Spark Data Frame
        :param path: The path where to write the data frame into files
        :param database: The database where to write the table
        :param table: Name of the table
        :param format: Format of the files
        """
        spark_df.write.format(format).option('path', path).saveAsTable(f'{database}.{table}')

    def is_table_cached(self, table):
        """
        Method to check whether a table is cached or not
        :param table: Name of the table
        :return: Table cached or not
        """
        table_cached = False
        if self.is_temp_table_exist(table):
           table_cached = self.session._jsparkSession.catalog().isCached(table)
        return table_cached

    def create_temp_table(self, spark_df, table):
        """
        Method to create a temp table
        :param spark_df: Spark data frame to write to the table
        :param table: Name of the table
        """
        #spark_df.registerTempTable(table)
        spark_df.createOrReplaceTempView(table)

    # Using SparkSQL
    def get_column_max_length(self, database, table, column):
        """
        Method to get the maximum length of a column
        :param database: Name of the database
        :param table: Name of the table
        :param column: Name of the column
        :return: Max length
        """
        if database:
            col_len_query = f'SELECT MAX(LENGTH({column})) FROM {database}.{table}'
        else:
            col_len_query = f'SELECT MAX(LENGTH({column})) FROM {table}'
        col_max_len = self.session.sql(col_len_query).collect()[0][0]
        if col_max_len:
            col_max_len = int(col_max_len)
        else:
            col_max_len = 0
        return col_max_len

    def get_column_null_count(self, database, table, column):
        """
        Method to get the null count of a column
        :param database: Name of the database
        :param table: Name of the table
        :param column: Name of the column
        :return: Null count
        """
        if database:
            null_col_query = f'SELECT COUNT({column}) FROM {database}.{table} WHERE {column} IS NULL'
        else:
            null_col_query = f'SELECT COUNT({column}) FROM {table} WHERE {column} IS NULL'
        null_count = self.session.sql(null_col_query).collect()[0][0]
        return null_count

    def get_column_data_type(self, database, table, column):
        """
        Method to get the data type of a column
        :param database: Name of the database
        :param table: Name of the table
        :param column: Name of the column
        :return: Data Type
        """
        if database:
            data_type_query = f'DESCRIBE {database}.{table} {column}'
        else:
            data_type_query = f'DESCRIBE {table} {column}'
        col_def_df = self.session.sql(data_type_query)
        col_data_type = col_def_df.select('info_value').where("info_name = 'data_type'").collect()[0][0]
        return col_data_type

    # Using SQL Context
    def delete_temp_table(self, table):
        """
        Method to delete the temp table
        :param table: Name of the table
        """
        self.sql_context.dropTempTable(table)


    def cache_table(self, table):
        """
        Method to cache a table
        :param table: Name of the table
        """
        self.sql_context.cacheTable(table)

    def uncache_table(self, table):
        """
        Method to uncache a table
        :param table: Name of the table
        """
        self.sql_context.uncacheTable(table)

    def clear_cache(self):
        """
        Method to clear the cache
        """
        self.sql_context.clearCache()

    def is_temp_table_exist(self, table):
        """
        Method to check whether a temp table exist or not
        :param table: Name of the table
        :return: table
        """
        tables = self.sql_context.tableNames()
        tables = set([table.lower() for table in tables])
        return table.lower() in tables
