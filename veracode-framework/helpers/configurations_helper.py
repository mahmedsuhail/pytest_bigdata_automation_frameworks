"""
----------------------------------------------------------------------------------------------------------
Description: This script is for the extraction of all the required parameters being used throughout the test cases

usage: Configurations & Variables

Author  : Adil Qayyum
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Adil Qayyum        VDL-247              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""


from helpers.ssm_helper import *
from managers.glue_architecture_based_managers.config_manager import *

# env = os.environ["ENV_NAME"]
# ENVIRONMENT
env = 'qa'

# GET SSM PARAMS
ssm_class_object = get_ssm_params()
host_name = ssm_class_object.get_ssm_keys("/qa/vdl/test/source/aurora/Host")
user = ssm_class_object.get_ssm_keys("/qa/vdl/test/source/aurora/USER")
password = ssm_class_object.get_ssm_keys("/qa/vdl/test/source/aurora/PASSWORD")

# ARNS
sns_topic_arn = 'arn:aws:sns:us-east-1:833309876439:Veracode-DL-TestCase-Notification'

# BUCKETS
bucket_client_ac = get_bucket_name('client_ac')
bucket_client_scan = get_bucket_name('client_scan')
bucket_s3sync_ac = get_bucket_name('s3sync_ac')
bucket_s3sync_scan = get_bucket_name('s3sync_scan')
bucket_landing = get_bucket_name('dev_landing')
bucket_raw = get_bucket_name('dev_raw')

# KEYS
key_value_ac = get_keys('ac_key')
key_value_scan = get_keys('scan_key')

# EXCLUSIONS
exclusion_value_ac = get_exclusions('ac')
exclusion_value_scan = get_exclusions('scan')

# FILE NAMES
ac_file_name = get_keys('ac_file_name')
scan_file_name = get_keys('scan_file_name')
flaw_file_name = get_keys('flaw_file_name')

# DATABASE & TABLES
ac_database_raw = get_databases('ac_db_raw')
ac_database_curated = get_databases('ac_db_curated')
ac_table = get_databases('ac_table')
scan_database_landing = get_databases('scan_db_landing')
scan_database_raw = get_databases('scan_db_raw')
scan_database_curated = get_databases('scan_db_curation')
scan_table = get_databases('scan_table')
flaw_database_landing = get_databases('flaw_db_landing')
flaw_database_raw = get_databases('flaw_db_raw')
flaw_database_curated = get_databases('flaw_db_curated')
flaw_table = get_databases('flaw_table')

# QUERY DATA UNSTRUCTURED
ac_columns_select = get_select_columns('ac_columns')
ac_columns_where = get_where_columns('ac_columns')
scan_columns_select = get_select_columns('scan_columns')
scan_columns_where = get_where_columns('scan_columns')
flaw_columns_select = get_select_columns('flaw_columns')
flaw_columns_where = get_where_columns('flaw_columns')

ac_raw = get_unstructured_query('ac_columns', 'ac_raw')
ac_curated = get_unstructured_query('ac_columns', 'ac_curated')
ac_create_view = get_unstructured_query('ac_columns', 'ac_create_view')
ac_drop_view = get_unstructured_query('ac_columns', 'ac_drop_view')

scan_raw = get_unstructured_query('scan_columns', 'scan_raw')
scan_curated = get_unstructured_query('scan_columns', 'scan_curated')
scan_create_view = get_unstructured_query('scan_columns', 'scan_create_view')
scan_drop_view = get_unstructured_query('scan_columns', 'scan_drop_view')

flaw_raw = get_unstructured_query('flaw_columns', 'flaw_raw')
flaw_curated = get_unstructured_query('flaw_columns', 'flaw_curated')
flaw_create_view = get_unstructured_query('flaw_columns', 'flaw_create_view')
flaw_drop_view = get_unstructured_query('flaw_columns','flaw_drop_view')

# QUERY DATA STRUCTURED CURATION
# RAW TABLES
organization_query_raw = get_structured_raw_data('identity')
organization_query_raw = organization_query_raw['td_organization']['query']

cwe_query_raw = get_structured_raw_data('finding')
cwe_query_raw = cwe_query_raw['td_cwe']['query']

user_query_raw = get_structured_raw_data('identity')
user_query_raw = user_query_raw['td_user']['query']

user_proxy_query_raw = get_structured_raw_data('identity')
user_proxy_query_raw = user_proxy_query_raw['td_user_proxy_org']['query']

app_team_query_raw = get_structured_raw_data('application')
app_team_query_raw = app_team_query_raw['td_app_team']['query']

app_tag_query_raw = get_structured_raw_data('application')
app_tag_query_raw = app_tag_query_raw['td_app_tag']['query']

app_query_raw = get_structured_raw_data('application')
app_query_raw = app_query_raw['td_app']['query']

cve_query_raw = get_structured_raw_data('finding')
cve_query_raw = cve_query_raw['td_cve']['query']

# DIMENSION TABLES
dim_account_query_curated = get_structured_curated_data('dim')
dim_account_query_curated = dim_account_query_curated['td_account']['query']

dim_cwe_query_curated = get_structured_curated_data('dim')
dim_cwe_query_curated = dim_cwe_query_curated['td_cwe']['query']

dim_app_query_curated = get_structured_curated_data('dim')
dim_app_query_curated = dim_app_query_curated['td_app']['query']

dim_user_query_curated= get_structured_curated_data('dim')
dim_user_query_curated = dim_user_query_curated['td_user']['query']

dim_proxy_query_curated = get_structured_curated_data('dim')
dim_proxy_query_curated = dim_proxy_query_curated['td_proxy']['query']

dim_app_team_curated = get_structured_curated_data('dim')
dim_app_team_curated = dim_app_team_curated['td_app_team']['query']

dim_app_tag_curated = get_structured_curated_data('dim')
dim_app_tag_curated = dim_app_tag_curated['td_app_tag']['query']

dim_cve_query_curated = get_structured_curated_data('dim')
dim_cve_query_curated = dim_cve_query_curated['td_cve']['query']