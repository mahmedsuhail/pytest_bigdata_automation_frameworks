"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Count Level Validation

Author  : Adil Qayyum, Usman Zahid
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Adil Qayyum        VDL-247              Initial draft.
03/1/2019        Usman Zahid        VDL-247              Initial draft.
-----------------------------------------------------------------------------------------------------------
"""


import io
from helpers.s3_helper import *
import time
from concurrent.futures import thread
from helpers.other_helper_utilities import *


s3 = boto3.resource('s3')
s3client = boto3.client('s3')
athena_client = boto3.client('athena')
S3_obj = S3()


class athena_helper:

    def query_builder(self, select_columns, where_columns, database, table, results):
        base_query = 'SELECT '
        last_item = 0

        for item in select_columns:
            base_query += item + ', '
            last_item = item
        base_query = base_query.replace(last_item+", ", last_item)

        base_query += ' FROM "' + database + '"."' + table

        base_query+= '" WHERE '

        try:
            for item in where_columns:
                base_query += item+ ' = '+repr(results[item])+' and '
                last_item = item+ ' = '+repr(results[item])
            base_query = base_query.replace(last_item + " and ", last_item)
            print(base_query)
            keys = results.keys()
            target_message = self.exec_query_athena(base_query, database)

            return target_message, keys, base_query
        except:
            print('Results not retrieved from the file')

    def exec_query_athena(self, query, database):
        response = athena_client.start_query_execution(
            QueryString=query,
            QueryExecutionContext={
                'Database': database
            },
            ResultConfiguration={
                'OutputLocation': 's3://veracode-qa-testing-artifacts/athena-results/ac/'
            }
        )

        execution_id = response['QueryExecutionId']
        result = athena_client.get_query_execution(QueryExecutionId = execution_id)
        output_key = (result['QueryExecution']['ResultConfiguration']['OutputLocation']).split('s3://veracode-qa-testing-artifacts/')

        while result['QueryExecution']['Status']['State'] == 'RUNNING':
            result = athena_client.get_query_execution(QueryExecutionId=execution_id)


        try:
            obj = s3client.get_object(Bucket='veracode-qa-testing-artifacts', Key=output_key[1])
            df = pd.read_csv(io.BytesIO(obj['Body'].read()))


            # File cleansing on S3
            S3_obj.s3_client.delete_object(Bucket='veracode-qa-testing-artifacts', Key=output_key[1])
            S3_obj.s3_client.delete_object(Bucket='veracode-qa-testing-artifacts', Key=output_key[1]+'.metadata')

        except Exception as e:
            df = pd.DataFrame()
            df = df.fillna(0)

        return df

    def compare_results(self, target_message, keys, base_query, select_columns, table, results):
        status = True
        if target_message.empty:
            print("\nNo data available for the query: " + base_query)
            return False

        column_name = list(target_message)

        target_result = {}
        for item in column_name:
            target_result[item] = target_message[item][0]

        for key in keys:
            if key in select_columns:
                if target_result[key] == results[key]:
                    print(
                        "\nData matched for table: " + table + " where column: " + key + " for the value: " + results[key])
                else:
                    print("\nData not matched for table: " + table + " where column: " + key + " for the value: " + results[
                        key])
                    status = False
        return status

    def build_athena_connection(self, query, s3_output):
        client = boto3.client('athena',region_name='us-east-1')
        self.response = client.start_query_execution(
            QueryString=query,
            ResultConfiguration={
                'OutputLocation': s3_output,
                }
            )
        self.response_id=self.response['QueryExecutionId']
        self.filepath = self.s3_ouput + '/' + self.response_id+'.csv'
        return self.response

    def execute_query(self,query, ddl=False):
        # config params are being fetched from config.json through json_parser.py
        json_config_obj = get_json_contents()
        config_list = json_config_obj.read_json("config")

        for i in range(0, len(config_list), 7):
            self.s3_ouput = config_list[i]
            s3bucket=config_list[i+1]
            prefix_folder = config_list[i+2]

            # Param definitions
            query = query
            sufix=".csv"
            # Execute all queries
            queries = [query]
            for q in queries:
                print("Executing query on Athena: %s" % (q))
                self.build_athena_connection(q, self.s3_ouput)
                # Concatenating the query ID with file suffix
                query_result_path = prefix_folder + self.response_id + sufix
                # i have added a code to get query execution status before reading the result file from s3
                client = boto3.client('athena')
                query_status = None
                while query_status == 'QUEUED' or query_status == 'RUNNING' or query_status is None:
                    query_status = \
                        client.get_query_execution(QueryExecutionId=self.response_id)['QueryExecution'][
                            'Status']['State']
                    # print(query_status + " " + endpoint)
                    if query_status == 'FAILED' or query_status == 'CANCELLED':
                        print(
                            'Athena query with the string"',q, '"failed or was cancelled')
                    time.sleep(3)

                # This service is used yo read data from CSV and load it to data frame
                # time.sleep(20)
                if ddl==True:
                    # print("Table is successfully created")
                    return True
                else:
                    s3client = boto3.client('s3')
                    obj = s3client.get_object(Bucket=s3bucket, Key=query_result_path)
                    # pandas function is used to read the data from s3 bucket against query ID
                    df = pd.read_csv(io.BytesIO(obj['Body'].read()))
                    df_with_null_handling = df.where(df.notnull(), "")
                    athena_results=df_with_null_handling.round(decimals=3)
                    query_results_in_object_type = athena_results.astype('object')
                    # below statement remove the headers from dataframe
                    query_results_in_object_type.columns = range(query_results_in_object_type.shape[1])
                    # print(query_results_in_object_type)
                    return query_results_in_object_type

    def create_database(self, database_name):
        return athena_client.create_database(DatabaseInput={'Name': database_name})

    def run_query(self, sql, output_path='s3://veracode-qa-testing-artifacts/athena-results/schema-evolution/'):
        response = athena_client.start_query_execution(QueryString=sql,
            ResultConfiguration={'OutputLocation': output_path})
        execution_id = response['QueryExecutionId']
        result = athena_client.get_query_execution(QueryExecutionId=execution_id)
        output_key = (result['QueryExecution']['ResultConfiguration']['OutputLocation']).split('s3://veracode-qa-testing-artifacts/')

        while result['QueryExecution']['Status']['State'] == 'RUNNING':
            result = athena_client.get_query_execution(QueryExecutionId=execution_id)
            # time.sleep(10)

        try:
            time.sleep(10)
            obj = s3client.get_object(Bucket='veracode-qa-testing-artifacts', Key=output_key[1])
            df = pd.read_csv(io.BytesIO(obj['Body'].read()))

            # File cleansing on S3
            S3_obj.s3_client.delete_object(Bucket='veracode-qa-testing-artifacts', Key=output_key[1])
            S3_obj.s3_client.delete_object(Bucket='veracode-qa-testing-artifacts', Key=output_key[1] + '.metadata')

        except Exception as e:
            df = pd.DataFrame()
            df = df.fillna(0)
            print(e)

        return df
        # return response

    def delete_database(self, name, output_path):
        sql = 'DROP DATABASE IF EXISTS {name};'
        return self.run_query(sql, output_path)

    def create_database(self, name, output_path='s3://veracode-qa-testing-artifacts/athena-results/schema-evolution/'):
        self.delete_database(name, output_path)
        sql = 'CREATE DATABASE {name};'
        return self.run_query(sql, output_path)