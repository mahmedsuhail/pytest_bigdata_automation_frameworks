"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Glue Helper Methods

Author  : Adil Qayyum
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Adil Qayyum        VDL-247              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import boto3


class Glue:
    glue_client = boto3.client('glue')

    def get_glue_table_column_names(self, database, table):
        try:
            table_data = self.glue_client.get_table(DatabaseName=database, Name=table)
            columns = table_data['Table']['StorageDescriptor']['Columns']
            column_details = {}
            for idx, i in enumerate(columns):
                column_details[(columns[idx]['Name'])] = (columns[idx]['Type'])
            return column_details
        except Exception as e:
            print(e)

    def get_glue_table_names(self, database):
        try:
            table_list = self.glue_client.get_tables(DatabaseName=database)
            table_names = []
            for idx, i in enumerate(table_list['TableList']):
                table_names.append(table_list['TableList'][idx]['Name'])
                return (table_names)
        except Exception as e:
            print(e)

    def delete_glue_table(self, database, table):
        self.glue_client.delete_table(DatabaseName=database, Name=table)
