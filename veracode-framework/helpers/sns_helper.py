import boto3
from helpers.configurations_helper import *


sns_client = boto3.client('sns')
subject = 'VDL - Test Case Failure Notification'


def publish_notification(message):
    response = sns_client.publish(TopicArn=sns_topic_arn, Subject=subject, Message=message)
    return response

