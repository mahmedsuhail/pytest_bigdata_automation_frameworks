import allure
from pyspark.sql import SparkSession
from pyspark import SparkConf
#
# class SparkSession:
#     def __init__(self):
#         pass

def get_spark_session( app_name='hudi_app'):
    # type = spark / hudi
    # if type == 'hudi':
    hudi_jars = '/usr/lib/spark/jars/httpclient-4.5.9.jar,/usr/lib/hudi/hudi-spark-bundle.jar,/usr/lib/spark/external/lib/spark-avro.jar'
    conf = SparkConf() \
        .set('spark.serializer', 'org.apache.spark.serializer.KryoSerializer') \
        .set('spark.sql.hive.convertMetastoreParquet', 'false') \
        .set('spark.jars', hudi_jars)\
        .set('spark.port.maxRetries', '40')
    # elif type == 'spark':
    # conf = SparkConf()
    return SparkSession.builder.appName(app_name) \
        .config(conf=conf).enableHiveSupport().getOrCreate()
