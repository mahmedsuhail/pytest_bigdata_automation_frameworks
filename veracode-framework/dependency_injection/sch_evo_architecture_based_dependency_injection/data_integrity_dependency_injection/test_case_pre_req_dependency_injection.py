"""
----------------------------------------------------------------------------------------------------------
Description: Pre-req to proceed test cases will be handled

usage: Count Level Validation

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-587

Modification Log:

-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/01/2020        Zeeshan Mirza        VDL-587             Initial draft.

-----------------------------------------------------------------------------------------------------------
"""
import pytest
import allure

from managers.common_managers.s3_manager import s3Manager
from managers.sch_evo_architecture_base_manager.data_integrity_manager.data_integrity_manager_impl.spark_column_manager_impl import \
    SparkColumnValidationManager
from managers.sch_evo_architecture_base_manager.json_managers.json_managers_impl.json_catalog_manager import \
    JsonCatalogManager
from managers.common_managers.athena_manager import AthenaManager
from managers.sch_evo_architecture_base_manager.json_managers.json_managers_impl.json_hop_manager import JsonHopManager
from managers.sch_evo_architecture_base_manager.json_managers.json_managers_impl.json_data_type_manager import \
    JsonDataTypeManager

pytest.is_cataloged = False


def __get_table_ddl_from_config(table_name, jsonCatalogManager):
    return jsonCatalogManager.get_columns_type_and_length_by_table(table_name)


def __get_target_db_by_hop(hop, jsonCatalogManager):
    return jsonCatalogManager.get_target_db_by_source_database(hop)


def __get_source_db_by_hop(hop, jsonCatalogManager):
    return jsonCatalogManager.get_target_db_by_source_database(hop)


def __create_athena_target_database(athenaManager, hop, jsonCatalogManager):
    target_db = __get_target_db_by_hop(hop, jsonCatalogManager)
    return athenaManager.create_database(target_db)


def __create_athena_source_database(athenaManager, hop, jsonCatalogManager):
    source_db = __get_source_db_by_hop(hop, jsonCatalogManager)
    return athenaManager.create_database(source_db)


def __get_source_etl_table_s3_path(hop):
    jsonHopManager = JsonHopManager()
    return jsonHopManager.get_hop_source_path(hop)


def __get_target_etl_table_s3_path(hop):
    jsonHopManager = JsonHopManager()
    return jsonHopManager.get_hop_target_path(hop)


def __get_postfix(precision, scale):
    postfix = ""
    if precision and scale:
        postfix = f'({precision},{scale})'
    elif precision:
        postfix = f'({precision})'
    return postfix


def __get_athena_columns(jsonDataTypeManager, columns, source_type='redshift'):
    for column in columns.keys():
        data_type = columns[column]['datatype'].split('(')[0]
        athena_data_type = jsonDataTypeManager.get_source_column_mapping(source_type, 'athena', data_type)
        postfix = __get_postfix(columns[column]['precision'], columns[column]['scale'])
        columns[column]['datatype'] = athena_data_type[0] + postfix
    return columns


def __create_glue_catalog_for_target_tables(etl_ts, etl_tables, hop, athenaManager, jsonCatalogManager):
    target_database = __get_target_db_by_hop(hop, jsonCatalogManager)
    file_type = "parquet"

    file_path = __get_target_etl_table_s3_path(hop)

    jsonDataTypeManager = JsonDataTypeManager()

    for table in etl_tables:
        splited_list = table.split('/')
        resultant_table = __get_table_name(table)
        print("resultant_table :: ", resultant_table)
        table_ddl = __get_table_ddl_from_config(resultant_table, jsonCatalogManager)

        table_ddl = __get_athena_columns(jsonDataTypeManager, table_ddl)

        location = file_path.format(etl_timestamp=etl_ts,
                                    table_name=resultant_table.lower())
        print("Location Variable :: ", location)
        response = athenaManager.create_table(target_database, hop + "_" + resultant_table + "_" + etl_ts, table_ddl,
                                              location, file_type)

        if response['status'] == 'SUCCEEDED':
            pass
        else:
            print("%s catalog is failed with reason %s ", table, response['reason'])
            exit(0)


def __get_table_name(table):
    return table.split('/')[-2].upper()


def __create_glue_catalog_for_source_tables(etl_ts, etl_tables, hop, athenaManager, jsonCatalogManager):
    __create_athena_source_database(athenaManager, hop, jsonCatalogManager)
    source_database = __get_source_db_by_hop(hop, jsonCatalogManager)

    file_type = "parquet"
    if hop == "staging":
        file_type = "csv"

    file_path = __get_source_etl_table_s3_path(hop)
    jsonDataTypeManager = JsonDataTypeManager()

    for table in etl_tables:
        source_table = __get_table_name(table)
        print("source_table :: ", source_table)
        table_ddl = __get_table_ddl_from_config(source_table, jsonCatalogManager)

        table_ddl = __get_athena_columns(jsonDataTypeManager, table_ddl)
        location = file_path.format(etl_timestamp=etl_ts, table_name=source_table)
        print("Location Variable :: ", location)
        response = athenaManager.create_table(source_database, "source_" + source_table + "_" + etl_ts, table_ddl,
                                              location, file_type)

        if response['status'] == 'SUCCEEDED':
            pass
        else:
            print("%s catalog is failed with reason %s ", table, response['reason'])
            exit(0)


@allure.step("Get source database by hop:{hop}")
def source_db(hop, source, database):
    jsonCatalogManager = JsonCatalogManager(source, database)
    return __get_source_db_by_hop(hop, jsonCatalogManager)


@allure.step("Get target database by hop:{hop}")
def resultant_db(hop, source, database):
    jsonCatalogManager = JsonCatalogManager(source, database)
    return __get_target_db_by_hop(hop, jsonCatalogManager)


def __get_etl_tables(etl_ts):
    s3_manager = s3Manager()
    etl_table_list = s3_manager.get_list_of_tables_by_etl_timestamp(etl_ts)
    return etl_table_list


@allure.step("Create Catalog for {source} and {database}")
def catalog_created(etl_ts, source, database, hop):
    athenaManager = AthenaManager()
    jsonCatalogManager = JsonCatalogManager(source, database)

    etl_tables = __get_etl_tables(etl_ts)

    __create_glue_catalog_for_source_tables(etl_ts, etl_tables, hop, athenaManager, jsonCatalogManager)
    __create_glue_catalog_for_target_tables(etl_ts, etl_tables, hop, athenaManager, jsonCatalogManager)


########################################################################################################################
################################### - PySpark-Hudi - ###################################################################
########################################################################################################################

def __create_hudi_catalog_for_target_tables(etl_ts, etl_tables, hop, sparkManager, jsonCatalogManager):
    file_type = "parquet"
    target_database = __get_target_db_by_hop(hop, jsonCatalogManager)
    print(f'Target DB: {target_database}')

    sparkManager.create_hive_database(target_database)

    jsonHopManager = JsonHopManager()

    for table in etl_tables:
        splited_list = table.split('/')
        resultant_table = splited_list[len(splited_list) - 2]
        print("resultant_table :: ", resultant_table)
        target_path = jsonHopManager.get_hop_temp_path(hop).format(etl_timestamp=etl_ts, hop=hop,
                                                                   table_name=resultant_table)
        src_path = jsonHopManager.get_hop_temp_path(hop).format(etl_timestamp=etl_ts, hop=hop,
                                                                table_name=resultant_table + '_temp')

        sparkManager.create_hive_table(target_database, resultant_table, src_path, target_path)


@allure.step("Create Hudi Catalog for {source} and {database}")
def hudi_catalog_created(etl_ts, source, database, hop, spark_session):
    jsonCatalogManager = JsonCatalogManager(source, database)

    sparkManager = SparkColumnValidationManager(spark_session)

    etl_tables = __get_etl_tables(etl_ts)

    __create_hudi_catalog_for_target_tables(etl_ts, etl_tables, hop, sparkManager, jsonCatalogManager)


@allure.step("Create and cache Temp Table")
def create_and_cache_temp_table(hop, modified_table_name, table_name, spark_session):
    jsonHopManager = JsonHopManager()
    sparkManager = SparkColumnValidationManager(spark_session)

    path = jsonHopManager.get_hop_target_path(hop)
    path = path.format(table_name=table_name.lower())
    sparkManager.create_cache_temp_table(modified_table_name, path)
