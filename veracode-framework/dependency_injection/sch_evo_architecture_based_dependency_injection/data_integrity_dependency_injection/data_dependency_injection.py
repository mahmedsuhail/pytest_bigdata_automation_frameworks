"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Not null column Validation

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-587

Modification Log:

-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/14/2020        Zeeshan Mirza        VDL-587             Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

from managers.common_managers.s3_manager import s3Manager

def etl_tables(etl_ts):
    s3_manager = s3Manager()
    print("etl_ts :: ", etl_ts)
    etl_table_list = s3_manager.get_list_of_tables_by_etl_timestamp(etl_ts)
    etl_table_list = [table.split('/')[-2] for table in etl_table_list]
    return etl_table_list

#print(etl_tables(202001030900))