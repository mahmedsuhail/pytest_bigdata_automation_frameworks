import findspark
findspark.init()

import argparse
from pyspark.sql import SparkSession
from pyspark import SparkConf

from managers.sch_evo_architecture_base_manager.json_managers.json_managers_impl.json_hop_manager import JsonHopManager
from managers.common_managers.s3_manager import s3Manager
from helpers.spark_helper import SparkHelper


def get_hudi_session(app_name='hudi_app'):
    conf = SparkConf()\
        .set('spark.serializer', 'org.apache.spark.serializer.KryoSerializer') \
        .set('spark.sql.hive.convertMetastoreParquet', 'false') \
        .set('spark.jars', '/usr/lib/hudi/hudi-spark-bundle.jar,/usr/lib/spark/external/lib/spark-avro.jar')
    return SparkSession.builder.appName(app_name)\
        .config(conf=conf).enableHiveSupport().getOrCreate()


def get_net_change(spark_helper, path):
    hudi_df = spark_helper.load_hudi_data_frame(path)
    remove_cols = ['_hoodie_commit_time', '_hoodie_commit_seqno',
                   '_hoodie_record_key', '_hoodie_partition_path',
                   '_hoodie_file_name']
    # using this approach in case order of these columns change
    select_cols = hudi_df.columns
    for col in remove_cols:
        select_cols.remove(col)
    # other option
    # select_cols = hudi_df.columns[5:]
    commit_time_col = '_hoodie_commit_time'
    max_value = spark_helper.get_column_max_value(hudi_df, commit_time_col)
    return hudi_df.select(select_cols).where(f'{commit_time_col} = {max_value}')


def write_net_change(spark_helper, source_path, target_path):
    nc_df = get_net_change(spark_helper, source_path)
    spark_helper.write_dataframe(nc_df, target_path, 'parquet')


def write_tables_net_change(hop, etl_ts):
    json_hop_manager = JsonHopManager()
    s3_manager = s3Manager()
    hudi_session = get_hudi_session()
    spark_helper = SparkHelper(hudi_session)

    tables = s3_manager.get_list_of_tables_by_etl_timestamp(etl_ts)
    snapshot_path = json_hop_manager.get_hop_target_path(hop)
    temp_path = json_hop_manager.get_hop_temp_path(hop)

    for table in tables:
        table_name = table.split('/')[-2]
        print(f'Table name: {table_name}')
        source_path = snapshot_path.format(table_name=table_name)
        target_path = temp_path.format(etl_timestamp=etl_ts, hop=hop, table_name=table_name+'_temp')
        print(f'Source Path: {source_path}')
        print(f'Target Path: {target_path}')
        write_net_change(spark_helper, source_path, target_path)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--hop')
    parser.add_argument('--etl_ts')
    args = parser.parse_args()
    hop = args.hop
    etl_ts = args.etl_ts
    #hop = 'snapshot'
    #etl_ts = '2020-01-02_15:03:32.143384'
    write_tables_net_change(hop, etl_ts)


if __name__ == "__main__":
    main()
