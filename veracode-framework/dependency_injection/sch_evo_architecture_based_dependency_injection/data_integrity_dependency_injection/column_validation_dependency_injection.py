"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Column level dependency injections to validate data

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-587

Modification Log:

-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/14/2020        Zeeshan Mirza        VDL-587             Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

from managers.sch_evo_architecture_base_manager.json_managers.json_managers_impl.json_catalog_manager import JsonCatalogManager

'''
    Start - Not null column dependency injections
'''


def __parse_dictionary(dictionary):
    keys = dictionary.keys()
    final_list = []
    for key in keys:
        initial_list = dictionary[key]
        for value in initial_list:
            temp = key+"."+value
            final_list.append(temp)
    return final_list


def __not_null_columns_list_for_given_tables(source, database, tables):
    catalogManager = JsonCatalogManager(source, database)
    not_null_columns = catalogManager.get_not_null_columns_for_given_tables(tables)
    return not_null_columns


def not_null_column(source, database, tables):
    result = __not_null_columns_list_for_given_tables(source, database, tables)
    print(f'Tables: {tables}')
    print(f'Result: {result}')
    return result


'''
    End - Not null column dependency injections
'''

########################################################################################################################

'''
    Start - Column length validation dependency injections
'''


def __get_columns_with_length(source, database, tables):
    catalogManager = JsonCatalogManager(source, database)
    return catalogManager.get_column_name_and_length_for_given_tables(tables)


def column_length(source, database, tables):
    return __get_columns_with_length(source, database, tables)


def __get_columns(source, database, tables):
    catalogManager = JsonCatalogManager(source, database)
    return catalogManager.get_column_name_for_given_tables(tables)


'''
    End - Column length validation dependency injections
'''

########################################################################################################################

'''
    Start - Column data type validation dependency injections
'''


def __get_resultant_db(source, database, hop):
    catalogManager = JsonCatalogManager(source, database)
    return catalogManager.get_target_db_by_source_database(hop)


def __get_columns_data_type_for_all_tables(hop, source, database, etl_tables):
    resultant_db = __get_resultant_db(source, database, hop)
    catalogManager = JsonCatalogManager(source, database)
    return catalogManager.get_column_data_type_list_by_databse_and_table_names_list(resultant_db, etl_tables)


def __get_column_data_type(source, database, tables):
    catalogManager = JsonCatalogManager(source, database)
    return catalogManager.get_column_name_and_datatype_for_given_tables(tables)


def columns_data_type(source, database, tables):
    return __get_column_data_type(source, database, tables)


def resultant_db(source, database, hop):
    return __get_resultant_db(source, database, hop)


def column_name(source, database, tables):
    return __get_columns(source, database, tables)


'''
    End - Column data type validation dependency injections
'''
