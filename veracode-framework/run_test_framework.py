import os
import sys
from check_test_case_status import get_test_cases_status
import boto3

def send_notificaiton(_status, response = None):
    detail = ""
    if response:
        detail = f"Total Tests: {response['total']}, Tests Failed: {response['failed']}, Tests Broken: {response['broken']}."
    message = f'Tests {_status} for the {_hop} hop.\n{detail}\nThe report link is: https://{_bucket}.s3.amazonaws.com/{_hop}/{_etl_ts}/index.html'
    client = boto3.client("sns")
    response = client.publish(
        TopicArn='arn:aws:sns:us-east-1:833309876439:veracode-dl-nbs-job-failed',
        Message=message,
        Subject=f'Tests {_status} - {_hop}'
    )
    print(response)

_source = sys.argv[1]
_database = sys.argv[2]
_hop = sys.argv[3]
_etl_ts = sys.argv[4]

_bucket = "nbs-test-static"

_emr_results_path = f"/home/hadoop/test-framework/allure-results/{_hop}"
_emr_results_history_path = f"{_emr_results_path}/history"
_emr_reports_path = f"/home/hadoop/test-framework/allure-reports/{_hop}/{_etl_ts}"
_emr_reports_serverity_path = f"/home/hadoop/test-framework/allure-reports/{_hop}/{_etl_ts}/widgets/severity.json"
_emr_history_path = f"{_emr_reports_path}/history"

_s3_hop_history_path = f"s3://{_bucket}/{_hop}/history"
_s3_reports_path = f"s3://{_bucket}/{_hop}/{_etl_ts}"

_code_path = "/home/hadoop/test-framework/test_cases/sch_evo_architecture_based_test_cases/data_integrity_test_cases"

_test_cases = {
    "staging": ["test_count_validation.py", "test_column_validation.py -n 20"],
    "snapshot": ["test_column_validation.py -n 20"]
}

try:
    test_cases = _test_cases[_hop.lower()]
    _catalog_created = False

    for _test_case in test_cases:
        print(f"Running Test Framework Test Case: {_test_case}, with parameters Source: {_source}, Database: {_database}, Hop: {_hop}, ETL_TS: {_etl_ts}, Catalog Created: {_catalog_created}")
        command = f"/usr/local/bin/pytest {_code_path}/{_test_case} --source {_source} --database {_database} --hop {_hop} --etl_ts {_etl_ts} --catalog_created {_catalog_created} --alluredir {_emr_results_path}"
        os.system(command)
        _catalog_created = True

    print("Getting history of previous reports")
    command = f"aws s3 sync {_s3_hop_history_path} {_emr_results_history_path}"
    print(command)
    os.system(command)

    print("Generating reports")
    command = f"/home/hadoop/.nvm/versions/node/v13.9.0/bin/node /home/hadoop/node_modules/allure-commandline/bin/allure generate --clean {_emr_results_path} -o {_emr_reports_path}"
    print(command)
    os.system(command)

    print("Storing reports on S3")
    command = f"aws s3 sync {_emr_reports_path} {_s3_reports_path}"
    print(command)
    os.system(command)

    print(f"Syncing history for hop {_hop} to S3")
    command = f"aws s3 sync {_emr_history_path} {_s3_hop_history_path}"
    print(command)
    os.system(command)

    response = get_test_cases_status(_emr_reports_serverity_path)
    if response["status_code"] == 1:
        send_notificaiton("Failed", response)
        raise Exception
    else:
        send_notificaiton("Passed")

except Exception as Ex:
    print(Ex)
    exit(1)
