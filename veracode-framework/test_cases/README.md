## Test Cases Execution
Test Cases can be executed in different ways depending upon requirements.

1) Run test cases belonging to one file only. 
```bash
pytest \
    {path_to_test_file}/test_column_validation.py \
    --hop {hop} \
    --source {source} \
    --database {database} \
    --etl_ts {etl_ts}  \
    --catalog_created False \
    --alluredir {path_to_allure_results}
```
2) Run only one test case from a file. Add `-k` with `test name` at the end of the command
```bash
pytest \
    {path_to_test_file}/test_column_validation.py \
    -k {test_name} \
    --hop {hop} \
    --source {source} \
    --database {database} \
    --etl_ts {etl_ts}  \
    --catalog_created False \
    --alluredir {path_to_allure_results}
```

### Allure Report Generation
**Copy History Folder:**
Allure provide a feature to see the Trends from previous test executions by managing history. For this we copy the history folder from previous reports data into the current binaries folder before generating new reports.
```bash
aws s3 sync {path_to_reports_history folder} {path_to_results_folder}
```
**Generate Reports:**
We've used allure attribute `--alluredir {/dir}` with test cases execution command that will generate binaries of the executed tests. 

To generate reports we use `allure generate`
```bash
allure generate --clean {results_path} -o {reports_path}
```

## Example Test Run:
As an example let's run test cases mentioned in file `test_column_validation.py`. 
We'll be using following parameters:
```bash
  hop = staging
  etl_ts = 202001030900
  source = oracle
  database = intake
```
Use this command to start test case execution:
```bash
/usr/local/bin/pytest /home/hadoop/test-framework/test_cases/sch_evo_architecture_based_test_cases/data_integrity_test_cases/test_column_validation.py --source oracle --database intake --hop staging --etl_ts 202001030900 --catalog_created False --alluredir /home/hadoop/test-framework/allure-results/staging
```
**Get History of Previous Reports:**

In order to manage trends we copy history folder from previously generated reports and paste into the allure-results
folder where binaries of the current test execution are saved. This is not required when running script for the first time.
```bash
aws s3 sync s3://nbs-test-static/staging/history /home/hadoop/test-framework/allure-results/staging/history
```

**Generate Allure Report:**
```bash
/home/hadoop/.nvm/versions/node/v13.9.0/bin/node /home/hadoop/node_modules/allure-commandline/bin/allure generate --clean /home/hadoop/test-framework/allure-results/staging -o /home/hadoop/test-framework/allure-reports/staging/202001030900
```

**Store Reports to S3 bucket** (static hosting enabled):

S3 Bucket Name `nbs-test-static`
```bash
aws s3 sync /home/hadoop/test-framework/allure-reports/staging/202001030900 s3://nbs-test-static/staging/202001030900
```

Allure html report can now found at path `s3://nbs-test-static/staging/202001030900`

Open `index.html` file to view report in browser













