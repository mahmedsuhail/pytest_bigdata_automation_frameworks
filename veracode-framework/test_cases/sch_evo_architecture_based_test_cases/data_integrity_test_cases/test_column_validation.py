"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Not null column Validation

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-587

Modification Log:

-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/08/2020        Zeeshan Mirza        VDL-587             Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import sys
import allure
import pytest
sys.path.append("..")

from managers.sch_evo_architecture_base_manager.data_integrity_manager.row_count_manager import RowCountValidationManager
from managers.common_managers.s3_manager import s3_landing_data
from managers.sch_evo_architecture_base_manager.data_integrity_manager.column_manager import ColumnValidationManager
from managers.sch_evo_architecture_base_manager.json_managers.json_managers_impl.json_catalog_manager import JsonCatalogManager

stage = None
pytest.source_to_validate = "oracle"
pytest.data_base = "intake"
pytest.etl_tss="2020-01-02_15:03:32.143384"
catalogManager = None


def parse_dictionary(dictionary):
    keys = dictionary.keys()
    final_list = []
    for key in keys:
        initial_list = dictionary[key]
        for value in initial_list:
            temp = key+"."+value
            final_list.append(temp)
    print(final_list)
    return final_list

# @pytest.fixture
# @pytest.mark.usefixtures(hop,database,source)
def get_not_null_columns_for_all_tables():
    print("getting list of not null columns for all tables")
    #print("Stage :: %s, Source :: %s, Database :: %s" % (hop, source, database))
    print(pytest.source_to_validate)
    print(pytest.data_base)
    print(pytest.etl_tss)

    catalogManager = JsonCatalogManager(pytest.source_to_validate, pytest.data_base, "catalog_config.json")
    not_null_coulumn_list = catalogManager.get_not_null_columns_for_all_tables()
    print(not_null_coulumn_list)
    s3_manager = s3_landing_data()  #etl_timestamp
    etl_table_list = s3_manager.get_list_of_tables_by_etl_timestamp(pytest.etl_tss)
    print(etl_table_list)
    #return parse_dictionary(not_null_coulumn_list)
    return etl_table_list


@pytest.fixture(params=get_not_null_columns_for_all_tables())
def params(request):
    """
    Fixture is applied to execute test cases dynamically for multiple attributes
    :param request:
    :return: This will return the params from config file and keep itself to execute test cases one by one
    """
    return request.param

# Count validation
# @allure.feature("Data Integrity | Column Level Validation")
# @allure.story("Column Not Null Validation")
# @allure.severity(allure.severity_level.CRITICAL)

def test_setlist(etl_ts):
    pytest.etl_tss = etl_ts
    print(pytest.etl_tss)

def test_column_not_null_validation(params, database, source, hop):
    print("In test casse")
    stage = hop
    source_to_validate = source
    data_base = database
    print(params)
    print("Stage :: %s, Source :: %s, Database :: %s" %(stage, source_to_validate, data_base))
    """
    container_result = False
    # print("Param Values \n",params)
    splited_list = params.split('.')
    print("List :: ", splited_list)
    resultant_table = splited_list[0]
    column_name = splited_list[len(splited_list) - 2]
    resultant_db = catalogManager.get_target_db_by_source_database(database)

    columnValidationManager = ColumnValidationManager()

    result = columnValidationManager.column_nullable_validation(resultant_table, resultant_db, column_name)
      
    expected_table = resultant_table
    manager = RowCountValidationManager()
    if resultant_table == 'order':
        print("Counts are matched")
        container_result = True
        assert container_result == True
        return
    print(hop)
    if hop == "historical":
        print("Hope is True")
        container_result = manager.landing_to_historical_row_count_validation(resultant_table,expected_table)

    if container_result is True:
        print("Counts are matched")
    else:
        print("Counts are not matched")
    assert container_result == True
"""