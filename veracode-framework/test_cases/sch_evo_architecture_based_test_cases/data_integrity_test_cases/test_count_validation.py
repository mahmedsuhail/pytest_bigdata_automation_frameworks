"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Count Level Validation

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-587

Modification Log:

-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/01/2020        Zeeshan Mirza        VDL-             Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import logging
import sys

#import allure
import pytest
import globals as gbl
sys.path.append("..")
# from test_cases.conftest import etl_ts
from managers.common_managers.s3_manager import s3_landing_data
from managers.sch_evo_architecture_base_manager.data_integrity_manager.row_count_manager import RowCountValidationManager

pytest.table_list=[]

# @pytest.fixture(scope="session")
def get_hop_source_etl_table_names():
    #etl_ts = "2020-01-02_15:03:32.143384"

    # data = pytest.request.config.cache.get('input_params')
    # print("Global :: ",pytest.global_etl_ts)
    # print("ETL Timestamp 2 :: " , pytest.global_etl_ts)
    # print("goint to get list")
    s3_manager = s3_landing_data(pytest.etl_tss)
    etl_table_list = s3_manager.get_list_of_tables_by_etl_timestamp()
    print(etl_table_list)
    return etl_table_list

# @pytest.fixture()
# def etl_ts(pytestconfig):
#     pytest.global_etl_ts = pytestconfig.getoption("etl_ts")
#     print("ETL :: :: ", pytestconfig.getoption("etl_ts"))
#     # return pytestconfig.getoption("etl_ts")


@pytest.fixture( params=get_hop_source_etl_table_names())
def params(request):
    """
    Fixture is applied to execute test cases dynamically for multiple attributes
    :param request:
    :return: This will return the params from config file and keep itself to execute test cases one by one
    """
    # request.param = get_hop_source_etl_table_names(etl_ts)
    return request.param

def test_setlist(etl_ts):
    pytest.etl_tss = etl_ts
    #print(pytest.etl_tss)

# Count validation
# @allure.feature("Data Integrity")
# @allure.story("Count Validation")
# @allure.severity(allure.severity_level.CRITICAL)
def test_count_validation(hop, params):
    # etl_timestamp = etl_ts
    # print("ETL time :: ", get_etl_ts)
    # params()
    container_result = False
    print(hop)
    print("Param Values \n",params)
"""
    splited_list = params.split('/')
    resultant_table = splited_list[len(splited_list)-2]
    print(len(splited_list))
    print(splited_list)
    print(splited_list[len(splited_list)-2])
    print("Table Name ",resultant_table )
    expected_table = resultant_table
    manager = RowCountValidationManager()
    # if resultant_table == 'order':
    #     print("Counts are matched")
    #     container_result = True
    #     assert container_result == True
    #     return
    # print(hop)
    if hop == "staging":
        # print("Hope is True")
        container_result = manager.landing_to_historical_row_count_validation(resultant_table,expected_table)

    if container_result is True:
        print("Counts are matched for table : ", resultant_table)
    else:
        print("Counts are not matched for table : ", resultant_table)
    assert container_result == True
"""