"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Curation Validation - Structured

Author  : Adil Qayyum
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Adil Qayyum        VDL-283              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import sys

import allure
import pytest
from managers.curation_manager import *

from managers.glue_architecture_based_managers.cataloging_manager import *

sys.path.append("..")


class TestCurationStructured:

    # Cataloging Test Cases

    @allure.feature("Structured - Curation - Cataloging")
    @allure.story("Dim Account - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_account_catalog(self):
        result = verify_curation_table_catalog('dim_account')
        assert result == True

    @pytest.mark.skip
    @allure.feature("Structured - Curation - Cataloging")
    @allure.story("Dim User - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_user_catalog(self):
        result = verify_curation_table_catalog('dim_user')
        assert result == True

    @pytest.mark.skip
    @allure.feature("Structured - Curation - Cataloging")
    @allure.story("Dim Proxy - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_proxy_catalog(self):
        result = verify_curation_table_catalog('dim_proxy')
        assert result == True

    @pytest.mark.skip
    @allure.feature("Structured - Curation - Cataloging")
    @allure.story("Dim App Team - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_app_team_catalog(self):
        result = verify_curation_table_catalog('dim_app_team')
        assert result == True

    @allure.feature("Structured - Curation - Cataloging")
    @allure.story("Dim App Tag - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_app_tag_catalog(self):
        result = verify_curation_table_catalog('dim_app_tag')
        assert result == True

    @allure.feature("Structured - Curation - Cataloging")
    @allure.story("Dim CWE - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_cwe_catalog(self):
        result = verify_curation_table_catalog('dim_cwe')
        assert result == True

    @allure.feature("Structured - Curation - Cataloging")
    @allure.story("Dim App - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_app_catalog(self):
        result = verify_curation_table_catalog('dim_app')
        assert result == True

    @allure.feature("Structured - Curation - Cataloging")
    @allure.story("Dim CVE - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_cve_catalog(self):
        result = verify_curation_table_catalog('dim_cve')
        assert result == True

    @allure.feature("Structured - Curation - Cataloging")
    @allure.story("Fact Findings - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_fact_findings_catalog(self):
        result = verify_curation_table_catalog('fact_findings')
        assert result == True

    @allure.feature("Structured - Curation - Cataloging")
    @allure.story("Fact Scan - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_fact_scan_catalog(self):
        result = verify_curation_table_catalog('fact_scan')
        assert result == True

    # Data Validation Test Cases

    @allure.feature("Structured - Curation - Data Validation")
    @allure.story("Dim Account - Data Validation")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_account_datavalidation(self):
        result = curation_verification('identity', organization_query_raw, dim_account_query_curated)
        assert result == True

    @allure.feature("Structured - Curation - Data Validation")
    @allure.story("Dim User - Data Validation")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_user_datavalidation(self):
        result = curation_verification('identity', user_query_raw, dim_user_query_curated)
        assert result == True

    @allure.feature("Structured - Curation - Data Validation")
    @allure.story("Dim Proxy - Data Validation")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_proxy_datavalidation(self):
        result = curation_verification('identity', user_proxy_query_raw, dim_proxy_query_curated)
        assert result == True

    @allure.feature("Structured - Curation - Data Validation")
    @allure.story("Dim App Team - Data Validation")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_app_team_datavalidation(self):
        result = curation_verification('application', app_team_query_raw, dim_app_team_curated)
        assert result == True

    @allure.feature("Structured - Curation - Data Validation")
    @allure.story("Dim App Tag - Data Validation")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_app_tag_datavalidation(self):
        result = curation_verification('application', app_tag_query_raw, dim_app_tag_curated)
        assert result == True

    @allure.feature("Structured - Curation - Data Validation")
    @allure.story("Dim CWE - Data Validation")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_cwe_datavalidation(self):
        result = curation_verification('finding', cwe_query_raw, dim_cwe_query_curated)
        assert result == True

    @allure.feature("Structured - Curation - Data Validation")
    @allure.story("Dim App - Data Validation")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_app_datavalidation(self):
        result = curation_verification('application', app_query_raw, dim_app_query_curated)
        assert result == True

    @allure.feature("Structured - Curation - Data Validation")
    @allure.story("Dim CVE - Data Validation")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_cve_datavalidation(self):
        result = curation_verification('finding', cve_query_raw, dim_cve_query_curated)
        assert result == True