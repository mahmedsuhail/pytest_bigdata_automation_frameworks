"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Curation Validation - Unstructured

Author  : Adil Qayyum
Release : 1
#Sprint  : 5
Story   : VDL-???

Modification Log:

How to execute:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/25/2019        Adil Qayyum        VDL-???              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import sys

import allure
from managers.curation_manager import *

from managers.glue_architecture_based_managers.cataloging_manager import *

sys.path.append("..")


class TestCurationUnstructured:

    # Cataloging Test Cases

    @allure.feature("Unstructured - Curation")
    @allure.story("AC Logs - Data Validation")
    @allure.severity(allure.severity_level.MINOR)
    def test_ac_logs_data(self):
        result = ac_curation_verification()
        assert result == True

    @allure.feature("Unstructured - Curation")
    @allure.story("Scan Logs - Data Validation")
    @allure.severity(allure.severity_level.MINOR)
    def test_scan_logs_data(self):
        result = scan_curation_verification()
        assert result == True

    @allure.feature("Unstructured - Curation")
    @allure.story("Flaw Logs - Data Validation")
    @allure.severity(allure.severity_level.MINOR)
    def test_flaw_logs_data(self):
        result = flaw_curation_verification()
        assert result == True

    @allure.feature("Unstructured - Curation")
    @allure.story("AC Logs - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_ac_curation_catalog(self):
        with allure.step('Cataloging on Curation'):
            result = verify_ac_logs_catalog('curation')
            assert result == True

    @allure.feature("Unstructured - Curation")
    @allure.story("Scan Logs - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_scan_curation_catalog(self):
        with allure.step('Cataloging on Curation'):
            result = verify_scan_logs_catalog('curation')
            assert result == True

    @allure.feature("Unstructured - Curation")
    @allure.story("Flaw Logs - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_flaw_curation_catalog(self):
        with allure.step('Cataloging on Curation'):
            result = verify_flaw_logs_catalog('curation')
            assert result == True