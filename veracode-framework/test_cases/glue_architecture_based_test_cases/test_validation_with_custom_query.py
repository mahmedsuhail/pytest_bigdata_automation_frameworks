
from managers.glue_architecture_based_managers.structured_testcase_manager import CustomValidation



# custom validation for source to raw
def test_custom_validation_for_source_to_raw():
    container_cls_obj = CustomValidation()
    container_result, variance=container_cls_obj.custom_testcase_container_for_source_to_raw_or_curation("custom_query")
    if container_result is True:
        print("Results are matched")
    else:
        print("Results are not matched , following variance occurred in data comparison \n", variance)
    assert container_result == True

# custom validation for source to landing
def test_custom_validation_for_source_to_landing():
    container_cls_obj = CustomValidation()
    container_result, variance=container_cls_obj.custom_testcase_container_for_source_to_landing("custom_query")
    if container_result is True:
        print("Results are matched")
    else:
        print("Results are not matched , following variance occurred in data comparison \n", variance)
    assert container_result == True

# custom validation for landing to raw
def test_custom_validation_for_landing_to_raw():
    container_cls_obj = CustomValidation()
    container_result, variance=container_cls_obj.custom_testcase_container_for_athena_to_athena("custom_query")
    if container_result is True:
        print("Results are matched")
    else:
        print("Results are not matched , following variance occurred in data comparison \n", variance)
    assert container_result == True


# custom validation for raw to curation
def test_custom_validation_for_raw_to_curation():
    container_cls_obj = CustomValidation()
    container_result, variance = container_cls_obj.custom_testcase_container_for_athena_to_athena("custom_query")
    if container_result is True:
        print("Results are matched")
    else:
        print("Results are not matched , following variance occurred in data comparison \n", variance)
    assert container_result == True

# custom validation for source to curation
def test_custom_validation_for_source_to_curation():
    container_cls_obj = CustomValidation()
    container_result, variance=container_cls_obj.custom_testcase_container_for_source_to_raw_or_curation("custom_query")
    if container_result is True:
        print("Results are matched")
    else:
        print("Results are not matched , following variance occurred in data comparison \n", variance)
    assert container_result == True



