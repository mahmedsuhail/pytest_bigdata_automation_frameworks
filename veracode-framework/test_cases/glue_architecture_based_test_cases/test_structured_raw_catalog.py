"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Raw Catalog Validation - Structured

Author  : Adil Qayyum
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/21/2019        Adil Qayyum        VDL-283              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import sys

import allure
import pytest

from managers.glue_architecture_based_managers.cataloging_manager import *

sys.path.append("..")


class TestRawStructured:

    # Cataloging Test Cases

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("Application - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_application_catalog(self):
        result = verify_raw_table_catalog('agora_applicationservice_'+env,'application')
        assert result == True

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("Sandbox - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_sandbox_catalog(self):
        result = verify_raw_table_catalog('agora_applicationservice_'+env,'sandbox')
        assert result == True

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("App Tag - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_app_tag_catalog(self):
        result = verify_raw_table_catalog('agora_applicationservice_'+env,'app_tag')
        assert result == True

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("App Team - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_app_team_catalog(self):
        result = verify_raw_table_catalog('agora_applicationservice_'+env,'app_team')
        assert result == True

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("Annotation - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_app_tag_catalog(self):
        result = verify_raw_table_catalog('agora_findingservice_'+env,'annotation')
        assert result == True

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("Dim CWE - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_cwe_catalog(self):
        result = verify_raw_table_catalog('agora_findingservice_'+env,'cve')
        assert result == True

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("Dim App - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_app_catalog(self):
        result = verify_raw_table_catalog('agora_findingservice_'+env,'cwe')
        assert result == True

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("Dim CVE - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_dim_cve_catalog(self):
        result = verify_raw_table_catalog('agora_findingservice_'+env,'finding')
        assert result == True

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("Scan - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_scan_catalog(self):
        result = verify_raw_table_catalog('agora_findingservice_'+env,'scan')
        assert result == True

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("State Change - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_state_change_catalog(self):
        result = verify_raw_table_catalog('agora_findingservice_'+env,'state_change')
        assert result == True

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("Status - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_status_catalog(self):
        result = verify_raw_table_catalog('agora_findingservice_'+env,'status')
        assert result == True

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("Organization - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_organization_catalog(self):
        result = verify_raw_table_catalog('agora_identityservice_'+env,'organization')
        assert result == True

    @pytest.mark.skip
    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("User - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_user_catalog(self):
        result = verify_raw_table_catalog('agora_identityservice_'+env,'user')
        assert result == True

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("Policy - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_policy_catalog(self):
        result = verify_raw_table_catalog('agora_policyservice_'+env,'policy')
        assert result == True

    @allure.feature("Structured - Raw - Cataloging")
    @allure.story("Policy Eval - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_policy_eval_catalog(self):
        result = verify_raw_table_catalog('agora_policyservice_'+env,'policy_eval')
        assert result == True