"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Unstructured Data & Cataloging Validation

Author  : Adil Qayyum
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute:  pytest test_unstructured_ingestion.py --alluredir ./results
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Adil Qayyum        VDL-247              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import sys

import allure
from managers.cataloging_manager import *

from managers.glue_architecture_based_managers.ingestion_manager import *

sys.path.append("..")


class TestIngestionUnstructured:

    # allure.environment(Report='Allure report', Environment='Dev')

    #Test Cases for AC logs

    @allure.feature("AC Logs")
    @allure.story("AC Logs - Counts")
    @allure.severity(allure.severity_level.CRITICAL)
    def test_ac_logs(self):
        result = verify_ac_log_count()
        logging.info("AC logs at S3Sync vs Landing:")
        verify_log_landing(bucket_s3sync_ac, bucket_landing, 'ac-logs')
        assert result == True, "AC log count mismatch between Source & S3Sync"

    @allure.feature("AC Logs")
    @allure.story("AC Logs - Counts against keys")
    def test_ac_for_key(self):
        logging.info("AC logs at Source vs S3Sync vs Landing:")
        result = verify_log_against_key(bucket_client_ac, bucket_s3sync_ac, bucket_landing, key_value_ac, exclusion_value_ac)
        assert result == True

    @allure.feature("AC Logs")
    @allure.story("AC Logs - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_ac_landing_catalog(self):
        result = verify_ac_logs_catalog('raw')
        assert result == True

    @allure.feature("AC Logs")
    @allure.story("AC Logs - Data Validation")
    @allure.severity(allure.severity_level.CRITICAL)
    def test_ac_log_values(self):
        result = verify_ac_log_values()
        assert result == True

    #Test Cases for Scan Logs

    @allure.feature("Scan Logs")
    @allure.story("Scan Logs - Counts")
    @allure.severity(allure.severity_level.CRITICAL)
    def test_scan_logs(self):
        result = verify_scan_log_count()
        logging.info("Scan logs at S3Sync vs Landing:")
        verify_log_landing(bucket_s3sync_scan, bucket_landing, 'scan-logs')
        assert result == True, "Scan log count mismatch between Source & S3Sync"

    @allure.feature("Scan Logs")
    @allure.story("Scan Logs - Counts against keys")
    def test_scan_for_key(self):
        print("Scan logs at Source vs S3Sync vs Landing:")
        result = verify_log_against_key(bucket_client_scan, bucket_s3sync_scan, bucket_landing, 'JobID', exclusion_value_scan)
        assert result == True

    @allure.feature("Scan Logs")
    @allure.story("Scan Logs - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_scan_landing_catalog(self):
        with allure.step('Cataloging on Raw'):
            result = verify_scan_logs_catalog('raw')
            assert result == True

    @allure.feature("Scan Logs")
    @allure.story("Scan Logs - Data Validation")
    @allure.severity(allure.severity_level.CRITICAL)
    def test_scan_log_values(self):
        result = verify_scan_log_values()
        assert result == True


    #Test Cases for Flaw Logs

    @allure.feature("Flaw Logs")
    @allure.story("Flaw Logs - Counts")
    @allure.severity(allure.severity_level.CRITICAL)
    def test_flaw_logs(self):
        result = verify_flaw_log_count()
        assert result == True

    @allure.feature("Flaw Logs")
    @allure.story("Flaw Logs - Data Validation")
    @allure.severity(allure.severity_level.CRITICAL)
    def test_flaw_log_values(self):
        result = verify_flaw_log_values()
        assert result == True

    @allure.feature("Flaw Logs")
    @allure.story("Flaw Logs - Cataloging")
    @allure.severity(allure.severity_level.MINOR)
    def test_flaw_landing_catalog(self):

        with allure.step('Cataloging on Raw'):
            result = verify_flaw_logs_catalog('raw')
            assert result == True

