"""
----------------------------------------------------------------------------------------------------------
Description:

usage: data Level Validation from source to landing bucket

Author  : Usman Zahid
Release : 1
#Sprint  : 5
Story   : VDL-

Modification Log:

-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/25/2019        Usman Zahid        VDL-             Initial draft.

-----------------------------------------------------------------------------------------------------------
"""


import sys

import allure

sys.path.append("..")
from managers.glue_architecture_based_managers.structured_testcase_manager import SourceToLandingValidation



# Data validation for agora_applicationservice.application
@allure.feature("Structured - ApplicationService Ingestion")
@allure.story("Application - Data Validation Source vs Landing")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_applicationservice_application():
    container_cls_obj = SourceToLandingValidation()
    container_result, variance=container_cls_obj.testcase_container_for_SourceToLanding_data_validation(
                                                                               "Agora_applicationservice_application_source_to_landing", "application_ddl")
    if container_result is True:
        print("Data is matched")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

@allure.feature("Structured - ApplicationService Ingestion")
@allure.story("App Tag - Data Validation Source vs Landing")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_applicationservice_app_tag():
    container_cls_obj = SourceToLandingValidation()
    container_result, variance=container_cls_obj.testcase_container_for_SourceToLanding_data_validation(
                                                                               "Agora_applicationservice_app_tag_source_to_landing", "app_tag_ddl")
    if container_result is True:
        print("Data is matched")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

# Data validation for agora_findingservice.cve
@allure.feature("Structured - FindingService Ingestion")
@allure.story("CVE - Data Validation Source vs Landing")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_findingservice_cve():
    container_cls_obj = SourceToLandingValidation()
    container_result, variance=container_cls_obj.testcase_container_for_SourceToLanding_data_validation(
                                                                               "Agora_findingservice_cve_source_to_landing", "cve_ddl")
    if container_result is True:
        print("Data is matched")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

