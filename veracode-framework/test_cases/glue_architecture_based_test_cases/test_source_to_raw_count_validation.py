"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Count Level Validation

Author  : Usman Zahid
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute: python -m pytest test_source_to_raw_count_validation.py --html=SourceToRawCountValidationReport.html --self-contained-html
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/1/2019        Usman Zahid        VDL-247              Initial draft.
05/05/2019       Adil Qayyum        VDL-247              Pytest & Allure Integration

-----------------------------------------------------------------------------------------------------------
"""



import sys

import allure

sys.path.append("..")
from managers.glue_architecture_based_managers.structured_testcase_manager import SourceToRawValidation


# Count validation for agora_applicationservice.application
@allure.feature("Structured - ApplicationService Ingestion")
@allure.story("Application - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_applicationservice_application():
    container_cls_obj = SourceToRawValidation()
    container_result=container_cls_obj.testcase_container_for_SourceToRaw_count_validation(
                                                                               "Agora_applicationservice_application")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True


# Count validation for agora_applicationservice.app_tag
@allure.feature("Structured - ApplicationService Ingestion")
@allure.story("App Tag - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_applicationservice_app_tag():
    container_cls_obj = SourceToRawValidation()
    container_result=container_cls_obj.testcase_container_for_SourceToRaw_count_validation( "Agora_applicationservice_app_tag")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True


# Count validation for agora_applicationservice.sandbox
@allure.feature("Structured - ApplicationService Ingestion")
@allure.story("Sandbox - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_applicationservice_sandbox():
    container_cls_obj = SourceToRawValidation()
    container_result=container_cls_obj.testcase_container_for_SourceToRaw_count_validation( "Agora_applicationservice_sandbox")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True

# Count validation for agora_applicationservice.app_team
@allure.feature("Structured - ApplicationService Ingestion")
@allure.story("App Team - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_applicationservice_app_team():
    container_cls_obj = SourceToRawValidation()
    container_result=container_cls_obj.testcase_container_for_SourceToRaw_count_validation( "Agora_applicationservice_app_team")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True

# Count validation for agora_findingservice.state_change
@allure.feature("Structured - FindingService Ingestion")
@allure.story("State Change - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_findingservice_state_change():
    container_cls_obj = SourceToRawValidation()
    container_result=container_cls_obj.testcase_container_for_SourceToRaw_count_validation( "Agora_findingservice_state_change")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True

# Count validation for agora_findingservice.status
@allure.feature("Structured - FindingService Ingestion")
@allure.story("Status - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_findingservice_status():
    container_cls_obj = SourceToRawValidation()
    container_result=container_cls_obj.testcase_container_for_SourceToRaw_count_validation( "Agora_findingservice_status")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True

# Count validation for agora_findingservice.cve
@allure.feature("Structured - FindingService Ingestion")
@allure.story("CVE - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_findingservice_cve():
    container_cls_obj = SourceToRawValidation()
    container_result=container_cls_obj.testcase_container_for_SourceToRaw_count_validation("Agora_findingservice_cve")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True


# Count validation for agora_findingservice.cwe
@allure.feature("Structured - FindingService Ingestion")
@allure.story("CWE - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_findingservice_cwe():
    container_cls_obj = SourceToRawValidation()
    container_result = container_cls_obj.testcase_container_for_SourceToRaw_count_validation( "Agora_findingservice_cwe")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True

# Count validation for agora_findingservice.annotation
@allure.feature("Structured - FindingService Ingestion")
@allure.story("Annotation - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_findingservice_annotation():
    container_cls_obj = SourceToRawValidation()
    container_result = container_cls_obj.testcase_container_for_SourceToRaw_count_validation(
                                                                                 "Agora_findingservice_annotation")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True

# Count validation for agora_findingservice.finding
@allure.feature("Structured - FindingService Ingestion")
@allure.story("Finding - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_findingservice_finding():
    container_cls_obj = SourceToRawValidation()
    container_result = container_cls_obj.testcase_container_for_SourceToRaw_count_validation( "Agora_findingservice_finding", True)
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True

# Count validation for agora_findingservice.scan
@allure.feature("Structured - FindingService Ingestion")
@allure.story("Scan - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_findingservice_scan():
    container_cls_obj = SourceToRawValidation()
    container_result = container_cls_obj.testcase_container_for_SourceToRaw_count_validation("Agora_findingservice_scan", True)
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True

# Count validation for agora_policyservice.policy
@allure.feature("Structured - PolicyService Ingestion")
@allure.story("Policy - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_policyservice_policy():
    container_cls_obj = SourceToRawValidation()
    container_result = container_cls_obj.testcase_container_for_SourceToRaw_count_validation( "Agora_policyservice_policy")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True

# Count validation for agora_policyservice.policy_eval
@allure.feature("Structured - PolicyService Ingestion")
@allure.story("Policy Eval - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_policyservice_policy_eval():
    container_cls_obj = SourceToRawValidation()
    container_result = container_cls_obj.testcase_container_for_SourceToRaw_count_validation(
                                                                                 "Agora_policyservice_policy_eval")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True

# Count validation for agora_identitiyservice.user_proxy_org
@allure.feature("Structured - IdentityService Ingestion")
@allure.story("User Proxy Org - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_identityservice_user_proxy_org():
    container_cls_obj = SourceToRawValidation()
    container_result = container_cls_obj.testcase_container_for_SourceToRaw_count_validation(
                                                                                 "Agora_identityservice_user_proxy_org")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True

# Count validation for agora_identitiyservice.user
@allure.feature("Structured - IdentityService Ingestion")
@allure.story("User - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_identityservice_user():
    container_cls_obj = SourceToRawValidation()
    container_result = container_cls_obj.testcase_container_for_SourceToRaw_count_validation( "Agora_identityservice_user")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True


# Count validation for agora_identitiyservice.organization
@allure.feature("Structured - IdentityService Ingestion")
@allure.story("Organization - Count Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_identityservice_organization():
    container_cls_obj = SourceToRawValidation()
    container_result = container_cls_obj.testcase_container_for_SourceToRaw_count_validation(
                                                                                 "Agora_identityservice_organization")
    if container_result is True:
        print("Counts are matched in both source and target")
    else:
        print("Counts are not matched, so data validation will not take place")
    assert container_result == True


