"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Count Level Validation from landing bucket to raw bucket

Author  : Usman Zahid
Release : 1
#Sprint  : 5
Story   : VDL-

Modification Log:

-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
03/25/2019        Usman Zahid        VDL-             Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import sys

import allure

sys.path.append("..")
from managers.glue_architecture_based_managers.structured_testcase_manager import LandingToRawValidation



# Count validation for agora_applicationservice.application
@allure.feature("Structured - ApplicationService Ingestion")
@allure.story("Application - Count Validation Landing vs Raw")
@allure.severity(allure.severity_level.CRITICAL)
def test_count_agora_applicationservice_application():
    container_cls_obj = LandingToRawValidation()
    container_result=container_cls_obj.testcase_container_for_LandingToRaw_count_validation(
                                                                               "Agora_applicationservice_application_landing_to_raw", "application_ddl")
    if container_result is True:
        print("Counts are matched")
    else:
        print("Counts are not matched")
    assert container_result == True


