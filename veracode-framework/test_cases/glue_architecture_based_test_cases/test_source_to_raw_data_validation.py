"""
----------------------------------------------------------------------------------------------------------
Description:

usage: Data Level Validation

Author  : Usman Zahid
Release : 1
#Sprint  : 4
Story   : VDL-247

Modification Log:

How to execute: python -m pytest test_source_to_raw_data_validation.py --html=SourceToRawDataValidationReport.html --self-contained-html
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
19/03/2019        Usman Zahid        VDL-             Initial draft.


-----------------------------------------------------------------------------------------------------------
"""



import sys

import allure

sys.path.append("..")
from managers.glue_architecture_based_managers.structured_testcase_manager import SourceToRawValidation


# Data validation for agora_applicationservice.application
@allure.feature("Structured - ApplicationService Ingestion")
@allure.story("Application - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_applicationservice_application():
    container_cls_obj = SourceToRawValidation()
    container_result, variance = container_cls_obj.testcase_container_for_SourceToRaw_data_validation\
        ("Agora_applicationservice_application")
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

@allure.feature("Structured - ApplicationService Ingestion")
@allure.story("App Tag - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_applicationservice_app_tag():
    container_cls_obj = SourceToRawValidation()
    container_result, variance=container_cls_obj.testcase_container_for_SourceToRaw_data_validation("Agora_applicationservice_app_tag")
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

# Data validation for agora_applicationservice.app_team
@allure.feature("Structured - ApplicationService Ingestion")
@allure.story("App Team - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_applicationservice_app_team():
    container_cls_obj = SourceToRawValidation()
    container_result, variance=container_cls_obj.testcase_container_for_SourceToRaw_data_validation( "Agora_applicationservice_app_team")
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

# Data validation for agora_applicationservice.sandbox
@allure.feature("Structured - ApplicationService Ingestion")
@allure.story("Sandbox - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_applicationservice_sandbox():
    container_cls_obj = SourceToRawValidation()
    container_result, variance=container_cls_obj.testcase_container_for_SourceToRaw_data_validation("Agora_applicationservice_sandbox")
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

# Data validation for agora_findingservice.annotation
@allure.feature("Structured - FindingService Ingestion")
@allure.story("Annotation - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_findingservice_annotation():
    container_cls_obj = SourceToRawValidation()
    container_result, variance=container_cls_obj.testcase_container_for_SourceToRaw_data_validation("Agora_findingservice_annotation")
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

# Data validation for agora_findingservice.cve
@allure.feature("Structured - FindingService Ingestion")
@allure.story("CVE - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_findingservice_cve():
    container_cls_obj = SourceToRawValidation()
    container_result, variance=container_cls_obj.testcase_container_for_SourceToRaw_data_validation("Agora_findingservice_cve")
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

# Data validation for agora_findingservice.cwe
@allure.feature("Structured - FindingService Ingestion")
@allure.story("CWE - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_findingservice_cwe():
    container_cls_obj = SourceToRawValidation()
    container_result, variance=container_cls_obj.testcase_container_for_SourceToRaw_data_validation("Agora_findingservice_cwe")
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True


# Data validation for agora_findingservice.status
@allure.feature("Structured - FindingService Ingestion")
@allure.story("Status - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_findingservice_status():
    container_cls_obj = SourceToRawValidation()
    container_result, variance=container_cls_obj.testcase_container_for_SourceToRaw_data_validation("Agora_findingservice_status")
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True


# Data validation for agora_findingservice.finding
@allure.feature("Structured - FindingService Ingestion")
@allure.story("Finding - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_findingservice_finding():
    container_cls_obj = SourceToRawValidation()
    container_result, variance=container_cls_obj.testcase_container_for_SourceToRaw_data_validation("Agora_findingservice_finding", True)
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

# Data validation for agora_findingservice.scan
@allure.feature("Structured - FindingService Ingestion")
@allure.story("Scan - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_findingservice_scan():
    container_cls_obj = SourceToRawValidation()
    container_result, variance=container_cls_obj.testcase_container_for_SourceToRaw_data_validation("Agora_findingservice_scan", True)
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

# Data validation for agora_findingservice.state_change
@allure.feature("Structured - FindingService Ingestion")
@allure.story("State Change - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_findingservice_state_change():
    container_cls_obj = SourceToRawValidation()
    container_result, variance=container_cls_obj.testcase_container_for_SourceToRaw_data_validation("Agora_findingservice_state_change")
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

# Data validation for agora_policyservice.policy
@allure.feature("Structured - PolicyService Ingestion")
@allure.story("Policy - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_policyservice_policy():
    container_cls_obj = SourceToRawValidation()
    container_result, variance = container_cls_obj.testcase_container_for_SourceToRaw_data_validation(
                                                                                                 "Agora_policyservice_policy")
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

# Data validation for agora_policyservice.policy_eval
@allure.feature("Structured - PolicyService Ingestion")
@allure.story("Policy Eval - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_policyservice_policy_eval():
    container_cls_obj = SourceToRawValidation()
    container_result, variance = container_cls_obj.testcase_container_for_SourceToRaw_data_validation(
                                                                                                 "Agora_policyservice_policy_eval")
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

# Data validation for agora_identitiyservice.user
@allure.feature("Structured - IdentityService Ingestion")
@allure.story("User - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_identityservice_user():
    container_cls_obj = SourceToRawValidation()
    container_result, variance = container_cls_obj.testcase_container_for_SourceToRaw_data_validation(
                                                                                                 "Agora_identityservice_user")
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True

# Data validation for agora_identitiyservice.organization
@allure.feature("Structured - IdentityService Ingestion")
@allure.story("Organization - Data Validation")
@allure.severity(allure.severity_level.CRITICAL)
def test_data_agora_identityservice_organization():
    container_cls_obj = SourceToRawValidation()
    container_result, variance = container_cls_obj.testcase_container_for_SourceToRaw_data_validation(
                                                                                                 "Agora_identityservice_organization")
    if container_result is True:
        print("Data is matched in both source and target")
    else:
        print("Data is not matched, following variance occurred in data comparison \n", variance)
    assert container_result == True


