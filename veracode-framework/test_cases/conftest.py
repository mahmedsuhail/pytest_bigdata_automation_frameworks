"""
----------------------------------------------------------------------------------------------------------
Description:

usage: This is a pytest config file which makes the input parameters available to test cases.

Author  : Zeeshan Mirza
Release : 1
#Sprint  : 2
Story   : VDL-587

Modification Log:
-----------------------------------------------------------------------------------------------------------
Date                Author              Story               Description
-----------------------------------------------------------------------------------------------------------
01/01/2019        Zeeshan Mirza        VDL-587              Initial draft.

-----------------------------------------------------------------------------------------------------------
"""

import pytest

def pytest_addoption(parser):
    parser.addoption("--etl_ts", action="store", default="20365458")
    parser.addoption("--hop", action="store", default="staging")
    parser.addoption("--loglevel", action="store", default="")
    parser.addoption("--database", action="store", default="db")
    parser.addoption("--source", action="store", default="sss")


@pytest.fixture
def hop(request):
    return request.config.getoption("--hop")

@pytest.fixture
def etl_ts(request):
    return request.config.getoption("--etl_ts")

@pytest.fixture
def loglevel(request):
    return request.config.getoption("--loglevel")

@pytest.fixture
def database(request):
    return request.config.getoption("--database")

@pytest.fixture
def source(request):
    return request.config.getoption("--source")