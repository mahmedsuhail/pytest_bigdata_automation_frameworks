## Test Framework
This repo has the source code for Test Automation Framework for Schema Evolution.

### Pre-requisite

There are few one-time configuration changes that needs to be done as per our execution requirements:

1) HUDI must be available on EMR cluster.

2) Cluster must have permissions to the following services.
```bash
    - AWS S3
    - AWS SNS
    - AWS Glue
    - AWS Athena
``` 

3) In file `buckets_config.json`, set S3 paths for source and target for different hops and athena query results.

4) Make sure file is at location `test-framework/configs/sch_evo_architecture_based_config/catalog_config.json` 

##### NOTE:
`run_test_framework.py` file is a wrapper to run the test framework with the step function. It needs to be updated with the below-mentioned points.

1) Create an S3 bucket with public access and static hosting enabled on which reports of the executed test cases will be saved. Also enable static hosting on that bucket
in order to view html reports in browser. Add that bucket name in `run_test_framework.py` file.

2) Create an SNS topic to be used in the `run_test_framework.py` file, to send the test framework notification


### Project Code Setup
Firstly, make sure the current working directory is `/home/hadoop/`.
Then we need to copy the project code into the instance from `S3` before we proceed with the environment setup.

```bash
# move to /home/hadoop/
cd ~
# aws s3 sync s3://<bucket_name>/<test-framework-code-path>/ ./test-framework 
# this command is being used in Veracode developer account
aws s3 sync s3://nbs-test-veracode/test-framework/ ./test-framework
```
### Execution Environment Setup:
All dependencies and installation commands are mentioned in file `test_env_installation_and_setup.sh`.
We just need to execute this shell file that will take care of all the instance setup including configuring environmental variables.

```bash
sh test-framework/test_env_installation_and_setup.sh
```
Running this file will do the following tasks:

1) Setting up Environmental Variables
```bash
    - SPARK_HOME
    - PYTHONPATH
```
2) Installing all the dependencies
```bash
    - boto3
    - pandas
    - findspark
    - PyMySQL
    - pytest
    - pytest-spark
    - allure-pytest
```
3) Set up Allure to generate reports
```bash
    - node
    - allure-commandline
```

### Test Files
For hop `staging` we execute the test cases mentioned in the following files:
```bash
test_count_validation.py
test_column_validation.py
```
For hop `snapshot` we use following file to run test cases:
```bash
test_column_validation.py
```
### Parameters
For test case execution here are some parameters that we need to provide:

**ETL TimeStamp:** This parameter `--etl_ts` will be used to fetch the latest updated data for test cases to run on.
 
**Database:** The database name `--database`to be used in the tests.

**Source:** This parameter specify source name `--source` with database to compare data with. 

**Hop:** This parameter `--hop` specifies where to execute test cases either on staging or on snapshot. 

## Project Structure
This document describes the project structure of the test automation framework developed.
```bash
-- test-framework   
    - helpers   
    - managers    
    - dependency_injection  
    - allure-report     
    - allure-results    
    - dependency_injection  
    - configs   
        -- glue_architecture_based_config   
        -- sch_evo_architecture_based_config    
            - catalog_config.json   
            - buckets_config.json   
            - data_type_config.json    
    - test_cases    
        -- glue_architecture_based_test_cases   
        -- sch_evo_architecture_based_test_cases    
            - data_integrity_test_cases
                -- test_column_validation.py
                -- test_count_validation.py
        -- conftest.py
    - run_test_framework.py
    - check_test_case_status.py
```

### Managers
1) `athena_column_manager_impl.py` file contains re-usable methods for performing major
validations operations on the columns of each table. These methods are specific to Athena and used to validate the data between 
source and the target data. Validation that are performed for each column are:
    
 - Column Nullable Validation
 - Column Length Validation
 - Column Data Type Validation

2) `spark_column_manager_impl.py` file is used to manage operations related to Spark. We use this file to create temporary tables, cache & uncache them and
perform validations using spark queries. Validations that are performed for each column are:

 - Column Nullable Validation
 - Column Length Validation
 - Column Data Type Validation

3) `row_count_manager.py` is used to perform validation for the row count of the given source and target tables. It queries athena to get the counts
and then returns the result to the test if the counts matches or not.

### Helpers
In our project structure we've helper layer containing python files for each AWS service used in the Framework. 
Each helper file lists re-usable functions belonging to that service reducing redundancy within code. Below are the helper files
we're using:
```bash
    - athena_helper.py
    - spark_helper.py
    - s3_helper.py
```

### Configs
`configs` folder contains configuration files to be used within framework. These files include s3 paths info,
schema details of each table and mapping of data types from redshift to athena or spark

1) `catalog_config.json` file contain information about database name, list of tables names and details about schema of each table. Schema
details include `datatype`, `field_length`, `precision` and `not_null` columns within each table.

2) `buckets_config.json` file contain s3 paths to the CSV and Parquet files for each hop individually. The framework uses
these paths to get tables data for the comparison between source and target.

3) `data_type_config.json` this file contains mapping of the data types from Redshift to the Athena or Spark data types.  

### Test_Cases

1) `test_column_validation.py` this file contain the actual test cases that drives the whole framework operations. These tests run
multiple times on list of tables columns provided in parameters. Test cases are executed for each column and assertions are performed that mark the test cases as pass or failed.

2) `test_count_validation.py` this file contain test case relevant to the number of rows of data in the provided table. It compares the row count between
the source and the target tables and performs the assertions.

3) `conftest.py` this file is used to manage the command-line parameters we've used in the code. It perform
all the pre-requisite operations before the test ran and made the data available that is needed for the test to run smoothly. 

### Allure-Results
This folder contains binaries that are generated as a result of the test cases executed. This folder also contains history
folder copied from `allure-reports` folder for managing trends from the past executed test cases.

### Allure-Reports
This folder contains Allure reports generated for the latest test execution. To view the report
open `index.html` file in browser. 








